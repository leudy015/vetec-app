module.exports = {
  dependencies: {
    'tipsi-stripe': {
      platforms: {android: null},
    },
    'react-native-nested-scroll-view': {
      platforms: {android: null},
    },
    'react-native-navigation': {
      platforms: {android: null},
    },
  },
};

////////https://github.com/tipsi/tipsi-stripe/issues/504
///https://github.com/GoldenOwlAsia/react-native-twitter-signin/issues/130

/* react-native/Libraries/Image/RCTUIImageViewAnimated.m 
if (_currentFrame) {
    layer.contentsScale = self.animatedImageScale;
    layer.contents = (__bridge id)_currentFrame.CGImage;
  } else {
    [super displayLayer:layer];
  }
 */
