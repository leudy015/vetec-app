//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Leudy Martes on 29/03/2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
