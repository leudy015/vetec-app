import mockAsyncStorage from '@react-native-community/async-storage/jest/async-storage-mock';
import { NativeModules as RNNativeModules } from "react-native";

jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);

jest.mock('react-navigation', () =>({
    NavigationEvents: 'mockNavigationEvents',
  }))

  RNNativeModules.UIManager = RNNativeModules.UIManager || {};
  RNNativeModules.UIManager.RCTView = RNNativeModules.UIManager.RCTView || {};
  RNNativeModules.RNGestureHandlerModule = RNNativeModules.RNGestureHandlerModule || {
    State: { BEGAN: "BEGAN", FAILED: "FAILED", ACTIVE: "ACTIVE", END: "END" },
    attachGestureHandler: jest.fn(),
    createGestureHandler: jest.fn(),
    dropGestureHandler: jest.fn(),
    updateGestureHandler: jest.fn(),
  
  };
  RNNativeModules.PlatformConstants = RNNativeModules.PlatformConstants || {
    forceTouchAvailable: false
  };

  jest.mock('react-navigation-stack', () => { return {Header: ()=>'whatever'} });