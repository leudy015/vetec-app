import React from 'react';
import {View, SafeAreaView, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {CustomText} from '../components/CustomText';
import {Button} from './../components/Button';
import _get from 'lodash.get';

function Thank() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}} />
      </SafeAreaView>
      <View style={{alignSelf: 'center'}}>
        <Icon
          type="AntDesign"
          name="checkcircle"
          style={{marginTop: 5, alignSelf: 'center', marginBottom: 10}}
          size={80}
          color="#90C33C"
        />
        <CustomText
          dark={colors.white}
          light={colors.rgb_153}
          style={styles.instruction}>
          ¡Consulta contratado con éxito, ahora ve a tus consulta para iniciar
          el chat una vez aceptada por el veterinario!
        </CustomText>
        <View style={styles.signupButtonContainer}>
          <Button
            dark={colors.white}
            light={colors.white}
            containerStyle={styles.buttonView}
            onPress={() => Navigation.navigate('Consultas')}
            title="Ir a consultas"
            titleStyle={styles.buttonTitle}
          />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers1: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: colors.white,
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(3),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  tarjeta: {
    padding: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.rgb_235,
  },

  instruction: {
    textAlign: 'center',
    marginBottom: 5,
    paddingHorizontal: dimensions.Width(8),
  },
  token: {
    padding: 20,
    textAlign: 'center',
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(10),
  },
  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  crecadr: {
    width: dimensions.Width(90),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    padding: dimensions.Width(5),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(2),
  },

  brand: {
    fontSize: 18,
    marginTop: 10,
    borderWidth: 1,
    padding: 2,
    borderColor: new DynamicValue(colors.back_suave_dark, colors.light_white),
    borderRadius: 5,
    fontWeight: 'bold',
  },

  cont: {
    justifyContent: 'center',
    alignSelf: 'center',
    paddingHorizontal: dimensions.Width(2),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(20),
  },
});

export default Thank;
