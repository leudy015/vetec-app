import React, {useState, useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {CustomText} from '../components/CustomText';
import Navigation from '../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {Avatar} from 'react-native-elements';
import {SwipeAction, Badge} from '@ant-design/react-native';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import NoData from '../components/NoData';
import moment from 'moment';
import {useNavigationParam} from 'react-navigation-hooks';
import Icon from 'react-native-dynamic-vector-icons';
import Toast from 'react-native-simple-toast';
import {GET_MESSAGE_NO_READ, GET_CONVERSATION_CLIENT} from './../query';
import {
  READ_MESSAGE,
  DELETE_CONVERSATION,
  RECIBE_MESSAGE,
} from './../mutations';
import {Query, Mutation, useMutation} from 'react-apollo';
import {usersGet} from '../actionCreators';
import {stylesText} from '../components/StyleText';
import LoadingPLace from './../components/PlaceholderChat';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function Chats({getUsers, navigation, appStart}) {
  const [refreshing, setRefreshing] = useState(false);
  const [recibeMessage] = useMutation(RECIBE_MESSAGE);
  let refetch_list = false;

  const styles = useDynamicStyleSheet(dynamicStyles);

  const data = useNavigationParam('data');
  useEffect(() => {
    appStart(QBConfig);
    getUsers();
    const navFocusListener = navigation.addListener('didFocus', () => {
      // do some API calls here
      if (refetch_list) {
        refetch_list();
      }
      console.log('hooooks');
    });

    return () => {
      navFocusListener.remove();
    };
  });

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const _renderItem = ({item}, refetch) => {
    refetch_list = refetch;

    const recibeMessages = ids => {
      recibeMessage({variables: {conversationID: ids}})
        .then(result => {
          console.log('hhhh', result);
          refetch();
        })
        .catch(() => {});
    };

    const Navegue = (readMessage, id) => {
      readMessage({variables: {conversationID: id}})
        .then(() => {
          refetch();
          recibeMessages(id);
          Navigation.navigate('ChatsScreens', {data: item});
        })
        .catch(err => console.log(err));
    };

    return (
      <View>
        <Mutation
          mutation={DELETE_CONVERSATION}
          variables={{conversationID: item._id}}>
          {deleteConversation => {
            return (
              <Mutation
                mutation={READ_MESSAGE}
                variables={{conversationID: item._id}}>
                {readMessage => {
                  return (
                    <SwipeAction
                      autoClose
                      style={{
                        backgroundColor: 'transparent',
                      }}
                      right={[
                        {
                          text: 'Eliminar',
                          style: {
                            backgroundColor: colors.ERROR,
                            color: 'white',
                            height: 'auto',
                          },
                          onPress: () => {
                            deleteConversation({
                              variables: {id: item._id},
                            }).then(() => {
                              Toast.showWithGravity(
                                'Conversación eliminada con éxito',
                                Toast.LONG,
                                Toast.TOP,
                              );
                              refetch();
                            });
                          },
                        },
                      ]}>
                      <Query
                        query={GET_MESSAGE_NO_READ}
                        variables={{
                          conversationID: item._id,
                          profId: item.prof,
                        }}>
                        {(response, error) => {
                          if (error) {
                            return console.log('Response Error-------', error);
                          }
                          if (response) {
                            const dat =
                              response &&
                              response.data &&
                              response.data.getMessageNoReadClient
                                ? response.data.getMessageNoReadClient
                                    .conversation[0]
                                : '';
                            response.refetch();
                            return (
                              <TouchableOpacity
                                style={styles.list}
                                onPress={() => Navegue(readMessage, item._id)}>
                                <View style={styles.chatcont}>
                                  <Badge dot={item.profesional.connected}>
                                    <Avatar
                                      rounded
                                      size={60}
                                      source={{
                                        uri:
                                          NETWORK_INTERFACE_LINK_AVATAR +
                                          item.profesional.avatar,
                                      }}
                                    />
                                  </Badge>
                                  <View style={styles.supercont}>
                                    <View style={styles.text_name}>
                                      <CustomText
                                        light={colors.black}
                                        dark={colors.white}
                                        numberOfLines={1}
                                        style={{
                                          fontSize: dimensions.FontSize(20),
                                          maxWidth: '50%',
                                        }}>
                                        {item.profesional.nombre}{' '}
                                        {item.profesional.apellidos}{' '}
                                      </CustomText>
                                      {item.profesional.isVerified ? (
                                        <Icon
                                          type="Octicons"
                                          name="verified"
                                          style={{
                                            alignSelf: 'center',
                                            marginLeft: 2,
                                            marginTop: 5,
                                          }}
                                          size={12}
                                          color={colors.main}
                                        />
                                      ) : null}
                                      <CustomText
                                        light={colors.black}
                                        dark={colors.white}
                                        numberOfLines={1}
                                        style={styles.fechas}>
                                        {moment(
                                          item.messagechat[0].createdAt,
                                        ).fromNow('LL')}
                                      </CustomText>
                                    </View>
                                    <View style={styles.text_name}>
                                      <CustomText
                                        numberOfLines={2}
                                        style={{
                                          color: colors.rgb_153,
                                          fontWeight: '300',
                                          fontSize: dimensions.FontSize(14),
                                          marginTop: 0,
                                          width: dimensions.Width(60),
                                        }}>
                                        {item.messagechat[0].text}
                                      </CustomText>
                                      <Badge
                                        text={dat ? dat.count : 0}
                                        overflowCount={9}
                                        style={styles.fechass}
                                      />
                                    </View>
                                  </View>
                                </View>
                              </TouchableOpacity>
                            );
                          }
                        }}
                      </Query>
                    </SwipeAction>
                  );
                }}
              </Mutation>
            );
          }}
        </Mutation>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Chats"
          back={true}
          atrasprofile={true}
        />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={[stylesText.h1, {marginLeft: 15, marginBottom: 20}]}>
        Chats
      </CustomText>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query query={GET_CONVERSATION_CLIENT} variables={{userId: data}}>
          {response => {
            if (response.error) {
              return <LoadingPLace />;
            }
            if (response.loading) {
              return <LoadingPLace />;
            }
            if (response) {
              const Conversation =
                response && response.data && response.data.getConversationclient
                  ? response.data.getConversationclient.conversation
                  : [];
              return (
                <FlatList
                  data={Conversation}
                  showsHorizontalScrollIndicator={false}
                  renderItem={item => _renderItem(item, response.refetch)}
                  keyExtractor={item => item._id}
                  ListEmptyComponent={
                    <NoData menssge="No tienes conversaciones" />
                  }
                />
              );
            }
          }}
        </Query>
      </ScrollView>
      <View />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  chatcont: {
    flexDirection: 'row',
    padding: 10,
    width: dimensions.ScreenWidth,
  },

  supercont: {
    marginLeft: 10,
    ...ifIphoneX(
      {
        width: dimensions.Width(75),
      },
      {
        width: dimensions.Width(75),
      },
    ),
  },

  text_name: {
    flexDirection: 'row',
    marginTop: 5,
  },

  fechas: {
    fontWeight: 'bold',
    marginLeft: 'auto',
  },

  fechass: {
    marginLeft: 'auto',
    marginRight: 15,
    marginTop: 10,
  },

  list: {
    borderBottomWidth: 1,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    minHeight: dimensions.Height(10),
    height: 100,
    justifyContent: 'center',
  },
});

// const mapStateToProps = ({ auth, users }, { exclude = [] }) => ({
//   data: users
//     .users
//     .filter(user => auth.user ? user.id !== auth.user.id : true)
//     .filter(user => exclude.indexOf(user.id) === -1)
// })

const mapDispatchToProps = {
  getUsers: usersGet,
  appStart: appStart,
};

export default connect(
  null,
  mapDispatchToProps,
)(Chats);
