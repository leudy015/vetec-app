import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  Dimensions,
  Vibration,
  Platform,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {Opiniones} from '../components/opiniones';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {Mutation} from 'react-apollo';
import {CREAR_USUARIO_FAVORITE, ELIMINAR_USUARIO_FAVORITE} from '../mutations';
import Toast from 'react-native-simple-toast';
import Share from 'react-native-share';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {stylesText} from '../components/StyleText';
import {GET_PROFEISONAL_DETAILS} from '../query/index';
import {Query} from 'react-apollo';
import PlaceHolder from './../components/placeHolderDetailprof';
import Star from '../components/star';

const window = Dimensions.get('window');

export default function DetailsPro() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  console.log(data.id);

  const [id, setId] = useState(null);
  const [enfavorito, setEnfavorito] = useState(data.anadidoFavorito);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const url = 'vetec://vet';
  const title = `${data.nombre} ${data.apellidos}`;
  const message = 'Te recomiendo este excelente veterinario.';
  const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
  const options = Platform.select({
    ios: {
      activityItemSources: [
        {
          // For sharing url with custom title.
          placeholderItem: {type: 'url', content: url},
          item: {
            default: {type: 'url', content: url},
          },
          subject: {
            default: title,
          },
          linkMetadata: {originalUrl: url, url, title},
        },
        {
          // For sharing text.
          placeholderItem: {type: 'text', content: message},
          item: {
            default: {type: 'text', content: message},
            message: null, // Specify no text to share via Messages app.
          },
          linkMetadata: {
            // For showing app icon on share preview.
            title: message,
          },
        },
        {
          // For using custom icon instead of default text icon at share preview when sharing with message.
          placeholderItem: {
            type: 'url',
            content: icon,
          },
          item: {
            default: {
              type: 'text',
              content: `${message} ${url}`,
            },
          },
          linkMetadata: {
            title: message,
            icon: icon,
          },
        },
      ],
    },
    default: {
      title,
      subject: title,
      message: `${message} ${url}`,
    },
  });

  const onShare = async () => {
    Share.open(options);
  };

  const ONE_SECOND_IN_MS = 1000;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
  data.professionalRatinglist.forEach(start => {
    if (start.rate === 1) rating['1'] += 1;
    else if (start.rate === 2) rating['2'] += 1;
    else if (start.rate === 3) rating['3'] += 1;
    else if (start.rate === 4) rating['4'] += 1;
    else if (start.rate === 5) rating['5'] += 1;
  });

  const ar =
    (5 * rating['5'] +
      4 * rating['4'] +
      3 * rating['3'] +
      2 * rating['2'] +
      1 * rating['1']) /
    data.professionalRatinglist.length;
  let averageRating = 0;
  if (data.professionalRatinglist.length) {
    averageRating = ar.toFixed(1);
  }

  const isNavigation = () => {
    if (id) {
      Navigation.navigate('SelecMascota', {data: data});
    } else {
      Alert.alert(
        'Para continuar debes iniciar sesión',
        'Debes iniciar sesión para continuar con la contratación',
        [
          {
            text: 'Iniciar sesión',
            onPress: () => Navigation.navigate('LoginSocial'),
          },
          {
            text: 'Regístrarme',
            onPress: () => Navigation.navigate('Register'),
          },
        ],
        {cancelable: false},
      );
    }
  };

  const anadirFavorito = (
    crearUsuarioFavoritoPro,
    profesionalId,
    usuarioId,
  ) => {
    if (!id) {
      Alert.alert(
        'Upps debes iniciar sesión',
        'Para añadir este profesional a favorito debes iniciar sesión',
        [
          {
            text: 'Iniciar sesión',
            onPress: () => Navigation.navigate('LoginSocial'),
          },
          {
            text: 'Regístrarme',
            onPress: () => Navigation.navigate('Register'),
          },
        ],
        {cancelable: false},
      );
      return null;
    } else {
      crearUsuarioFavoritoPro({
        variables: {profesionalId, usuarioId},
      }).then(async ({data: res}) => {
        if (
          res &&
          res.crearUsuarioFavoritoPro &&
          res.crearUsuarioFavoritoPro.success
        ) {
          Toast.showWithGravity(
            'Profesional añadido con éxito a al lista de deseos',
            Toast.LONG,
            Toast.TOP,
          );
          console.log('Producto añadido con éxito');
          setEnfavorito(true);
          Vibration.vibrate(PATTERN);
        } else if (
          res &&
          res.crearUsuarioFavoritoPro &&
          !res.crearUsuarioFavoritoPro.success
        )
          return null;
      });
    }
  };

  return (
    <Query
      query={GET_PROFEISONAL_DETAILS}
      variables={{id: data.id, updateProfVisit: true}}>
      {response => {
        if (response.loading) {
          return <PlaceHolder />;
        }
        if (response.error) {
          return console.log('Response Error-------', response.error);
        }
        if (response) {
          response.refetch();
          const datas =
            response && response.data && response.data.getProfesionalone
              ? response.data.getProfesionalone.data
              : '';
          function convert(value) {
            if (value >= 1000000) {
              const re = value / 1000000;
              value = re.toFixed(1) + 'M';
            } else if (value >= 1000) {
              const re = value / 1000;
              value = re.toFixed(1) + 'Mil';
            }
            return value;
          }

          const visitas = convert(datas.visitas);

          return (
            <View style={styles.container}>
              <ParallaxScrollView
                showsVerticalScrollIndicator={false}
                headerBackgroundColor={
                  new DynamicValue(colors.white, colors.back_dark)
                }
                stickyHeaderHeight={STICKY_HEADER_HEIGHT}
                parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
                backgroundSpeed={10}
                renderBackground={() => (
                  <View key="background">
                    <Image
                      blurRadius={10}
                      style={styles.background}
                      source={{
                        uri: NETWORK_INTERFACE_LINK_AVATAR + data.avatar,
                      }}
                    />
                  </View>
                )}
                renderForeground={() => (
                  <View key="parallax-header" style={styles.parallaxHeader}>
                    <Image
                      style={styles.avatar}
                      source={{
                        uri: NETWORK_INTERFACE_LINK_AVATAR + data.avatar,
                      }}
                    />
                  </View>
                )}
                renderFixedHeader={() => (
                  <View key="fixed-header" style={styles.fixedSection}>
                    <View style={styles.fixedSectionText}>
                      <TouchableOpacity
                        onPress={() => Navigation.goBack(null)}
                        style={styles.back}>
                        <Icon
                          name="close"
                          type="AntDesign"
                          size={20}
                          style={styles.icon}
                        />
                      </TouchableOpacity>
                    </View>

                    <View style={styles.fixedSectionText1}>
                      <TouchableOpacity
                        onPress={() => onShare()}
                        style={styles.back}>
                        <Icon
                          name="share"
                          type="Feather"
                          size={20}
                          style={styles.icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
                renderStickyHeader={() => (
                  <View key="sticky-header" style={styles.stickySection}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={styles.stickySectionText}>
                      {data.nombre}{' '}
                      {data.isVerified ? (
                        <Icon
                          name="verified"
                          type="Octicons"
                          size={14}
                          style={{
                            alignSelf: 'center',
                            marginTop: 0,
                            marginLeft: 0,
                            color: colors.main,
                          }}
                        />
                      ) : null}
                    </CustomText>
                  </View>
                )}>
                <View style={styles.contenedor}>
                  <View
                    style={{
                      marginTop: dimensions.Height(5),
                      paddingHorizontal: dimensions.Width(4),
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <CustomText
                        numberOfLines={1}
                        light={colors.back_dark}
                        dark={colors.white}
                        style={[
                          stylesText.mainText,
                          {
                            maxWidth: dimensions.Width(40),
                            width: 'auto',
                          },
                        ]}>
                        {data.nombre} {data.apellidos}{' '}
                      </CustomText>
                      {data.isVerified ? (
                        <Icon
                          name="verified"
                          type="Octicons"
                          size={14}
                          style={{
                            alignSelf: 'center',
                            marginTop: 0,
                            marginLeft: 7,
                            color: colors.main,
                          }}
                        />
                      ) : null}
                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={[
                          stylesText.secondaryText,
                          {
                            marginLeft: 'auto',
                          },
                        ]}>
                        {data.Precio}€ /Consulta
                      </CustomText>
                    </View>

                    <View style={{flexDirection: 'row'}}>
                      <CustomText
                        light={colors.main}
                        dark={colors.main}
                        style={[stylesText.secondaryText, {marginTop: 5}]}>
                        {data.profesion}
                      </CustomText>
                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={[
                          stylesText.secondaryText,
                          {marginTop: 5, marginLeft: 'auto'},
                        ]}>
                        <Icon
                          name="enviromento"
                          type="AntDesign"
                          size={14}
                          color={colors.main}
                        />{' '}
                        {data.ciudad}
                      </CustomText>
                    </View>
                    <CustomText
                      numberOfLines={1}
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {marginTop: 5, width: 300},
                      ]}>
                      {data.experiencia} de experiencia
                    </CustomText>
                    <View style={{marginLeft: 'auto'}}>
                      {enfavorito ? (
                        <Mutation
                          mutation={ELIMINAR_USUARIO_FAVORITE}
                          variables={{id: data.id}}>
                          {eliminarUsuarioFavoritoPro => {
                            return (
                              <TouchableOpacity
                                onPress={() => {
                                  eliminarUsuarioFavoritoPro({
                                    variables: {id: data.id},
                                  }).then(res => {
                                    Toast.showWithGravity(
                                      'Profesional eliminado con éxito de la lista de deseos',
                                      Toast.LONG,
                                      Toast.TOP,
                                    );
                                    setEnfavorito(false);
                                    Vibration.vibrate(PATTERN);
                                    if (
                                      res &&
                                      res.data &&
                                      res.data.eliminarUsuarioFavoritoPro
                                        ? res.data.eliminarUsuarioFavoritoPro
                                            .success
                                        : ''
                                    ) {
                                      console.log(
                                        res &&
                                          res.data &&
                                          res.data.eliminarUsuarioFavoritoPro
                                          ? res.data.eliminarUsuarioFavoritoPro
                                              .message
                                          : '',
                                      );
                                    } else if (
                                      res &&
                                      res.data &&
                                      res.data.eliminarUsuarioFavoritoPro
                                        ? !res.data.eliminarUsuarioFavoritoPro
                                            .success
                                        : ''
                                    )
                                      console.log(
                                        res &&
                                          res.data &&
                                          res.data.eliminarUsuarioFavoritoPro
                                          ? !res.data.eliminarUsuarioFavoritoPro
                                              .message
                                          : '',
                                      );
                                  });
                                }}>
                                <Icon
                                  name="heart"
                                  type="AntDesign"
                                  size={40}
                                  style={{
                                    alignSelf: 'center',
                                    marginTop: -3,
                                    marginRight: 15,
                                    color: colors.ERROR,
                                  }}
                                />
                              </TouchableOpacity>
                            );
                          }}
                        </Mutation>
                      ) : (
                        <Mutation mutation={CREAR_USUARIO_FAVORITE}>
                          {crearUsuarioFavoritoPro => {
                            return (
                              <TouchableOpacity
                                onPress={() =>
                                  anadirFavorito(
                                    crearUsuarioFavoritoPro,
                                    data.id,
                                    id,
                                  )
                                }>
                                <Icon
                                  name="hearto"
                                  type="AntDesign"
                                  size={40}
                                  style={{
                                    alignSelf: 'center',
                                    marginTop: -3,
                                    marginRight: 15,
                                    color: colors.rgb_153,
                                  }}
                                />
                              </TouchableOpacity>
                            );
                          }}
                        </Mutation>
                      )}
                    </View>
                  </View>

                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'flex-start',
                    }}>
                    <View
                      style={{
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                        marginTop: 20,
                        marginLeft: 15,
                      }}>
                      <View style={styles.qubo}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.light_white}
                          style={[stylesText.terciaryText, {marginBottom: 5}]}>
                          <Icon
                            name="eye"
                            type="AntDesign"
                            size={20}
                            color={colors.lightGreen}
                          />
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.light_white}
                          style={{
                            fontSize: dimensions.FontSize(14),
                            fontWeight: '300',
                            textAlign: 'center',
                          }}>
                          {visitas} Vistas
                        </CustomText>
                      </View>
                      <View style={styles.qubo}>
                        <CustomText
                          style={{
                            fontSize: dimensions.FontSize(14),
                            fontWeight: '300',
                            marginBottom: 5,
                          }}>
                          <Icon
                            name="dog"
                            type="MaterialCommunityIcons"
                            size={20}
                            color={colors.main}
                          />
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.light_white}
                          style={[
                            stylesText.terciaryText,
                            {marginBottom: 5, textAlign: 'center'},
                          ]}>
                          +{convert(data.consultas.length)} Consultas
                        </CustomText>
                      </View>
                      <View style={styles.qubo}>
                        <CustomText
                          style={{
                            fontSize: dimensions.FontSize(14),
                            fontWeight: '400',
                            marginBottom: 5,
                          }}>
                          <Icon
                            name={
                              data.professionalRatinglist.length === 0
                                ? 'staro'
                                : 'star'
                            }
                            type="AntDesign"
                            size={20}
                            color={colors.orange}
                          />
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.light_white}
                          style={[
                            stylesText.terciaryText,
                            {marginBottom: 5, textAlign: 'center'},
                          ]}>
                          ({convert(data.professionalRatinglist.length)})
                          Opiniones
                        </CustomText>
                      </View>
                    </View>
                  </View>

                  <View style={{marginTop: dimensions.Height(2)}}>
                    <TouchableOpacity
                      style={styles.btncont}
                      onPress={isNavigation}>
                      <CustomText
                        light={colors.main}
                        dark={colors.white}
                        style={styles.te}>
                        Hacer consulta
                      </CustomText>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      marginTop: dimensions.Height(3),
                      paddingHorizontal: dimensions.Width(4),
                    }}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.white}
                      style={stylesText.mainText}>
                      Descripción
                    </CustomText>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={[stylesText.secondaryText, {marginTop: 20}]}>
                      {data.descricion}
                    </CustomText>
                  </View>

                  <View
                    style={{
                      marginTop: dimensions.Height(3),
                      paddingHorizontal: dimensions.Width(4),
                    }}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.white}
                      style={stylesText.mainText}>
                      Opiniones de pacientes
                    </CustomText>

                    <View style={styles.cardop}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.rating}>
                        {averageRating}
                      </CustomText>
                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: dimensions.Width(3),
                        }}>
                        <Star star={averageRating} />
                        <CustomText
                          light={colors.rgb_153}
                          dark={colors.rgb_153}
                          style={stylesText.secondaryText}>
                          ({convert(data.professionalRatinglist.length)}){' '}
                          Opiniones de pacientes
                        </CustomText>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      marginTop: dimensions.Height(1),
                      flex: 1,
                      marginBottom: dimensions.Height(5),
                    }}>
                    <Opiniones data={data.professionalRatinglist} />
                  </View>
                </View>
              </ParallaxScrollView>
            </View>
          );
        }
      }}
    </Query>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,

    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },

  btncont: {
    width: dimensions.Width(91),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.main),
    borderWidth: 1,
    borderColor: colors.main,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 100,
      },
      {
        left: 75,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
    flexDirection: 'row',
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },
  prof: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 10,
  },

  cardop: {
    width: dimensions.Width(92),
    height: 90,
    paddingHorizontal: dimensions.Width(4),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  rating: {
    alignSelf: 'center',
    fontSize: dimensions.FontSize(50),
    fontWeight: 'bold',
  },

  textw: {
    fontSize: dimensions.FontSize(16),
    marginTop: 10,
  },

  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(12),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(3),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
  },

  back1: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
    borderRadius: 10,
    marginLeft: 'auto',
  },
});
