import React, {useState} from 'react';
import {View, Alert, Platform, ActivityIndicator} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/HeaderPlus';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {CustomText} from './../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {ScrollView} from 'react-native-gesture-handler';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from './../constants/config';
import {Avatar} from 'react-native-elements';
import {Button} from './../components/Button';
import {Mutation} from 'react-apollo';
import {useNavigationParam} from 'react-navigation-hooks';
import moment from 'moment';
import MyMacotas from './../components/MascotaforSuscripcion';
import {ACTUALIZAR_USUARIO} from './../mutations';
import io from 'socket.io-client';
import QB from 'quickblox-react-native-sdk';
import {showError} from '../services/FlashMessageService';
import {webrtcCall} from '../actionCreators'; // webrtc call audio & video
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';
import {connect} from 'react-redux';
import {stylesText} from '../components/StyleText';

function Myplus({call, qbUsers}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');
  const [Loading, setLoading] = useState(false);

  if (Platform.OS === 'ios') {
    requestMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.CAMERA]).then(
      statuses => {
        console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
        console.log('FaceID', statuses[PERMISSIONS.IOS.FACE_ID]);
      },
    );
  } else {
    requestMultiple([
      PERMISSIONS.ANDROID.MICROPHONE,
      PERMISSIONS.ANDROID.CAMERA,
    ]).then(statuses => {
      console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
      console.log('FaceID', statuses[PERMISSIONS.IOS.FACE_ID]);
    });
  }

  data.ref();

  const SendPushNotificationVideo = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification-profesional?IdOnesignal=${
        data.datos.profesional.UserID
      }&textmessage= Video llamada de ${data.datos.usuario.nombre} ${
        data.datos.usuario.apellidos
      }`,
    ).catch(err => console.log(err));
  };

  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  socket.on('connect', () => {
    socket.emit('online_user', {
      usuario: data.datos.usuario.id,
      profesional: data.datos.profesional.id,
      id: data.datos.usuario.id,
    });
  });

  let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
  data &&
    data.datos.profesional.professionalRatinglist.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

  const ar =
    (5 * rating['5'] +
      4 * rating['4'] +
      3 * rating['3'] +
      2 * rating['2'] +
      1 * rating['1']) /
      data && data.datos.profesional.professionalRatinglist.length;
  let averageRating = 0;
  if (data && data.datos.profesional.professionalRatinglist.length) {
    averageRating = ar.toFixed(1);
  }

  var day = moment.unix(data.datos.usuario.suscription.current_period_start);
  var day1 = moment.unix(data.datos.usuario.suscription.current_period_end);

  const chats = () => {
    Navigation.navigate('ChatsScreens', {
      data: data.datos,
    });
    socket.emit('available', {
      user: data.datos.usuario.id,
      prof: data.datos.profesional.id,
    });
  };

  const handleSubmit = async mutation => {
    const input = {
      id: data.datos.usuario.id,
      setVideoConsultas: ++data.datos.usuario.setVideoConsultas,
    };
    mutation({variables: {input}})
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));
  };

  const videoCall = mutation => {
    setLoading(true);
    const emails = data.datos.profesional.email;
    console.log('qbUsers', qbUsers);
    let users = qbUsers.filter(opponentUser => {
      if (opponentUser.login === emails) {
        handleSubmit(mutation);
        setLoading(false);
        return true;
      } else {
        setLoading(true);
        return false;
      }
    });
    console.log('usersusers', users);
    const opponentsIds = [users[0].id];
    try {
      call({
        opponentsIds,
        type: QB.webrtc.RTC_SESSION_TYPE.VIDEO,
      });
      SendPushNotificationVideo();
      Navigation.navigate('CheckConnection');
    } catch (e) {
      showError('Error', e.message);
    }
  };

  const calcelarsus = async () => {
    let calcelar = await fetch(
      NETWORK_INTERFACE_LINK +
        `/cancel-suscription?suscription=${
          data.datos.usuario.suscription.id
        }&UserID=${data.datos.usuario.id}`,
    );

    const canceled = await calcelar.json();
    console.log(canceled);
    if (canceled.success) {
      socket.emit('not_available', {
        user: data.datos.usuario.id,
        prof: data.datos.profesional.id,
      }),
        Navigation.navigate('Profile', {
          data: data.id,
        });
    } else {
      Alert.alert(
        'Error con la operación',
        'Hubo un error con tu operación vuelve a intentarlo por favor',
        [
          {
            text: 'Ok',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
    }
  };

  return (
    <Mutation mutation={ACTUALIZAR_USUARIO}>
      {mutation => {
        return (
          <View style={styles.container}>
            <View>
              <Headers
                navigation={Navigation}
                calcelarsus={calcelarsus}
                plus={true}
              />
            </View>
            <View style={styles.contenedor} />
            <ScrollView showsVerticalScrollIndicator={false}>
              <CustomText
                style={[stylesText.mainText, {textAlign: 'center'}]}
                light={colors.back_dark}
                dark={colors.white}>
                Mi área de{' '}
                <CustomText
                  style={[stylesText.mainText, {textAlign: 'center'}]}
                  light={colors.main}
                  dark={colors.main}>
                  Vetec PLUS
                </CustomText>
              </CustomText>

              <View style={styles.containers}>
                <View style={styles.coninfo}>
                  <Avatar
                    rounded
                    size={100}
                    source={{
                      uri:
                        NETWORK_INTERFACE_LINK_AVATAR +
                        data.datos.profesional.avatar,
                    }}
                    containerStyle={styles.avatarst}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <CustomText
                      numberOfLines={1}
                      light={colors.back_dark}
                      dark={colors.white}
                      style={[
                        stylesText.mainText,
                        {
                          textAlign: 'center',
                          width: dimensions.Width(40),
                          marginTop: 20,
                        },
                      ]}>
                      {data.datos.profesional.nombre}{' '}
                      {data.datos.profesional.apellidos} {''}
                    </CustomText>
                    {data.datos.profesional.isVerified ? (
                      <Icon
                        name="verified"
                        type="Octicons"
                        size={16}
                        style={{
                          alignSelf: 'center',
                          marginTop: 0,
                          paddingLeft: 5,
                          color: colors.main,
                        }}
                      />
                    ) : null}
                  </View>
                </View>
                <View
                  style={{
                    alignSelf: 'center',
                  }}>
                  <CustomText
                    light={colors.main}
                    dark={colors.main}
                    style={[
                      stylesText.secondaryText,
                      {
                        textAlign: 'center',
                        marginTop: 10,
                      },
                    ]}>
                    {data.datos.profesional.profesion}
                  </CustomText>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.secondaryText,
                      {
                        textAlign: 'center',
                        marginTop: 10,
                      },
                    ]}>
                    {data.datos.profesional.experiencia} de experiencia
                  </CustomText>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 10,
                      alignItems: 'center',
                      alignSelf: 'center',
                      textAlign: 'center',
                    }}>
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      color={colors.orange}
                    />
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      color={colors.orange}
                    />
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      color={colors.orange}
                    />
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      color={colors.orange}
                    />
                    <Icon
                      name="staro"
                      type="AntDesign"
                      size={20}
                      color={colors.orange}
                    />
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[
                        stylesText.secondaryText,
                        {
                          textAlign: 'center',
                          marginLeft: 10,
                        },
                      ]}>
                      ({averageRating})
                    </CustomText>
                  </View>
                  <View style={styles.suscripcion}>
                    <View
                      style={{
                        marginTop: dimensions.Height(2),
                        marginRight: 10,
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={[
                          stylesText.secondaryText,
                          {
                            textAlign: 'center',
                            marginTop: 0,
                          },
                        ]}>
                        Inicio de la suscripción
                      </CustomText>
                      <CustomText
                        light={colors.main}
                        dark={colors.main}
                        style={[
                          stylesText.secondaryText,
                          {
                            textAlign: 'center',
                            marginTop: 10,
                          },
                        ]}>
                        {moment(Number(day)).format('LL')}
                      </CustomText>
                    </View>
                    <View
                      style={{
                        marginTop: dimensions.Height(2),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={[
                          stylesText.secondaryText,
                          {
                            textAlign: 'center',
                            marginTop: 0,
                          },
                        ]}>
                        Proxímo pago autómatico
                      </CustomText>
                      <CustomText
                        light={colors.main}
                        dark={colors.main}
                        style={[
                          stylesText.secondaryText,
                          {
                            textAlign: 'center',
                            marginTop: 10,
                          },
                        ]}>
                        {moment(Number(day1)).format('LL')}
                      </CustomText>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: dimensions.Height(3),
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View style={styles.signupButtonContainer}>
                    <Button
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() => chats()}
                      title="Chat"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>
                  {data.datos.usuario.setVideoConsultas < 3 ? (
                    <View style={styles.signupButtonContainer}>
                      {Loading ? (
                        <ActivityIndicator />
                      ) : (
                        <Button
                          light={colors.white}
                          dark={colors.white}
                          containerStyle={styles.buttonView}
                          onPress={() => videoCall(mutation)}
                          title="Video llamada"
                          titleStyle={styles.buttonTitle}
                        />
                      )}
                    </View>
                  ) : (
                    <View style={styles.buttonView}>
                      <CustomText
                        style={styles.buttonTitle}
                        light={colors.white}
                        dark={colors.white}>
                        Llamadas agotada
                      </CustomText>
                    </View>
                  )}
                </View>
                <CustomText
                  style={[
                    stylesText.placeholderText,
                    {
                      textAlign: 'right',
                      marginRight: 40,
                    },
                  ]}
                  light={colors.black}
                  dark={colors.white}>
                  Haz realizado {data.datos.usuario.setVideoConsultas} de / 3
                  llamadas
                </CustomText>
                <View>
                  <View
                    style={{
                      marginTop: dimensions.Height(2),
                      height: 'auto',
                      marginBottom: dimensions.Height(10),
                      alignSelf: 'center',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MyMacotas />
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      }}
    </Mutation>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  containers: {
    alignSelf: 'center',
    marginTop: dimensions.Height(3),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(25),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: dimensions.Width(100),
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: dimensions.Height(5),
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 20,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(0),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 140,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(0),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
    width: 170,
    textAlign: 'center',
    marginTop: dimensions.Height(1),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },

  buttonView: {
    backgroundColor: colors.rgb_153,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
    margin: 10,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(0),
  },

  name: {
    fontSize: dimensions.FontSize(16),
    marginLeft: 10,
    textAlign: 'center',
  },

  coninfo: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  avatarst: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 4,
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  suscripcion: {
    marginTop: dimensions.Height(2),

    width: dimensions.Width(100),
  },
  btncont: {
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.ERROR, colors.ERROR),
    borderWidth: 1,
    borderColor: colors.ERROR,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
    fontWeight: '200',
  },
});

const mapStateToProps = ({auth, users, user}, {exclude = []}) => {
  console.log('usersusers', users);
  return {
    qbUsers:
      (users &&
        users.users
          .filter(smuser => (auth.user ? smuser.id !== auth.user.id : true))
          .filter(smuser => exclude.indexOf(smuser.id) === -1)) ||
      [],
    user: user.user,
  };
};

const mapDispatchToProps = {
  call: webrtcCall,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Myplus);
