import React from 'react';
import {View, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {CustomText} from './../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {colors, dimensions} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {stylesText} from '../components/StyleText';

const InfoSus = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{marginBottom: dimensions.Height(5)}}>
      <View style={styles.viewBox}>
        <Icon
          name="dog"
          type="MaterialCommunityIcons"
          size={40}
          style={styles.icons}
        />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Mascotas ilimitadas, todas las que quieras
        </CustomText>
      </View>

      <View style={styles.viewBox}>
        <Icon
          name="checkcircleo"
          type="AntDesign"
          size={40}
          style={styles.icons}
        />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Verificación de la salud de tu mascota online
        </CustomText>
      </View>
      <View style={styles.viewBox}>
        <Icon name="message1" type="AntDesign" size={40} style={styles.icons} />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Consultas a través del chat ilimitada
        </CustomText>
      </View>
      <View style={styles.viewBox}>
        <Icon
          name="videocamera"
          type="AntDesign"
          size={40}
          style={styles.icons}
        />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Hasta 3 consultas de Video llamadas
        </CustomText>
      </View>
      <View style={styles.viewBox}>
        <Icon name="hearto" type="AntDesign" size={40} style={styles.icons} />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Medicamentos para pulgas, garrapatas, y paracitos
        </CustomText>
      </View>
      <View style={styles.viewBox}>
        <Icon
          name="solution1"
          type="AntDesign"
          size={40}
          style={styles.icons}
        />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Acesoramiento personalizado
        </CustomText>
      </View>
      <View style={styles.viewBox}>
        <Icon
          name="closecircleo"
          type="AntDesign"
          size={40}
          style={styles.icons}
        />
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 5,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Sin compromiso, cancela cuando quieras
        </CustomText>
      </View>
      {props.plus ? (
        <View
          style={{
            marginTop: dimensions.Height(2),
            marginBottom: dimensions.Height(3),
          }}>
          <TouchableOpacity
            style={styles.btncont}
            onPress={() => {
              Alert.alert(
                '¿Estás seguro que deseas cancelar tu suscripción?',
                'Al cancelar la suscripción pierdes todos los veneficios de Vetec PLUS',
                [
                  {
                    text: 'Ok',
                    onPress: () => console.log('ok'),
                  },
                  {
                    text: 'Cancelar suscripción',
                    onPress: () => props.calcelarsus(),
                  },
                ],

                {cancelable: false},
              );
            }}>
            <CustomText
              light={colors.white}
              dark={colors.white}
              style={styles.te}>
              Cancelar suscripción
            </CustomText>
          </TouchableOpacity>
        </View>
      ) : null}
    </ScrollView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(20),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: dimensions.Width(100),
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: dimensions.Height(5),
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 20,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(3),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },

  precio: {
    fontSize: 30,
    textAlign: 'center',
    marginTop: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(10),
    fontWeight: 'bold',
  },

  desmas: {
    marginTop: dimensions.Height(1),
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(5),
  },

  desmass: {
    marginTop: dimensions.Height(3),
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(5),
    marginBottom: dimensions.Height(20),
  },

  card: {
    marginTop: dimensions.Height(2),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.Width(95),
    height: 'auto',
    padding: dimensions.Height(2),
    borderRadius: 10,
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  btncont: {
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.ERROR, colors.ERROR),
    borderWidth: 1,
    borderColor: colors.ERROR,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
    fontWeight: '200',
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

export default InfoSus;
