import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
import Navigation from '../services/NavigationService';
import {dimensions, colors} from '../themes';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';

import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDynamicValue,
} from 'react-native-dark-mode';
import {image} from '../constants/image';

const lightLogo = image.LogoVetecDias;
const darkLogo = image.LogoVetecnoche;

function Splash({appStart}) {
  const logoUri = new DynamicValue(lightLogo, darkLogo);
  const source = useDynamicValue(logoUri);
  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    console.log("appStartappStart",appStart)
    appStart(QBConfig);
    const timer = setTimeout(() => {
      Navigation.navigate('Home');
      appStart(QBConfig);
    }, 4000);
    return () => clearTimeout(timer)
  }, [])

 

  return (
    <View style={styles.container}>
      <Image source={source} style={styles.imagen} />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  imagen: {
    width: 120,
    height: 160,
  },
});

export default connect(
  null,
  {appStart},
)(Splash);