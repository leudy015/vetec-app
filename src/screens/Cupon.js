import React from 'react';
import {
  View,
  ActivityIndicator,
  Image,
  Clipboard,
  TouchableOpacity,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import {ButtomBar} from './../components/ButtomBar';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {image} from '../constants/image';
import {GET_CUPON} from '../query';
import {Query} from 'react-apollo';
import Toast from 'react-native-simple-toast';

export default function Cupon() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Regalos y cupones"
          back={true}
        />
      </View>
      <View style={styles.contenedor} />
      <View style={{alignSelf: 'center'}}>
        <Image
          source={image.LandingImg}
          style={{width: 250, height: 250, alignSelf: 'center'}}
        />
        <Query query={GET_CUPON}>
          {(response, error, loading) => {
            if (loading) {
              return (
                <ActivityIndicator size="large" color={colors.green_main} />
              );
            }
            if (error) {
              return console.log('Response Error-------', error);
            }
            if (response) {
              const datos =
                response && response.data ? response.data.getCuponAll : '';

              const copiar = value => {
                Clipboard.setString(value);
                Toast.showWithGravity(
                  'Cupón copiado en el portapapeles',
                  Toast.LONG,
                  Toast.TOP,
                );
              };

              return (
                <View
                  style={{
                    marginTop: dimensions.Height(1),
                    alignSelf: 'center',
                    paddingHorizontal: dimensions.Width(4),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.white}
                    style={styles.des}>
                    Consigue un {datos.descuento}% de descuento con el código
                  </CustomText>
                  <TouchableOpacity onPress={() => copiar(datos.clave)}>
                    <View>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.clave}>
                        {datos.clave}
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.clave1}>
                        Pulsa para copiar
                      </CustomText>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }
          }}
        </Query>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  des: {
    fontSize: dimensions.FontSize(18),
    textAlign: 'center',
  },

  clave: {
    fontSize: dimensions.FontSize(18),
    textAlign: 'center',
    marginTop: dimensions.Height(4),
    borderWidth: 0.5,
    borderColor: colors.rgb_153,
    padding: dimensions.Height(1),
    borderRadius: 20,
  },

  clave1: {
    fontSize: dimensions.FontSize(12),
    textAlign: 'center',
    marginTop: dimensions.Height(1),
  },
});
