import React, {PureComponent} from 'react';
import {
  KeyboardAvoidingView,
  View,
  Platform,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import stripe from 'tipsi-stripe';
import {colors, dimensions} from './../themes';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';

const ContainerView = Platform.select({
  ios: KeyboardAvoidingView,
  android: View,
});

stripe.setOptions({
  publishableKey: 'pk_live_ZSnprUnfFoYZnuMSyv69szKq002HmxhXU8',
});

export default class CardTextFieldScreen extends PureComponent {
  static title = 'Guardar tarjeta';

  handleCardPayPress = async () => {
    try {
      const token = await stripe.paymentRequestWithCardForm({
        // Only iOS support this options
        smsAutofillDisabled: true,
        prefilledInformation: {
          billingAddress: {
            name: '',
            email: '',
            country: 'ES',
          },
        },
      });
      let response = await fetch(
        `${NETWORK_INTERFACE_LINK}/card-create?customers=${
          this.props.clientID
        }&token=${token.tokenId}`,
      );
      const secret = await response.json();
      if (secret) {
        this.props.getCard();
        const pagar = async () => {
          let paga = await fetch(
            NETWORK_INTERFACE_LINK +
              `/create-suscription?customers=${this.props.clientID}&UserID=${
                this.props.userID
              }`,
          );
          const procederalpago = await paga.json();
          console.log(procederalpago);
          if (procederalpago.success) {
            Navigation.navigate('Thanksuscriptions', {
              data: this.props.userID,
            });
          } else {
            Navigation.navigate('Error');
          }
        };
        pagar();
      }
    } catch (e) {
      console.log(e);
      //setLoading(false);
    }
  };

  render() {
    return (
      <ContainerView
        behavior="padding"
        style={styles.container}
        onResponderGrant={dismissKeyboard}
        onStartShouldSetResponder={() => true}>
        <TouchableOpacity
          style={styles.btncont}
          onPress={() => this.handleCardPayPress()}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            style={styles.te}>
            Añadir tarjeta
          </CustomText>
        </TouchableOpacity>
      </ContainerView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instruction: {
    textAlign: 'center',
    color: colors.main,
    marginBottom: 5,
  },
  token: {
    height: 20,
  },
  spoiler: {
    width: 300,
  },
  params: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    padding: 10,
    margin: 5,
  },
  field: {
    width: dimensions.Width(85),
    color: colors.main,
    borderColor: colors.rgb_153,
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'transparent',
    overflow: 'hidden',
  },

  btncont: {
    width: dimensions.Width(85),
    padding: dimensions.Height(2),
    backgroundColor: colors.main,
    borderWidth: 1,
    borderColor: colors.main,
    borderRadius: dimensions.Width(10),
    marginTop: dimensions.Height(3),
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
});
