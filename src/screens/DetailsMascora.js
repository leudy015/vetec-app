import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import moment from 'moment';
import {LineDotsLoader} from 'react-native-indicator';
import {GET_DIAGNOSTICO} from '../query/index';
import {Query} from 'react-apollo';
import NoData from '../components/NoData';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {stylesText} from '../components/StyleText';

const window = Dimensions.get('window');

export default function DetailsPro() {
  const [line, setLine] = useState(4);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const _renderIntem = ({item}) => {
    return (
      <View style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.console}>
            <Image
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar,
              }}
              style={styles.avatarpro}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: dimensions.Width(0)}}>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={stylesText.mainText}>
                {item.profesional.nombre} {item.profesional.apellidos}
              </CustomText>
              <CustomText
                numberOfLines={1}
                style={[
                  stylesText.secondaryText,
                  {color: colors.main, marginTop: 5},
                ]}>
                {item.profesional.profesion}
              </CustomText>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {color: colors.rgb_153, marginTop: 5},
                ]}>
                {moment(Number(item.created_at)).format('LL')}
              </CustomText>
            </View>
          </View>
        </View>
        <View
          style={{
            paddingHorizontal: dimensions.Width(2),
          }}>
          <CustomText
            numberOfLines={line}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={stylesText.secondaryText}>
            {item.diagnostico}
          </CustomText>
          {line === 4 ? (
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => setLine(100)}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText]}>
                Ver más
              </CustomText>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => setLine(4)}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText]}>
                Ver menos
              </CustomText>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              blurRadius={10}
              style={styles.background}
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + data.avatar}}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + data.avatar}}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <Icon
                  name="close"
                  type="AntDesign"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.stickySectionText}>
              {data.name}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(2),
              marginBottom: dimensions.Height(4),
              paddingHorizontal: dimensions.Height(2),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.light_white}
              style={stylesText.h1}>
              {data.name}
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.secondaryText}>
              {moment(data.age).format('ll')}
            </CustomText>
          </View>

          <View style={{justifyContent: 'center', alignSelf: 'center'}}>
            <View style={styles.item}>
              <Icon
                name="dog"
                type="MaterialCommunityIcons"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                {data.especie}
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="tago"
                type="AntDesign"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                {data.raza}
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="transgender"
                type="FontAwesome"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                {data.genero}
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>

            <View style={styles.item}>
              <Icon
                name="weight-kilogram"
                type="MaterialCommunityIcons"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                {data.peso} kg
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="pushpin"
                type="AntDesign"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                {data.vacunas} (Vacunas)
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="closecircleo"
                type="AntDesign"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                Alergía a {data.alergias}
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="checksquareo"
                type="AntDesign"
                size={18}
                style={styles.icons}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 10}]}>
                Chip {data.microchip ? data.microchip : 'No registrado'}
              </CustomText>
              <Icon
                name="check"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
          </View>
          <View style={styles.consul}>
            <View
              style={{
                marginTop: dimensions.Height(3),
                marginBottom: dimensions.Height(5),
              }}>
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.mainText, {marginLeft: 15}]}>
                Historial de consultas
              </CustomText>
              <Query query={GET_DIAGNOSTICO} variables={{id: data.id}}>
                {({loading, error, data, refetch}) => {
                  refetch = refetch;
                  if (loading) {
                    return <LineDotsLoader color={colors.main} />;
                  }
                  if (error) {
                    return console.log('error in mascota', error);
                  }
                  if (data) {
                    return (
                      <FlatList
                        data={
                          data && data.getDiagnostico
                            ? data.getDiagnostico.list
                            : ''
                        }
                        renderItem={item => _renderIntem(item)}
                        keyExtractor={item => item.profesional}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        ListEmptyComponent={
                          <NoData menssge="Aún no tenemos ningún historial" />
                        }
                      />
                    );
                  } else {
                    return <LineDotsLoader color={colors.main} />;
                  }
                }}
              </Query>
            </View>
          </View>
        </View>
      </ParallaxScrollView>
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },

  btncont: {
    width: dimensions.Width(91),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.main),
    borderWidth: 1,
    borderColor: colors.main,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 50,
      },
      {
        left: 45,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
  },

  fixedSectionText1: {
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },

  avatarpro: {
    borderRadius: 15,
    width: 80,
    height: 80,
  },

  console: {
    width: 90,
    height: 90,
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.Height(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  title: {
    fontSize: dimensions.FontSize(28),
    fontWeight: 'bold',
    maxWidth: dimensions.Width(50),
    width: 'auto',
  },

  name_pro: {
    fontSize: dimensions.FontSize(18),
    fontWeight: 'bold',
    width: 'auto',
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  item: {
    width: dimensions.Width(95),
    height: 60,
    marginTop: dimensions.Height(2),
    paddingHorizontal: dimensions.Height(2),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    alignContent: 'center',
    alignItems: 'center',
  },

  sub_item_text: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '300',
    marginLeft: 10,
  },

  icons: {
    color: new DynamicValue(colors.rgb_153, colors.rgb_153),
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
  },

  card: {
    width: 370,
    height: 'auto',
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 3},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },
});
