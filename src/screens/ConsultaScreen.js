import React, {useState} from 'react';
import {View, SafeAreaView, TouchableOpacity, Alert, Modal} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {CustomText} from '../components/CustomText';
import {Avatar} from 'react-native-elements';
import {useNavigationParam} from 'react-navigation-hooks';
import {CustomTextInput} from './../components/CustomTextInput';
import {Button} from './../components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import _get from 'lodash.get';
import {CREAR_MODIFICAR_CONSULTA} from '../mutations';
import {useMutation} from 'react-apollo';
import Toast from 'react-native-simple-toast';
import {Switch} from '@ant-design/react-native';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import moment from 'moment';
import Informacion from '../components/informacion';
import WebViweb from '../components/Webview';
import DropDownItem from 'react-native-drop-down-item';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function ConsultaScreen() {
  const [clave, setClave] = useState('');
  const [descuento, setDescuento] = useState({
    clave: '',
    descuento: '',
    tipo: '',
  });
  const [ModalVisible, setModalVisible] = useState(false);
  const [aceptaTerminos, setAceptaTerminos] = useState(false);
  const [visibleModal, setvisibleModal] = useState(false);
  const [crearModificarConsulta] = useMutation(CREAR_MODIFICAR_CONSULTA);

  const IC_ARR_DOWN = require('./icons/ic_arr_down.png');
  const IC_ARR_UP = require('./icons/ic_arr_up.png');

  const abrirweb = () => {
    setvisibleModal(true);
  };

  const cerrarweb = () => {
    setvisibleModal(false);
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  const onSwitchChange = () => {
    if (aceptaTerminos === true) {
      setAceptaTerminos(false);
    } else {
      setAceptaTerminos(true);
    }
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const data = useNavigationParam('data');

  const noTerm = () => {
    if (aceptaTerminos === false) {
      Alert.alert(
        'Debes aceptar los términos y condiciones',
        'Para continuar con el pago debes confirmar que has leído y que estas de acuerdo con los términos y condiciones de uso de Vetec',
        [
          {
            text: 'Aceptar términos',
            onPress: () => setAceptaTerminos(true),
          },
        ],
        {cancelable: false},
      );
      setClave('');
      return null;
    }
  };

  const handleSubmit = () => {
    const input = {
      id: data.id,
      usuario: data.usuario.id,
      profesional: data.profesional.id,
      clave: clave,
    };
    console.log(input);
    crearModificarConsulta({
      variables: {
        input: input,
      },
    }).then(res => {
      if (res.data.crearModificarConsulta.success) {
        Toast.showWithGravity('Cupón añadido con éxito', Toast.LONG, Toast.TOP);
        const desc =
          res &&
          res.data &&
          res.data.crearModificarConsulta &&
          res.data.crearModificarConsulta.data
            ? res.data.crearModificarConsulta.data.descuento
            : '';
        setDescuento({
          descuento: desc,
        });
        Navigation.navigate('PaymentPage', {data: res});
        setClave('');
      } else {
        Toast.showWithGravity(
          res.data.crearModificarConsulta.message,
          Toast.LONG,
          Toast.TOP,
        );
      }
    });
  };

  const guardarOrden = () => {
    const input = {
      id: data.id,
      aceptaTerminos: aceptaTerminos,
    };
    console.log('saved order modified', aceptaTerminos);
    crearModificarConsulta({variables: {input: input}})
      .then(res => {
        if (res.data.crearModificarConsulta.success) {
          Navigation.navigate('PaymentPage', {data: res});
        }
      })
      .catch(err => {
        console.log('err', err);
      });
  };

  const valorDescuento = descuento.descuento
    ? descuento.descuento.descuento
    : '';
  const tipo = descuento.descuento ? descuento.descuento.tipo : '';
  const subtotal = data.profesional.Precio * data.cantidad;
  let displayDescuento = `0€`;
  let descuentoFinal = 0;
  let total = subtotal;
  if (valorDescuento && tipo) {
    switch (tipo) {
      case 'dinero':
        displayDescuento = `${valorDescuento}€`;
        total = subtotal - valorDescuento;
        break;
      case 'porcentaje':
        displayDescuento = `${valorDescuento}%`;
        descuentoFinal = valorDescuento / 100;
        total = subtotal - subtotal * descuentoFinal;
        break;
    }
  }

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => Navigation.goBack(null)}
            style={{marginLeft: 5}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.title}>
            Completar el Pago
          </CustomText>
          <TouchableOpacity
            onPress={() => openModal()}
            style={{marginLeft: 'auto', marginRight: 15}}>
            <Icon
              name="questioncircleo"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <Modal
            animationType="slide"
            transparent={false}
            presentationStyle="formSheet"
            visible={ModalVisible}>
            <SafeAreaView style={styles.headers1}>
              <View style={{flexDirection: 'row', marginTop: 20}}>
                <View style={{alignItems: 'flex-start', marginLeft: 10}} />
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => setModalVisible(!ModalVisible)}>
                    <CustomText style={{marginRight: 20}}>
                      <Icon
                        type="AntDesign"
                        name="close"
                        size={25}
                        color={colors.green_main}
                      />
                    </CustomText>
                  </TouchableOpacity>
                </View>
              </View>
            </SafeAreaView>
            <Informacion />
          </Modal>
        </View>
      </SafeAreaView>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        style={{paddingHorizontal: dimensions.Width(4)}}>
        <View
          style={{
            height: 'auto',
            width: dimensions.Width(92),
            borderBottomColor: colors.rgb_153,
            borderBottomWidth: 0.3,
            borderTopColor: colors.rgb_153,
            borderTopWidth: 0.3,
            paddingHorizontal: dimensions.Width(2),
            flexDirection: 'row',
            paddingBottom: dimensions.Height(2),
          }}>
          <Avatar
            rounded
            size={40}
            source={{
              uri: NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
            }}
            containerStyle={{marginTop: 20}}
          />
          <View>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.white}
                style={styles.name}>
                {data.profesional.nombre} {data.profesional.apellidos}
              </CustomText>
              {data.profesional.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={14}
                  style={{
                    alignSelf: 'center',
                    marginTop: 20,
                    marginLeft: 10,
                    color: colors.main,
                  }}
                />
              ) : null}
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={{marginLeft: dimensions.Width(3), marginTop: 3}}>
              {data.profesional.profesion}
            </CustomText>
          </View>
        </View>

        <View
          style={{
            marginTop: 20,
            paddingBottom: 10,
          }}>
          <DropDownItem
            style={styles.dropDownItem}
            contentVisible={false}
            invisibleImage={IC_ARR_DOWN}
            visibleImage={IC_ARR_UP}
            header={
              <View style={{width: dimensions.Width(75)}}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.name11}>
                  Total a pagar
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.name111}>
                  {total}€
                </CustomText>
              </View>
            }>
            <View
              style={{
                flexDirection: 'row',
                marginRight: 10,
                width: dimensions.Width(80),
              }}>
              <View
                style={{
                  marginRight: 10,
                }}>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  Precio unitario
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  Cantidad
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  Subtotal
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  Descuento
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  Total a pagar
                </CustomText>
              </View>
              <View style={{marginLeft: 'auto'}}>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  {data.profesional.Precio}€
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  {data.cantidad}
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  {subtotal}€
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  {displayDescuento}
                </CustomText>
                <CustomText
                  style={{
                    fontSize: 14,
                    color: colors.rgb_102,
                    marginTop: 10,
                  }}>
                  {total}€
                </CustomText>
              </View>
            </View>
          </DropDownItem>
        </View>
        <View>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={{fontWeight: '200', marginTop: 30}}>
            Mascota
          </CustomText>

          <View style={styles.cardinfo}>
            <View
              style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
              <Avatar
                rounded
                size={50}
                source={{
                  uri: NETWORK_INTERFACE_LINK_AVATAR + data.mascota.avatar,
                }}
                containerStyle={styles.avatar}
              />
            </View>
            <View
              style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={styles.name1}>
                {data.mascota.name}
              </CustomText>
              <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                {moment(data.mascota.age).format('ll')}
              </CustomText>
            </View>
            <View
              style={{
                alignSelf: 'center',
                marginLeft: 'auto',
                marginRight: dimensions.Width(4),
              }}>
              <Icon
                name="checkcircle"
                type="AntDesign"
                size={20}
                color="#90C33C"
              />
            </View>
          </View>
        </View>

        <View
          style={{
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: colors.rgb_235,
            paddingBottom: 20,
          }}>
          <View style={{marginTop: 22}}>
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps="always"
              showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: dimensions.Height(2),
                }}>
                <View>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.light_white}
                    style={{width: dimensions.Width(70)}}>
                    Confirmo que he leído y acepto el aviso legal sobre
                    protección de datos personales y los
                  </CustomText>
                  <TouchableOpacity onPress={() => abrirweb()}>
                    <CustomText light={colors.main} dark={colors.main}>
                      Téminos y condiciones
                    </CustomText>
                  </TouchableOpacity>
                  <WebViweb
                    url="https://vetec.es/condiciones"
                    visibleModal={visibleModal}
                    cerrarweb={cerrarweb}
                  />
                </View>
                <Switch
                  checked={aceptaTerminos}
                  onChange={() => onSwitchChange()}
                  style={{marginLeft: 'auto'}}
                  trackColor={colors.main1}
                />
              </View>

              {aceptaTerminos === true ? (
                <View
                  style={{
                    marginTop: dimensions.Height(4),
                    marginBottom: dimensions.Height(2),
                  }}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={styles.buttonView}
                    onPress={() => guardarOrden()}
                    title="Continual al pago"
                    titleStyle={styles.buttonTitle}
                  />
                </View>
              ) : (
                <View style={{marginTop: dimensions.Height(2)}}>
                  <TouchableOpacity
                    style={styles.btncont}
                    onPress={() => noTerm()}>
                    <CustomText
                      light={colors.black}
                      dark={colors.white}
                      style={styles.te}>
                      Continuar al pago
                    </CustomText>
                  </TouchableOpacity>
                </View>
              )}

              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontWeight: '200', marginTop: 30}}>
                ¿Tienes un cupón?
              </CustomText>

              <View>
                <CustomTextInput
                  containerStyle={{marginTop: dimensions.Height(1)}}
                  onChangeText={text => setClave(text)}
                  keyboardType="default"
                  placeholder="Cupón"
                  onPress={() => handleSubmit()}
                />
              </View>
            </KeyboardAwareScrollView>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        paddingTop: 0,
      },
      {
        paddingTop: 30,
      },
    ),
  },

  headers1: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(10),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },
  name: {
    fontSize: dimensions.FontSize(18),
    marginTop: dimensions.Height(2.5),
    marginLeft: dimensions.Width(3),
    width: 'auto',
    maxWidth: dimensions.Width(50),
  },

  name11: {
    fontSize: dimensions.FontSize(18),
    marginLeft: dimensions.Width(3),
    width: dimensions.Width(70),
    height: 30,
  },

  name111: {
    fontSize: dimensions.FontSize(20),
    marginLeft: 'auto',
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(-3),
      },
      {
        marginTop: dimensions.Height(-4),
      },
    ),
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 80,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginRight: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },

  cardtarjeta1: {
    color: '#52c41a',
    width: 300,
    height: 60,
    backgroundColor: '#b7eb8f',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    paddingLeft: 30,
    paddingRight: 30,
    alignSelf: 'center',
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(0),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
  btncont: {
    width: dimensions.Width(85),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.rgb_235, colors.rgb_153),
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },
  te: {
    textAlign: 'center',
    fontWeight: '300',
    fontSize: dimensions.FontSize(18),
  },
});

export default ConsultaScreen;
