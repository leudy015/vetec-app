import gql from 'graphql-tag';

export const USER_DETAIL = gql`
  query getUsuario($id: ID!) {
    getUsuario(id: $id) {
      id
      email
      UserID
      nombre
      apellidos
      ciudad
      telefono
      avatar
      notificacion
      StripeID
      isPlus
      connected
      lastTime
      setVideoConsultas
      suscription {
        id
      }
    }
  }
`;

export const GET_CONVERSATION_CLIENT = gql`
  query getConversationclient($userId: ID!) {
    getConversationclient(userId: $userId) {
      message
      success
      conversation {
        _id
        user
        prof
        profesional {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          anadidoFavorito
          notificacion
          isProfesional
          isVerified
          isAvailable
          connected
          lastTime
          professionalRatinglist {
            id
          }
        }
        usuario {
          id
          email
          UserID
          nombre
          apellidos
          ciudad
          telefono
          avatar
          isPlus
          notificacion
          StripeID
          connected
          lastTime
        }
        messagechat {
          _id
          text
          image
          read
          createdAt
          user {
            _id
            name
            avatar
          }
        }
      }
    }
  }
`;

export const GET_MESSAGE_NO_READ = gql`
  query getMessageNoReadClient($conversationID: ID!, $profId: ID!) {
    getMessageNoReadClient(conversationID: $conversationID, profId: $profId) {
      message
      success
      conversation {
        _id
        count
      }
    }
  }
`;

export const GET_CATEGORIES = gql`
  query {
    getCategories {
      id
      title
      image
      font
      description
      profesionales {
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
        profesion
        experiencia
        descricion
        Precio
        anadidoFavorito
        notificacion
        isProfesional
        isVerified
        isAvailable
        connected
        lastTime
        professionalRatinglist {
          id
          rate
          updated_at
          coment
          customer {
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
            connected
            lastTime
          }
        }
        consultas {
          id
        }
      }
    }
  }
`;

export const USUARIO_FAVORITO_PROFESIONAL = gql`
  query getUsuarioFavoritoPro($id: ID!) {
    getUsuarioFavoritoPro(id: $id) {
      success
      message
      list {
        id
        profesionalId
        usuarioId
        profesional {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          notificacion
          isProfesional
          isVerified
          isAvailable
          anadidoFavorito
          connected
          lastTime
          professionalRatinglist {
            id
            customer {
              id
              email
              nombre
              apellidos
              ciudad
              telefono
              avatar
              isPlus
              UserID
            }
            rate
            updated_at
            coment
          }
          consultas {
            id
          }
        }
      }
    }
  }
`;

export const USUARIO_PLUS = gql`
  query getUserPlus($id: ID!) {
    getUserPlus(id: $id) {
      message
      success
      data {
        usuario {
          id
          email
          UserID
          nombre
          apellidos
          ciudad
          telefono
          avatar
          notificacion
          setVideoConsultas
          StripeID
          isPlus
          connected
          lastTime
          suscription {
            id
            current_period_end
            current_period_start
            status
            livemode
            trial_end
            trial_start
          }
        }
        profesional {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          notificacion
          isProfesional
          isVerified
          isAvailable
          anadidoFavorito
          connected
          lastTime
          professionalRatinglist {
            id
            customer {
              id
              email
              nombre
              apellidos
              ciudad
              telefono
              avatar
              UserID
            }
            rate
            updated_at
            coment
          }
          consultas {
            id
          }
        }
      }
    }
  }
`;

export const GET_MASCOTAS = gql`
  query getMascota($usuarios: ID!) {
    getMascota(usuarios: $usuarios) {
      message
      success
      list {
        id
        name
        age
        usuario
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    }
  }
`;

export const GET_PROFEISONAL = gql`
  {
    getProfesionalall {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      avatar
      UserID
      profesion
      experiencia
      descricion
      Precio
      anadidoFavorito
      notificacion
      isProfesional
      isVerified
      isAvailable
      connected
      lastTime
      professionalRatinglist {
        id
        customer {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
        }
        rate
        updated_at
        coment
      }
      consultas {
        id
      }
    }
  }
`;

export const GET_PROFEISONAL_SUSCRIPCION = gql`
  {
    getProfesionaforSuscribcion {
      id
      email
      nombre
      apellidos
      ciudad
      telefono
      avatar
      UserID
      profesion
      experiencia
      descricion
      Precio
      anadidoFavorito
      notificacion
      isProfesional
      isVerified
      isAvailable
      connected
      lastTime
      professionalRatinglist {
        id
        customer {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
        }
        rate
        updated_at
        coment
      }
      consultas {
        id
      }
    }
  }
`;

export const GET_PROFESIONAL_CITY = gql`
  query getProfesionalCity($city: String) {
    getProfesionalCity(city: $city) {
      message
      success
      data {
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        anadidoFavorito
        isVerified
        isAvailable
        connected
        lastTime
        professionalRatinglist {
          id
          customer {
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
          }
          rate
          updated_at
          coment
        }
        consultas {
          id
        }
      }
    }
  }
`;

export const GET_PROFESIONAL_SEARCH = gql`
  query getProfesionalSearch($search: String) {
    getProfesionalSearch(search: $search) {
      message
      success
      data {
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        UserID
        avatar
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        isVerified
        anadidoFavorito
        isAvailable
        connected
        lastTime
        professionalRatinglist {
          id
          customer {
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
          }
          rate
          updated_at
          coment
        }
        consultas {
          id
        }
      }
    }
  }
`;

export const CONSULTA_BY_USUARIO = gql`
  query getConsultaByUsuario($usuarios: ID!, $dateRange: DateRangeInput) {
    getConsultaByUsuario(usuarios: $usuarios, dateRange: $dateRange) {
      success
      message
      list {
        id
        time
        estado
        nota
        endDate
        created_at
        mascota {
          id
          name
          age
          usuario
          avatar
          especie
          raza
          genero
          peso
          microchip
          vacunas
          alergias
          castrado
        }
        usuario {
          id
          email
          UserID
          nombre
          apellidos
          ciudad
          telefono
          avatar
          isPlus
          notificacion
          StripeID
          connected
          lastTime
        }
        profesional {
          id
          email
          nombre
          apellidos
          UserID
          ciudad
          telefono
          avatar
          profesion
          experiencia
          descricion
          Precio
          anadidoFavorito
          notificacion
          isProfesional
          isVerified
          isAvailable
          connected
          lastTime
          professionalRatinglist {
            id
            customer {
              id
              email
              nombre
              apellidos
              ciudad
              telefono
              avatar
              UserID
            }
            rate
            updated_at
            coment
          }
        }
      }
    }
  }
`;

export const GET_NOTIFICATIONS = gql`
  query getNotifications($Id: ID!) {
    getNotifications(Id: $Id) {
      success
      message
      notifications {
        _id
        type
        read
        createdAt
        user
        profesional {
          id
          nombre
          apellidos
          avatar
          anadidoFavorito
        }
        usuario {
          id
          nombre
          apellidos
          avatar
        }
      }
    }
  }
`;

export const GET_CUPON = gql`
  {
    getCuponAll {
      id
      clave
      tipo
      descuento
    }
  }
`;

export const GET_DIAGNOSTICO = gql`
  query getDiagnostico($id: ID!) {
    getDiagnostico(id: $id) {
      message
      success
      list {
        _id
        profesional {
          nombre
          apellidos
          profesion
          avatar
        }
        diagnostico
        mascota
        created_at
      }
    }
  }
`;
