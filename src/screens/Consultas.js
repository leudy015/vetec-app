import React, {useState} from 'react';
import {View, ScrollView, RefreshControl} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Consulta from '../components/Consultas';
import {CustomText} from '../components/CustomText';
import {stylesText} from '../components/StyleText';

export default function Consultas() {
  const [refreshing, setRefreshing] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Consultas" back={true} />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={[stylesText.h1, {marginLeft: 15, marginBottom: 20}]}>
        Consultas
      </CustomText>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Consulta navigation={Navigation} />
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
});
