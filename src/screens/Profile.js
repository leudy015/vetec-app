import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  Modal,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import AsyncStorage from '@react-native-community/async-storage';
import {Query} from 'react-apollo';
import {USER_DETAIL, USUARIO_PLUS} from '../query';
import {useNavigationParam} from 'react-navigation-hooks';
import {Avatar} from 'react-native-elements';
import Icon from 'react-native-dynamic-vector-icons';
import Mascota from '../components/Mascota';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import AddMascota from './AddMascota';
import PlaceHolder from './../components/PlaceholderProfile';
import {IconPlus} from './../components/iconplus';
import {logoutRequest} from '../actionCreators';
import {connect} from 'react-redux';
import {usersGet} from '../actionCreators';
import {stylesText} from '../components/StyleText';

function Profile({logoutRequest, getUsers}) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [modalVisible, setModalVisible] = useState(false);
  const [userData, setUserData] = useState({
    id: '',
    nombre: '',
    apellidos: '',
    email: '',
  });

  const openModal = () => {
    setModalVisible(true);
  };

  useEffect(() => {
    getUsers();
  });

  const logout = async () => {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('id');
    logoutRequest();
    Navigation.navigate('Landing');
  };

  const datass = useNavigationParam('data');

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title={`¡Hola, ${userData.nombre}!`}
          back={true}
          atrasprofile={true}
        />
      </View>
      <Query query={USER_DETAIL} variables={{id: datass}}>
        {({loading, error, data, refetch}) => {
          refetch();
          if (error) {
            console.log('Response Error-------', error);
            return <PlaceHolder />;
          }
          if (loading) {
            return <PlaceHolder />;
          }
          if (data) {
            const userDatas = data.getUsuario;
            setUserData(userDatas);
            return (
              <ScrollView
                showsVerticalScrollIndicator={false}
                style={{paddingHorizontal: 15}}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={stylesText.h1}>
                  Cuenta
                </CustomText>

                <View
                  style={{
                    marginTop: dimensions.Height(3),
                    marginBottom: dimensions.Height(15),
                  }}>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('UpdateProfile', {
                        data: userDatas,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Avatar
                        rounded
                        size={30}
                        source={{
                          uri: NETWORK_INTERFACE_LINK_AVATAR + userDatas.avatar,
                        }}
                        containerStyle={styles.avatar}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        {userData.nombre} {userData.apellidos}
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Edita tus datos del perfil
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('Chats', {
                        data: userDatas.id,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="message1"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Chats
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Conversaciones con tus vets
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('Consultas', {
                        data: userDatas,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="book"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Consultas
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Revisa tus consultas
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={styles.separator} />
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('MyPayment', {
                        data: userData,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="creditcard"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Método de pago
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Administra tus métodos de pago
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => openModal()}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="pluscircleo"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Añadir mascota
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Añade una mascota
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.white}
                    style={{
                      paddingHorizontal: dimensions.Width(2),
                      marginTop: dimensions.Height(3),
                    }}>
                    Mis Mascotas
                  </CustomText>
                  <View style={{marginTop: dimensions.Height(3)}}>
                    <Mascota navigation={Navigation} />
                  </View>
                  <View style={styles.separator} />
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('MayFavorites', {
                        data: userData.id,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="hearto"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Favoritos
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Mis Vet favoritos
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>

                  {userData.isPlus ? (
                    <Query query={USUARIO_PLUS} variables={{id: datass}}>
                      {(response, error, loading) => {
                        if (loading) {
                          return (
                            <ActivityIndicator
                              size="large"
                              color={colors.green_main}
                            />
                          );
                        }
                        if (error) {
                          return console.log('Response Error-------', error);
                        }
                        if (response) {
                          console.log(response);
                          const datos =
                            response &&
                            response.data &&
                            response.data.getUserPlus
                              ? response.data.getUserPlus.data
                              : '';

                          const ref = response.refetch;

                          const items = {
                            datos,
                            ref,
                          };
                          return (
                            <TouchableOpacity
                              style={styles.cardinfo}
                              onPress={() =>
                                Navigation.navigate('MyareaPlus', {
                                  data: items,
                                })
                              }>
                              <View
                                style={{
                                  alignSelf: 'center',
                                  marginLeft: dimensions.Width(1),
                                }}>
                                <IconPlus />
                              </View>

                              <View
                                style={{
                                  alignSelf: 'center',
                                  marginLeft: dimensions.Width(4),
                                }}>
                                <CustomText
                                  light={colors.back_dark}
                                  dark={colors.white}
                                  style={stylesText.mainText}>
                                  Mi área PLUS
                                </CustomText>
                                <CustomText
                                  light={colors.back_dark}
                                  dark={colors.rgb_153}
                                  style={styles.sub}>
                                  Ver mis beneficios
                                </CustomText>
                              </View>

                              <View
                                style={{
                                  alignSelf: 'center',
                                  marginLeft: 'auto',
                                  marginRight: dimensions.Width(4),
                                }}>
                                <Icon
                                  name="right"
                                  type="AntDesign"
                                  size={15}
                                  color={colors.rgb_153}
                                />
                              </View>
                            </TouchableOpacity>
                          );
                        }
                      }}
                    </Query>
                  ) : (
                    <TouchableOpacity
                      style={styles.cardinfo}
                      onPress={() =>
                        Navigation.navigate('Vetecplus', {
                          data: userDatas,
                        })
                      }>
                      <View
                        style={{
                          alignSelf: 'center',
                        }}>
                        <IconPlus />
                      </View>

                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: dimensions.Width(4),
                        }}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.white}
                          style={stylesText.mainText}>
                          Vetec PLUS
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.sub}>
                          Descube las ventajas
                        </CustomText>
                      </View>

                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: 'auto',
                          marginRight: dimensions.Width(4),
                        }}>
                        <Icon
                          name="right"
                          type="AntDesign"
                          size={15}
                          color={colors.rgb_153}
                        />
                      </View>
                    </TouchableOpacity>
                  )}
                  <Modal
                    animationType="slide"
                    presentationStyle="formSheet"
                    transparent={false}
                    visible={modalVisible}
                    onRequestClose={() => {
                      Alert.alert('Modal has been closed.');
                    }}>
                    <View style={styles.containerModal}>
                      <SafeAreaView
                        style={{
                          width: dimensions.ScreenWidth,
                          height: dimensions.Height(6),
                        }}>
                        <TouchableOpacity
                          onPress={() => setModalVisible(false)}>
                          <View
                            style={{
                              alignSelf: 'center',
                              marginLeft: 'auto',
                              marginRight: dimensions.Width(4),
                              marginTop: dimensions.Height(2),
                            }}>
                            <Icon
                              name="close"
                              type="AntDesign"
                              size={25}
                              color={colors.ERROR}
                            />
                          </View>
                        </TouchableOpacity>
                      </SafeAreaView>
                      <AddMascota
                        id={datass}
                        setModalVisible={modalVisible =>
                          setModalVisible(modalVisible)
                        }
                      />
                    </View>
                  </Modal>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => Navigation.navigate('Cupon')}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="gift"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Cupones y regalos
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Mira lo que tenemos para ti
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={styles.separator} />
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => Navigation.navigate('Configuraciones')}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="setting"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Configuración y ayuda
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Configura tu privacidad
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.cardinfo,
                      {marginBottom: dimensions.Height(7)},
                    ]}
                    onPress={() =>
                      Alert.alert(
                        'Cerrar sesión',
                        '¿Seguro que quieres cerrar sesión?',
                        [
                          {
                            text: 'Cancelar',
                            onPress: () => console.log('calcel'),
                          },
                          {text: 'Si', onPress: () => logout()},
                        ],

                        {cancelable: false},
                      )
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="logout"
                        type="AntDesign"
                        size={30}
                        color={colors.ERROR}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={stylesText.mainText}>
                        Cerrar sesión
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}>
                        Te esperamos pronto en Vetec
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={15}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={{marginLeft: 'auto'}}>
                    Versión 1.1.7 Vetec® - ©2020
                  </CustomText>
                </View>
              </ScrollView>
            );
          }
        }}
      </Query>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },
  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  cardinfo: {
    width: dimensions.Width(93),
    height: 'auto',
    marginTop: dimensions.Height(4),
    flexDirection: 'row',
  },
  separator: {
    backgroundColor: new DynamicValue(colors.light_grey, colors.rgb_153),
    width: dimensions.Width(93),
    height: 0.5,
    marginTop: dimensions.Height(4),
  },

  cons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  avatarst: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 4,
  },

  name: {
    marginTop: dimensions.Height(1),
    alignSelf: 'center',
    fontSize: dimensions.FontSize(24),
  },

  name1: {
    fontSize: dimensions.FontSize(24),
  },

  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(5),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(1),
    alignSelf: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});

const mapDispatchToProps = {
  logoutRequest,
  getUsers: usersGet,
};

export default connect(
  null,
  mapDispatchToProps,
)(Profile);
