import React, {useState} from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors, dimensions} from './../themes';
import {CustomText} from './../components/CustomText';
import {withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {LineDotsLoader} from 'react-native-indicator';
import {stylesText} from '../components/StyleText';
import IntlPhoneInput from 'react-native-intl-phone-input';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import {useNavigationParam} from 'react-navigation-hooks';

function FerifyPhone() {
  const [phone, setPhone] = useState('');
  const [Loading, setLoading] = useState(false);
  const [isVerified, setisVerified] = useState(null);
  const data = useNavigationParam('data');

  console.log(data);

  const dynamicStyles = new DynamicStyleSheet({
    container: {
      flex: 1,
    },
    formView: {
      marginHorizontal: dimensions.Width(8),
      marginTop: dimensions.Height(4),
      marginBottom: dimensions.Height(15),
    },
    rememberMeView: {
      marginTop: dimensions.Height(2),
      flexDirection: 'row',
    },
    rememberText: {
      fontSize: dimensions.FontSize(18),
    },

    containerModal: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      height: dimensions.ScreenHeight,
      alignSelf: 'center',
    },

    phoneInputStyle: {
      backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    },

    impu: {
      backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
      borderRadius: 40,
      marginBottom: 40,
      padding: 20,
      marginTop: 55,
    },

    conu: {
      color: new DynamicValue(colors.back_dark, colors.light_white),
    },

    code: {color: new DynamicValue(colors.back_dark, colors.light_white)},

    bntc1: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      color: colors.rgb_153,
      marginTop: 15,
    },

    bntc: {
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      color: colors.rgb_153,
    },

    signupButtonContainer: {
      marginTop: dimensions.Height(5),
      alignSelf: 'center',
    },
    buttonView: {
      backgroundColor: isVerified
        ? new DynamicValue(colors.main, colors.main)
        : colors.rgb_153,
      width: dimensions.Width(84),
      borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(5),
      color: colors.white,
      fontWeight: '400',
      fontSize: dimensions.FontSize(17),
    },
    contenedor: {
      width: dimensions.ScreenWidth,
      height: dimensions.ScreenHeight,
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    },
    h1: {
      fontSize: dimensions.FontSize(30),
      fontWeight: 'bold',
      textAlign: 'left',
    },
  });

  const styles = useDynamicStyleSheet(dynamicStyles);

  const onChangeText = ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    setisVerified(isVerified);
    setPhone(dialCode + unmaskedPhoneNumber);
  };

  const VerifyPhone = async (phone, id) => {
    const datas = {
      phone,
      id,
      data,
    };
    setLoading(true);
    fetch(`${NETWORK_INTERFACE_LINK}/verify-phone?phone=${phone}`)
      .then(res => {
        if (res.ok) {
          console.log('>>>>>>>>>>>', res);
          setLoading(false);
          Navigation.navigate('Entrecode', {data: datas});
        } else {
          Alert.alert(
            'Demaciado intentos',
            'Haz hecho demaciado intentos vuelve a intentarlo en  10 minutos.',
            [
              {
                text: 'Volver a enviar',
                onPress: () => console.log('ok'),
              },
            ],

            {cancelable: false},
          );
          setLoading(false);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Verifica tu teléfono"
          back={true}
          rigth={true}
        />
      </View>
      <View style={styles.contenedor}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
          style={{marginBottom: dimensions.Height(10)}}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              marginLeft: dimensions.Width(5),
              marginBottom: dimensions.Height(1),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.h1}>
              Verifica tu
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.h1}>
              teléfono
            </CustomText>

            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, {marginTop: 30}]}>
              Te enviaremos un código para verificar tu teléfono.
            </CustomText>
          </View>
          <View style={styles.formView}>
            <IntlPhoneInput
              filterInputStyle={styles.impu}
              dialCodeTextStyle={styles.code}
              closeButtonStyle={styles.bntc}
              searchIconStyle={styles.bntc1}
              containerStyle={styles.phoneInputStyle}
              modalCountryItemCountryNameStyle={styles.conu}
              modalContainer={styles.containerModal}
              defaultCountry="ES"
              lang="ES"
              closeText="Cerrar"
              filterText="Buscar"
              onChangeText={onChangeText}
            />
            <View style={styles.signupButtonContainer}>
              {Loading ? (
                <LineDotsLoader color={colors.main} />
              ) : (
                <TouchableOpacity
                  style={styles.buttonView}
                  onPress={() =>
                    isVerified ? VerifyPhone(phone, data.id) : ''
                  }>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.buttonTitle}>
                    Enviar código
                  </CustomText>
                </TouchableOpacity>
              )}
            </View>
            <View />
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
}

export default withApollo(FerifyPhone);
