import React, {useState} from 'react';
import {View, ScrollView, RefreshControl} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions} from '../themes/dimensions';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import Headers from '../components/Header';
import {CustomText} from '../components/CustomText';
import {useNavigationParam} from 'react-navigation-hooks';
import Profesional from '../components/Profesional';
import {stylesText} from '../components/StyleText';

export default function CategoriaPro() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [refreshing, setRefreshing] = useState(false);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const data = useNavigationParam('data');

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title={data.title} back={true} />
      </View>
      <View style={styles.contenedor} />
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            paddingTop: dimensions.Height(2),
          }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={[stylesText.h1, {marginLeft: 15}]}>
            {data.title}
          </CustomText>
          <CustomText
            light={colors.back_dark}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, {marginLeft: 15, marginTop: 10}]}>
            ({data.profesionales.length}) resultado para esta categoría
          </CustomText>
        </View>
        <View
          style={{
            paddingTop: dimensions.Height(2),
            marginBottom: dimensions.Height(5),
          }}>
          <Profesional navigation={Navigation} data={data.profesionales} />
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});
