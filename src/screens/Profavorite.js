import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  RefreshControl,
  Vibration,
} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions} from '../themes/dimensions';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import Headers from '../components/Header';
import {CustomText} from '../components/CustomText';
import NoData from './../components/NoData';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import Icon from 'react-native-dynamic-vector-icons';
import {Button} from './../components/Button';
import AsyncStorage from '@react-native-community/async-storage';
import {ELIMINAR_USUARIO_FAVORITE} from '../mutations';
import Toast from 'react-native-simple-toast';
import {Mutation, Query} from 'react-apollo';
import {USUARIO_FAVORITO_PROFESIONAL} from '../query';
import LoadingPLace from './../components/Placeholder';
import {stylesText} from '../components/StyleText';
import {Badge} from 'react-native-elements';

export default function CategoriaPro() {
  const [refreshing, setRefreshing] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [id, setId] = useState(null);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const ONE_SECOND_IN_MS = 100;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  const _renderItem = ({item}, refetch) => {
    const isNavigation = () => {
      if (id) {
        Navigation.navigate('SelecMascota', {data: item});
      } else {
        Alert.alert(
          'Para continuar debes iniciar sesión',
          'Debes iniciar sesión para continuar con la contratación',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
      }
    };

    let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
    item.profesional.professionalRatinglist.forEach(start => {
      if (start.rate === 1) rating['1'] += 1;
      else if (start.rate === 2) rating['2'] += 1;
      else if (start.rate === 3) rating['3'] += 1;
      else if (start.rate === 4) rating['4'] += 1;
      else if (start.rate === 5) rating['5'] += 1;
    });

    const ar =
      (5 * rating['5'] +
        4 * rating['4'] +
        3 * rating['3'] +
        2 * rating['2'] +
        1 * rating['1']) /
      item.profesional.professionalRatinglist.length;
    let averageRating = 0;
    if (item.profesional.professionalRatinglist.length) {
      averageRating = ar.toFixed(1);
    }

    const favouritesDelete = async (eliminarUsuarioFavoritoPro, id) => {
      await eliminarUsuarioFavoritoPro({variables: {id: id}}).then(res => {
        refetch();
        Toast.showWithGravity(
          'Profesional eliminado con éxito de la lista de deseos',
          Toast.LONG,
          Toast.TOP,
        );
        Vibration.vibrate(PATTERN);
      });
    };

    return (
      <TouchableOpacity
        onPress={() =>
          Navigation.navigate('DetailsPro', {data: item.profesional})
        }
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View style={styles.avatar}>
            {item.connected ? (
              <Badge
                status="success"
                badgeStyle={{position: 'absolute', left: 69, zIndex: 100}}
              />
            ) : null}
            <Image
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar,
              }}
              style={styles.avatar}
            />
          </View>
          <View style={{marginLeft: dimensions.Width(2)}}>
            <View
              style={{
                flexDirection: 'row',
                width: dimensions.Width(65),
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.light_white}
                style={[
                  stylesText.mainText,
                  {maxWidth: dimensions.Width(42), width: 'auto'},
                ]}>
                {item.profesional.nombre} {item.profesional.apellidos}{' '}
              </CustomText>
              {item.profesional.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={12}
                  style={{
                    alignSelf: 'center',
                    marginTop: 0,
                    paddingLeft: 5,
                    color: colors.main,
                  }}
                />
              ) : null}
              <View style={{marginLeft: 'auto'}}>
                <Mutation mutation={ELIMINAR_USUARIO_FAVORITE}>
                  {eliminarUsuarioFavoritoPro => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          favouritesDelete(
                            eliminarUsuarioFavoritoPro,
                            item.profesional.id,
                          )
                        }>
                        <Icon
                          name="heart"
                          type="AntDesign"
                          size={16}
                          style={{
                            alignSelf: 'center',
                            marginTop: 0,
                            marginRight: 0,
                            color: colors.ERROR,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  }}
                </Mutation>
              </View>
            </View>
            <View style={{flexDirection: 'row', width: dimensions.Width(65)}}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[
                  stylesText.secondaryText,
                  {marginTop: dimensions.Height(1)},
                ]}>
                {item.profesional.profesion}
              </CustomText>
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[
                  stylesText.secondaryText,
                  {marginLeft: 'auto', marginTop: dimensions.Height(1)},
                ]}>
                <Icon
                  name="staro"
                  type="AntDesign"
                  size={14}
                  color={colors.orange}
                />{' '}
                ({averageRating})
              </CustomText>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1)},
              ]}>
              {item.profesional.experiencia} de experiencia
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1)},
              ]}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.profesional.ciudad}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingTop: dimensions.Width(3),
          }}>
          <TouchableOpacity>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.mainText, {marginTop: dimensions.Height(1)}]}>
              {item.profesional.Precio}€ /Consulta
            </CustomText>
          </TouchableOpacity>
          <View style={{marginLeft: 'auto'}}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={isNavigation}
                title="Hacer consulta"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Vet favoritos" back={true} />
      </View>
      <CustomText
        light={colors.back_dark}
        dark={colors.white}
        style={[stylesText.h1, {marginLeft: 15}]}>
        Me gustan
      </CustomText>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query query={USUARIO_FAVORITO_PROFESIONAL} variables={{id: id}}>
          {response => {
            if (response.loading) {
              return <LoadingPLace />;
            }
            if (response.error) {
              return <LoadingPLace />;
            }
            if (response) {
              const datas =
                response && response.data && response.data.getUsuarioFavoritoPro
                  ? response.data.getUsuarioFavoritoPro.list
                  : '';

              return (
                <FlatList
                  data={datas}
                  renderItem={item => _renderItem(item, response.refetch)}
                  keyExtractor={item => item.id}
                  showsVerticalScrollIndicator={false}
                  ListEmptyComponent={
                    <NoData menssge="Aún no tienes veterinarios en favoritos" />
                  }
                />
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  textss: {
    fontSize: dimensions.FontSize(16),
    marginBottom: dimensions.Height(2),
    marginLeft: dimensions.Height(2),
  },

  text1: {
    fontSize: dimensions.FontSize(28),
    marginLeft: dimensions.Height(2),
  },

  card: {
    width: dimensions.Width(92),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    marginBottom: dimensions.Height(2),
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  text: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(0.3),
  },

  textt: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1.5),
  },
  title: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
});
