import React from 'react';
import {View, ScrollView, RefreshControl} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions} from '../themes/dimensions';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import Headers from '../components/Header';
import {CustomText} from '../components/CustomText';
import Card from '../components/CardAdopcion';
import LoadingPLace from './../components/Placeholder';
import {useState} from 'react';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';
import {GET_MASCOTA_ADOPCION} from '../query/index';
import {Query} from 'react-apollo';
import {stylesText} from '../components/StyleText';

export const Adopcion = ({appStart}) => {
  const [refreshing, setRefreshing] = useState(false);
  appStart(QBConfig);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Adoptar" back={true} />
      </View>
      <View style={styles.container}>
        <CustomText
          light={colors.back_dark}
          dark={colors.white}
          style={[stylesText.h1, {marginLeft: 15, marginBottom: 20}]}>
          Adoptar
        </CustomText>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
          }
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              paddingTop: dimensions.Height(2),
              marginBottom: dimensions.Height(5),
            }}>
            <Query query={GET_MASCOTA_ADOPCION}>
              {response => {
                if (response.loading) {
                  return <LoadingPLace />;
                }
                if (response.error) {
                  return console.log('Response Error-------', response.error);
                }
                if (response) {
                  response.refetch();
                  console.log(response);
                  return (
                    <Card
                      navigation={Navigation}
                      data={
                        response && response.data && response.data.getAdopcion
                          ? response.data.getAdopcion.list
                          : ''
                      }
                      refetch={response.refetch}
                    />
                  );
                }
              }}
            </Query>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  text: {
    fontSize: dimensions.FontSize(16),
    marginBottom: dimensions.Height(2),
    marginLeft: dimensions.Height(2),
  },

  text1: {
    fontSize: dimensions.FontSize(16),
    marginLeft: dimensions.Height(2),
  },
});

const mapDispatchToProps = {appStart};

export default connect(
  null,
  mapDispatchToProps,
)(Adopcion);
