import React from 'react';
import {View, TouchableOpacity, FlatList} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/HeaderPlus';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {CustomText} from './../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {ScrollView} from 'react-native-gesture-handler';
import {NETWORK_INTERFACE_LINK_AVATAR} from './../constants/config';
import {Avatar} from 'react-native-elements';
import {Button} from './../components/Button';
import {Query, Mutation} from 'react-apollo';
import {useNavigationParam} from 'react-navigation-hooks';
import {GET_PROFEISONAL_SUSCRIPCION} from './../query';
import {ACTUALIZAR_USUARIO} from './../mutations';
import LoadingPLace from './../components/Placeholder';
import {stylesText} from '../components/StyleText';

export default function Consultas() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const _renderItem = ({item}, refetch) => {
    let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
    item.professionalRatinglist.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

    const ar =
      (5 * rating['5'] +
        4 * rating['4'] +
        3 * rating['3'] +
        2 * rating['2'] +
        1 * rating['1']) /
      item.professionalRatinglist.length;
    let averageRating = 0;
    if (item.professionalRatinglist.length) {
      averageRating = ar.toFixed(1);
    }

    const handleSubmit = async mutation => {
      const input = {
        id: data.id,
        MyVet: item.id,
      };
      mutation({variables: {input}})
        .then(res => {
          console.log('done', res);
          Navigation.navigate('PaymentVet', {data: data});
        })
        .catch(err => console.log(err));
    };

    return (
      <Mutation mutation={ACTUALIZAR_USUARIO}>
        {mutation => {
          return (
            <TouchableOpacity
              style={styles.cardinfo}
              onPress={() => handleSubmit(mutation)}>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(2),
                }}>
                <Avatar
                  rounded
                  size={50}
                  source={{
                    uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar,
                  }}
                  containerStyle={styles.avatar}
                />
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(4),
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <CustomText
                    numberOfLines={1}
                    light={colors.back_dark}
                    dark={colors.white}
                    style={[
                      stylesText.mainText,
                      {width: 'auto', maxWidth: dimensions.Width(30)},
                    ]}>
                    {item.nombre} {item.apellidos}
                  </CustomText>
                  {item.isVerified ? (
                    <Icon
                      name="verified"
                      type="Octicons"
                      size={12}
                      style={{
                        alignSelf: 'center',
                        marginTop: 0,
                        paddingLeft: 5,
                        color: colors.main,
                      }}
                    />
                  ) : null}
                </View>
                <CustomText
                  numberOfLines={1}
                  light={colors.main}
                  dark={colors.main}
                  style={[
                    stylesText.secondaryText,
                    {width: 'auto', maxWidth: dimensions.Width(30)},
                  ]}>
                  {item.profesion}
                </CustomText>
                <CustomText
                  numberOfLines={1}
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={[
                    stylesText.secondaryText,
                    {width: 'auto', maxWidth: dimensions.Width(20)},
                  ]}>
                  {item.experiencia} de experiencia
                </CustomText>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Icon
                    name="staro"
                    type="AntDesign"
                    size={15}
                    color={colors.orange}
                  />
                  <Icon
                    name="staro"
                    type="AntDesign"
                    size={15}
                    color={colors.orange}
                  />
                  <Icon
                    name="staro"
                    type="AntDesign"
                    size={15}
                    color={colors.orange}
                  />
                  <Icon
                    name="staro"
                    type="AntDesign"
                    size={15}
                    color={colors.orange}
                  />
                  <Icon
                    name="staro"
                    type="AntDesign"
                    size={15}
                    color={colors.orange}
                  />
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={[
                      stylesText.placeholderText,
                      {
                        width: 'auto',
                        maxWidth: dimensions.Width(20),
                        marginLeft: 5,
                      },
                    ]}>
                    ({averageRating})
                  </CustomText>
                </View>
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(4),
                }}>
                <View style={styles.signupButtonContainer}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={styles.buttonView}
                    onPress={() =>
                      Navigation.navigate('DetailsPro', {
                        data: item,
                      })
                    }
                    title="Ver detalles"
                    titleStyle={styles.buttonTitle}
                  />
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      </Mutation>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} />
      </View>
      <View style={styles.contenedor} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <CustomText
          style={[stylesText.mainText, {textAlign: 'center'}]}
          light={colors.back_dark}
          dark={colors.white}>
          Selecciona tu Vet favorito de{' '}
          <CustomText
            style={[stylesText.mainText, {textAlign: 'center'}]}
            light={colors.main}
            dark={colors.main}>
            Vetec PLUS
          </CustomText>
        </CustomText>
        <Query query={GET_PROFEISONAL_SUSCRIPCION}>
          {response => {
            if (response.loading) {
              return <LoadingPLace />;
            }
            if (response.error) {
              return <LoadingPLace />;
            }
            if (response) {
              return (
                <View style={{marginTop: dimensions.Height(5)}}>
                  <FlatList
                    data={
                      response && response.data
                        ? response.data.getProfesionaforSuscribcion
                        : ''
                    }
                    renderItem={item =>
                      _renderItem(item, response.refetch, response.loading)
                    }
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(25),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: dimensions.Width(100),
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: dimensions.Height(5),
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 20,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(3),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 140,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
    width: 'auto',
    maxWidth: 160,
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(35),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },
});
