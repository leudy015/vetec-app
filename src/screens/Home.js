import React from 'react';
import {
  View,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions} from '../themes/dimensions';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import Headers from '../components/Header';
import Category from '../components/Category';
import {CustomText} from '../components/CustomText';
import Profesional from '../components/Profesional';
import LoadingPLace from './../components/Placeholder';
import {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';
import {GET_PROFEISONAL, USER_DETAIL} from '../query/index';
import {Query} from 'react-apollo';
import {stylesText} from '../components/StyleText';
import MascotaHome from '../components/MascotaHome';
import AsyncStorage from '@react-native-community/async-storage';

export const Home = ({appStart}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [id, setId] = useState(null);
  appStart(QBConfig);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      appStart(QBConfig);
    }, 2000);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Veterinarios" />
      </View>
      <View style={styles.container}>
        {id ? (
          <Query query={USER_DETAIL} variables={{id: id}}>
            {({loading, error, data, refetch}) => {
              if (error) {
                console.log('Response Error-------', error);
                return <ActivityIndicator style={{marginLeft: 10}} />;
              }
              if (loading) {
                return <ActivityIndicator style={{marginLeft: 10}} />;
              }
              if (data) {
                refetch();
                let very = {
                  id: id,
                  fromSocial: true,
                };
                const userDatas = data.getUsuario;
                return (
                  <View>
                    {!userDatas.verifyPhone ? (
                      <View style={styles.noverify}>
                        <CustomText
                          style={[
                            stylesText.secondaryText,
                            {
                              width: 200,
                              color: colors.white,
                              paddingTop: 15,
                            },
                          ]}>
                          Aún no has verificado número de teléfono,
                        </CustomText>
                        <TouchableOpacity
                          style={styles.btns}
                          onPress={() =>
                            Navigation.navigate('VerifyPhone', {data: very})
                          }>
                          <CustomText
                            style={[
                              stylesText.secondaryText,
                              {
                                color: colors.white,
                                textAlign: 'center',
                                padding: 8,
                                paddingHorizontal: 15,
                              },
                            ]}>
                            ¡Verificar ahora!
                          </CustomText>
                        </TouchableOpacity>
                      </View>
                    ) : null}
                  </View>
                );
              }
            }}
          </Query>
        ) : null}
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
          }
          showsVerticalScrollIndicator={false}>
          {id ? (
            <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={[stylesText.mainText, {marginLeft: 15}]}>
                  Mis mascotas
                </CustomText>
              </View>
              <ScrollView
                style={{
                  marginBottom: dimensions.Height(1),
                  marginTop: 20,
                }}
                showsHorizontalScrollIndicator={false}
                horizontal={true}>
                <MascotaHome />
              </ScrollView>
            </View>
          ) : null}

          <View style={{marginBottom: dimensions.Height(1)}}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[
                stylesText.mainText,
                {
                  marginLeft: 15,
                  marginBottom: 5,
                  marginTop: id ? 30 : 0,
                },
              ]}>
              Categorías
            </CustomText>
            <Category />
          </View>
          <View
            style={{
              paddingTop: dimensions.Height(2),
              marginBottom: dimensions.Height(5),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[stylesText.mainText, {marginLeft: 15}]}>
              Veterinarios
            </CustomText>
            <Query query={GET_PROFEISONAL}>
              {(response, error, loading) => {
                if (response.loading) {
                  return <LoadingPLace />;
                }
                if (error) {
                  return console.log('Response Error-------', error);
                }
                if (response) {
                  response.refetch();
                  return (
                    <Profesional
                      navigation={Navigation}
                      data={
                        response && response.data
                          ? response.data.getProfesionalall
                          : ''
                      }
                      refetch={response.refetch}
                    />
                  );
                }
              }}
            </Query>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  btn: {
    marginLeft: 20,
    paddingHorizontal: 25,
    paddingVertical: 3,
    borderRadius: 20,
  },

  noverify: {
    width: dimensions.Width(100),
    height: 70,
    backgroundColor: colors.ERROR,
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 15,
    flexDirection: 'row',
  },
  btns: {
    borderColor: colors.white,
    borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 15,
    borderRadius: 55,
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.light_white, colors.back_dark),
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    alignSelf: 'center',
    flex: 1,
  },

  icons: {
    color: new DynamicValue(colors.back_suave_dark, colors.light_white),
  },
});

const mapDispatchToProps = {appStart};

export default connect(
  null,
  mapDispatchToProps,
)(Home);
