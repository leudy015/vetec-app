import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Modal,
  Alert,
  Vibration,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {CustomText} from '../components/CustomText';
import {useNavigationParam} from 'react-navigation-hooks';
import _get from 'lodash.get';
import {PaymentPaypal} from '../components/Paymentpaypal';
import {PaymentVisa} from '../components/PaymentVisa';
import {WebView} from 'react-native-webview';
import {NETWORK_INTERFACE_LINK} from '../constants/config';
import stripe from 'tipsi-stripe';
import Informacion from '../components/informacion';
import {ScrollView} from 'react-native-gesture-handler';
import {LineDotsLoader} from 'react-native-indicator';
import {CREATE_NOTIFICATION} from '../mutations/index';
import {useMutation} from '@apollo/react-hooks';
import {stylesText} from '../components/StyleText';
import {ifIphoneX} from 'react-native-iphone-x-helper';

stripe.setOptions({
  publishableKey: 'pk_live_ZSnprUnfFoYZnuMSyv69szKq002HmxhXU8',
});

function PaymentPage() {
  const [ModalVisible, setModalVisible] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [token, setToken] = useState('');
  const [tatalPay, setTatalPay] = useState('');
  const [cards, setcards] = useState('');
  const [messageTexteNeworden] = useState(
    'Vetec App, has recibido una nueva consulta ve, a tu perfil para procesarla. https://profesionales.vetec.es/profile-professional',
  );

  const datos = useNavigationParam('data');

  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const data =
    datos && datos.data && datos.data.crearModificarConsulta
      ? datos.data.crearModificarConsulta.data
      : datos;

  const ONE_SECOND_IN_MS = 1000;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  const SendPushNotificationNeworden = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification-profesional?IdOnesignal=${
        data.profesional.UserID
      }&textmessage=${messageTexteNeworden}`,
    ).catch(err => console.log(err));
  };

  const SendTextSMSNeworden = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-message?recipient=${
        data.profesional.telefono
      }&textmessage=${messageTexteNeworden}`,
    ).catch(err => console.log(err));
  };

  const getCard = async () => {
    let res = await fetch(
      NETWORK_INTERFACE_LINK + `/get-card?customers=${data.usuario.StripeID}`,
    );
    const card = await res.json();
    setcards(card);
  };

  const fetchMyAPI = useCallback(async () => {
    if (!data.usuario.StripeID) {
      const createclient = async () => {
        let res = await fetch(
          NETWORK_INTERFACE_LINK +
            `/create-client?userID=${data.usuario.id}&nameclient=${
              data.usuario.nombre
            }&email=${data.usuario.email}`,
        );
        const createcliente = await res.json();
        console.log(createcliente);
      };
      createclient();
    } else {
      return null;
    }
  }, []);

  useEffect(() => {
    fetchMyAPI();
    getCard();
  }, []);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.crecadr}
        onPress={() => {
          const pagar = async () => {
            var totals = parseInt(tatalPay);
            console.log();
            let paga = await fetch(
              NETWORK_INTERFACE_LINK +
                `/payment-existing-card?customers=${item.customer}&cardID=${
                  item.id
                }&amount=${totals.toFixed(2) * 100}&ConsultaID=${data.id}`,
            );
            const procederalpago = await paga.json();
            console.log(procederalpago);
            if (procederalpago.success) {
              const NotificationInput = {
                consulta: data.id,
                user: data.profesional.id,
                usuario: data.usuario.id,
                profesional: data.profesional.id,
                type: 'new_order',
              };
              console.log('sending nitification', NotificationInput);
              await createNotification({
                variables: {input: NotificationInput},
              })
                .then(async results => {
                  console.log('results', results);
                })
                .catch(err => {
                  console.log('err', err);
                });
              SendPushNotificationNeworden();
              SendTextSMSNeworden();
              Navigation.navigate('Thank');
            } else {
              Vibration.vibrate(PATTERN);
              Alert.alert(
                'Error con tu método de pago',
                'Hubo un error con tu método de pago vuelve a intentarlo por favor',
                [
                  {
                    text: 'Volver a intentarlo',
                    onPress: () => console.log('calcel'),
                  },
                ],

                {cancelable: false},
              );
            }
          };
          pagar();
        }}>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.main}
            style={stylesText.mainText}>
            {data.usuario.nombre} {data.usuario.apellidos}
          </CustomText>
        </View>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={styles.brand}>
            {item.card.brand}
          </CustomText>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={[stylesText.mainText, {marginTop: 10, marginLeft: 10}]}>
            **** **** {item.card.last4}
          </CustomText>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={[stylesText.mainText, {marginTop: 10, marginLeft: 'auto'}]}>
            {item.card.exp_month} / {item.card.exp_year}
          </CustomText>
        </View>
      </TouchableOpacity>
    );
  };

  useEffect(() => {
    const valorDescuento =
      data && data.descuento ? data.descuento.descuento : '';
    const tipo = data.descuento ? data.descuento.tipo : '';
    const subtotal = data
      ? data.profesional.Precio
      : data.profesional.Precio * data.cantidad;
    let descuentoFinal = 0;
    let total = subtotal;
    if (valorDescuento && tipo) {
      switch (tipo) {
        case 'dinero':
          total = subtotal - valorDescuento;
          break;
        case 'porcentaje':
          descuentoFinal = valorDescuento / 100;
          total = subtotal - subtotal * descuentoFinal;
          break;
      }
    }
    setTatalPay(total);
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const openModal = () => {
    setModalVisible(true);
  };

  const handleCardPayPress = async () => {
    try {
      const token = await stripe.paymentRequestWithCardForm({
        // Only iOS support this options
        smsAutofillDisabled: true,
        prefilledInformation: {
          billingAddress: {
            name: '',
            line1: '',
            city: '',
            state: '',
            country: 'ES',
            postalCode: '',
            email: '',
          },
        },
      });
      setToken(token);

      let response = await fetch(
        `${NETWORK_INTERFACE_LINK}/card-create?customers=${
          data.usuario.StripeID
        }&token=${token.tokenId}`,
      );
      const secret = await response.json();
      console.log(secret);
      getCard();
    } catch (error) {
      setToken(null);
    }
  };

  const submit = async ev => {
    const valorDescuento =
      data && data.descuento ? data.descuento.descuento : '';
    const tipo = data.descuento ? data.descuento.tipo : '';
    const subtotal = data.profesional.Precio * data.cantidad;
    let descuentoFinal = 0;
    let total = subtotal;
    if (valorDescuento && tipo) {
      switch (tipo) {
        case 'dinero':
          total = subtotal - valorDescuento;
          break;
        case 'porcentaje':
          descuentoFinal = valorDescuento / 100;
          total = subtotal - subtotal * descuentoFinal;
          break;
      }
    }

    let response = await fetch(
      `${NETWORK_INTERFACE_LINK}/stripe/chargeToken?stripeToken=${
        token.tokenId
      }&orderId=${data.id}&amount=${total.toFixed(2) * 100}`,
    );
    if (response.ok) {
      const NotificationInput = {
        consulta: data.id,
        user: data.profesional.id,
        usuario: data.usuario.id,
        profesional: data.profesional.id,
        type: 'new_order',
      };
      console.log('sending nitification', NotificationInput);
      await createNotification({
        variables: {input: NotificationInput},
      })
        .then(async results => {
          console.log('results', results);
        })
        .catch(err => {
          console.log('err', err);
        });
      SendPushNotificationNeworden();
      SendTextSMSNeworden();
      Navigation.navigate('Thank');
    } else {
      Vibration.vibrate(PATTERN);
      Alert.alert(
        'Error con tu método de pago',
        'Hubo un error con tu método de pago vuelve a intentarlo por favor',
        [
          {
            text: 'Volver a intentarlo',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
    }
  };

  if (token) {
    submit();
  }
  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => Navigation.goBack(null)}
            style={{marginLeft: 5}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.title}>
            Completar el Pago
          </CustomText>
          <TouchableOpacity
            onPress={() => openModal()}
            style={{marginLeft: 'auto', marginRight: 10}}>
            <Icon
              name="questioncircleo"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalVisible}
          presentationStyle="formSheet">
          <SafeAreaView style={styles.headers1}>
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <View style={{alignItems: 'flex-start', marginLeft: 10}} />
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => setModalVisible(!ModalVisible)}>
                  <CustomText style={{marginRight: 20}}>
                    <Icon
                      type="AntDesign"
                      name="close"
                      size={25}
                      color={colors.green_main}
                    />
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
          <Informacion />
        </Modal>
      </SafeAreaView>
      <View style={styles.container}>
        <View>
          <View style={{marginTop: dimensions.Height(4)}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <TouchableOpacity
                onPress={() => handleCardPayPress()}
                style={styles.tarjeta}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      borderWidth: 0.5,
                      padding: 8,
                      borderColor: colors.rgb_153,
                      borderRadius: 8,
                      backgroundColor: 'transparent',
                    }}>
                    <PaymentVisa />
                  </View>
                  <CustomText
                    style={{
                      alignSelf: 'center',
                      marginLeft: 20,
                      fontSize: dimensions.FontSize(16),
                      fontWeight: '300',
                      color: colors.rgb_102,
                    }}>
                    Pagar con Tarjeta
                  </CustomText>
                  <Icon
                    type="AntDesign"
                    name="right"
                    style={{
                      marginTop: 5,
                      marginLeft: 'auto',
                      alignSelf: 'center',
                    }}
                    size={18}
                    color={colors.rgb_153}
                  />
                </View>
              </TouchableOpacity>
              <Modal
                animationType="slide"
                presentationStyle="formSheet"
                visible={showModal}
                onRequestClose={() => setShowModal(true)}>
                <SafeAreaView style={styles.headers1}>
                  <View style={{flexDirection: 'row', marginTop: 20}}>
                    <View style={{alignItems: 'flex-start', marginLeft: 10}} />
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity onPress={() => setShowModal(false)}>
                        <CustomText style={{marginRight: 20}}>
                          <Icon
                            type="AntDesign"
                            name="close"
                            size={25}
                            color={colors.green_main}
                          />
                        </CustomText>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SafeAreaView>
                <WebView
                  useWebKit={true}
                  source={{
                    uri:
                      NETWORK_INTERFACE_LINK +
                      `/paypal?price=${tatalPay}&order=${data.id}`,
                  }}
                  startInLoadingState={true}
                  renderLoading={() => <LineDotsLoader />}
                  onNavigationStateChange={async datas => {
                    console.log(datas);
                    if (datas.title === 'success') {
                      setShowModal(false);
                      const NotificationInput = {
                        consulta: data.id,
                        user: data.profesional.id,
                        usuario: data.usuario.id,
                        profesional: data.profesional.id,
                        type: 'new_order',
                      };
                      console.log('sending nitification', NotificationInput);
                      await createNotification({
                        variables: {input: NotificationInput},
                      })
                        .then(async results => {
                          console.log('results', results);
                        })
                        .catch(err => {
                          console.log('err', err);
                        });
                      SendPushNotificationNeworden();
                      SendTextSMSNeworden();
                      Navigation.navigate('Thank');
                    } else if (datas.title === 'cancel') {
                      setShowModal(false);
                      Navigation.navigate('Error');
                    }
                  }}
                  javaScriptEnabled={true}
                  domStorageEnabled={true}
                  injectedJavaScript={`document.f1.submit()`}
                />
              </Modal>
              <TouchableOpacity
                onPress={() => setShowModal(true)}
                style={styles.tarjeta}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      borderWidth: 0.5,
                      padding: 8,
                      borderColor: colors.rgb_153,
                      borderRadius: 8,
                      backgroundColor: 'transparent',
                    }}>
                    <PaymentPaypal />
                  </View>
                  <CustomText
                    style={{
                      alignSelf: 'center',
                      marginLeft: 20,
                      fontSize: dimensions.FontSize(16),
                      fontWeight: '300',
                      color: colors.rgb_102,
                    }}>
                    Pagar con Paypal
                  </CustomText>
                  <Icon
                    type="AntDesign"
                    name="right"
                    style={{
                      marginTop: 5,
                      marginLeft: 'auto',
                      alignSelf: 'center',
                    }}
                    size={18}
                    color={colors.rgb_153}
                  />
                </View>
              </TouchableOpacity>
              {cards && cards.data ? (
                <View style={styles.cont}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.white}
                    style={[stylesText.mainText, {marginLeft: 10}]}>
                    Pagar con tarjeta existente
                  </CustomText>
                  <FlatList
                    data={cards && cards.data}
                    renderItem={item => _renderItem(item)}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
              ) : null}
            </ScrollView>
          </View>
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        paddingTop: 0,
      },
      {
        paddingTop: 30,
      },
    ),
  },

  headers1: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(6),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(3),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  tarjeta: {
    padding: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.rgb_235,
  },

  instruction: {
    textAlign: 'center',
    marginBottom: 5,
  },
  token: {
    padding: 20,
    textAlign: 'center',
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(10),
  },
  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  crecadr: {
    width: dimensions.Width(90),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    padding: dimensions.Width(5),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(2),
  },

  brand: {
    fontSize: 18,
    marginTop: 10,
    borderWidth: 1,
    padding: 2,
    borderColor: new DynamicValue(colors.back_suave_dark, colors.light_white),
    borderRadius: 5,
    fontWeight: 'bold',
    width: 45,
  },

  cont: {
    justifyContent: 'center',
    alignSelf: 'center',
    paddingHorizontal: dimensions.Width(2),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(20),
  },
});

export default PaymentPage;
