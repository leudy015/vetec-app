import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {GiftedChat, Send, Bubble, InputToolbar} from 'react-native-gifted-chat';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from '../constants/config';
import Icon from 'react-native-dynamic-vector-icons';
import {Avatar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import {UPLOAD_FILE} from '../mutations';
import {Mutation} from 'react-apollo';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import io from 'socket.io-client';
import {useNavigationParam} from 'react-navigation-hooks';
import QB from 'quickblox-react-native-sdk';
import {showError} from '../services/FlashMessageService';
import {webrtcCall} from '../actionCreators'; // webrtc call audio & video
import {stylesText} from '../components/StyleText';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function ChatsScreen({call, qbUsers}) {
  const data = useNavigationParam('data');
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [text, setText] = useState({text: ''});
  const [messages, setMessages] = useState(
    data.messagechat ? data.messagechat : [],
  );
  const [Loading, setLoading] = useState(false);
  const [LoadingAudio, setLoadingAudio] = useState(false);

  if (Platform.OS === 'ios') {
    requestMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.CAMERA]).then(
      statuses => {
        console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
        console.log('FaceID', statuses[PERMISSIONS.IOS.FACE_ID]);
      },
    );
  } else {
    requestMultiple([
      PERMISSIONS.ANDROID.MICROPHONE,
      PERMISSIONS.ANDROID.CAMERA,
    ]).then(statuses => {
      console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
      console.log('FaceID', statuses[PERMISSIONS.IOS.FACE_ID]);
    });
  }

  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  useEffect(() => {
    socket.on('connect', () => {
      socket.emit('online_user', {
        usuario: data.usuario.id,
        profesional: data.profesional.id,
        id: data.usuario.id,
      });
    });

    socket.on('messages', datas => {
      let old_messages = [...messages];
      setMessages(messages => [...(datas || []), ...old_messages]);
    });
  }, []);

  const onSend = newMessages => {
    let old_messages = [...messages];
    setMessages(messages => GiftedChat.append(old_messages, newMessages));
    const dats = newMessages[0];
    const messagechat = [
      {
        text: dats.text,
        read: false,
        sent: true,
        received: true,
        pending: true,
        user: {
          _id: dats.user._id,
          name: dats.user.name,
          avatar: dats.user.avatar,
        },
        createdAt: dats.createdAt,
        _id: dats._id,
      },
    ];
    socket.emit('private_message', {
      messagechat,
      Userid: data.profesional.UserID,
      receptor: data.profesional.id,
      prof: data.profesional.id,
      user: data.usuario.id,
    });
  };

  const renderInputToolbar = props => {
    return <InputToolbar {...props} containerStyle={styles.inputs} />;
  };

  const renderSend = props => {
    return (
      <Send {...props}>
        <View style={{marginRight: 5, marginTop: 5}}>
          <Icon
            type="MaterialCommunityIcons"
            name="send-circle"
            size={40}
            color={colors.main}
            style={styles.Send}
          />
        </View>
      </Send>
    );
  };

  const renderBubble = props => {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: colors.black,
            fontWeight: '200',
          },
          left: {
            color: colors.black,
            fontWeight: '200',
          },
        }}
        wrapperStyle={{
          left: {
            backgroundColor: colors.rgb_235,
          },
          right: {
            backgroundColor: colors.main,
          },
        }}
      />
    );
  };

  const videoCall = () => {
    setLoading(true);
    const emails = data.profesional.email;
    console.log('qbUsers', qbUsers);
    let users = qbUsers.filter(opponentUser => {
      if (opponentUser.login === emails) {
        setLoading(false);
        return true;
      } else {
        setLoading(true);
        return false;
      }
    });
    console.log('usersusers', users);
    if (users[0]) {
      const opponentsIds = [users[0].id];
      try {
        call({opponentsIds, type: QB.webrtc.RTC_SESSION_TYPE.VIDEO});
        const messagechat = [
          {
            text: 'Videollamada',
            read: false,
            sent: true,
            received: true,
            pending: true,
            user: {
              _id: data.usuario.id,
              name: data.usuario.nombre,
              avatar: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
            },
            createdAt: new Date(),
            _id: uuidv4(),
          },
        ];
        socket.emit('private_message', {
          messagechat,
          Userid: data.profesional.UserID,
          receptor: data.profesional.id,
          prof: data.profesional.id,
          user: data.usuario.id,
        });
        Navigation.navigate('CheckConnection', {data: data.profesional});
      } catch (e) {
        showError('Error', e.message);
      }
    }
  };

  const VoiceCall = () => {
    setLoadingAudio(true);
    console.log('qbUsers', qbUsers);
    const emails = data.profesional.email;
    let users = qbUsers.filter(opponentUser => {
      if (opponentUser.login === emails) {
        setLoadingAudio(false);
        return true;
      } else {
        setLoadingAudio(true);
        return false;
      }
    });
    console.log('usersusers', users);
    if (users[0]) {
      const opponentsIds = [users[0].id];
      try {
        call({opponentsIds, type: QB.webrtc.RTC_SESSION_TYPE.AUDIO});
        const messagechat = [
          {
            text: 'Llamada de audio',
            read: false,
            sent: true,
            received: true,
            pending: true,
            user: {
              _id: data.usuario.id,
              name: data.usuario.nombre,
              avatar: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
            },
            createdAt: new Date(),
            _id: uuidv4(),
          },
        ];
        socket.emit('private_message', {
          messagechat,
          Userid: data.profesional.UserID,
          receptor: data.profesional.id,
          prof: data.profesional.id,
          user: data.usuario.id,
        });
        Navigation.navigate('CheckConnection', {data: data.profesional});
      } catch (e) {
        showError('Error', e.message);
      }
    }
  };

  const selectPhotoTapped = singleUpload => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar imagen',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async response => {
      if (response.data) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({variables: {imgBlob}})
            .then(res => {
              const sendImagen = () => {
                const messagechat = [
                  {
                    image:
                      NETWORK_INTERFACE_LINK_AVATAR +
                      res.data.singleUpload.filename,
                    text: 'Imagen',
                    read: false,
                    sent: true,
                    received: true,
                    pending: true,
                    user: {
                      _id: data.usuario.id,
                      name: data.usuario.nombre,
                      avatar:
                        NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
                    },
                    createdAt: new Date(),
                    _id: uuidv4(),
                  },
                ];

                socket.emit('private_message', {
                  messagechat,
                  Userid: data.profesional.UserID,
                  receptor: data.profesional.id,
                  prof: data.profesional.id,
                  user: data.usuario.id,
                });

                setMessages(messages => [...messagechat, ...messages]);
              };
              sendImagen();
            })
            .catch(error => {
              console.log('fs error: que me arroja', error);
            });
        }
      }
    });
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => Navigation.goBack(null)}
            style={{marginLeft: 5}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <View style={{marginLeft: dimensions.Width(4), flexDirection: 'row'}}>
            <Avatar
              rounded
              size={30}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
              }}
              containerStyle={styles.avatar}
            />
            <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  numberOfLines={1}
                  light={colors.back_dark}
                  dark={colors.white}
                  style={[
                    stylesText.mainText,
                    {
                      marginLeft: 8,
                      width: 'auto',
                      maxWidth: dimensions.Width(30),
                    },
                  ]}>
                  {data.profesional.nombre} {data.profesional.apellidos}
                </CustomText>
                {data.profesional.isVerified ? (
                  <Icon
                    name="verified"
                    type="Octicons"
                    size={12}
                    style={{
                      alignSelf: 'center',
                      marginTop: 3,
                      marginLeft: 0,
                      color: colors.main,
                    }}
                  />
                ) : null}
              </View>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: 8,
                    marginTop: 10,
                    marginBottom: 15,
                  }}>
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={20}
                    color={colors.orange}
                  />
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={20}
                    color={colors.orange}
                  />
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={20}
                    color={colors.orange}
                  />
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={20}
                    color={colors.orange}
                  />
                  <Icon
                    name="star"
                    type="AntDesign"
                    size={20}
                    color={colors.orange}
                  />
                </View>
              </View>
            </View>
          </View>
          {!data.usuario.isPlus ? (
            <View
              style={{
                marginLeft: 'auto',
                flexDirection: 'row',
                marginRight: 5,
              }}>
              {LoadingAudio ? (
                <ActivityIndicator style={[styles.bacs, {marginRight: 20}]} />
              ) : (
                <TouchableOpacity
                  onPress={() => VoiceCall()}
                  style={{marginRight: 20}}>
                  <Icon
                    name="phone"
                    type="AntDesign"
                    size={25}
                    style={styles.bacs}
                  />
                </TouchableOpacity>
              )}
              {Loading ? (
                <ActivityIndicator style={[styles.bacs, {marginRight: 5}]} />
              ) : (
                <TouchableOpacity
                  onPress={() => videoCall()}
                  style={{marginRight: 20}}>
                  <Icon
                    name="videocamera"
                    type="AntDesign"
                    size={25}
                    style={styles.bacs}
                  />
                </TouchableOpacity>
              )}
              <Mutation mutation={UPLOAD_FILE}>
                {singleUpload => (
                  <TouchableOpacity
                    onPress={() => selectPhotoTapped(singleUpload)}
                    style={{marginRight: 10}}>
                    <Icon
                      name="camerao"
                      type="AntDesign"
                      size={25}
                      style={styles.bacs}
                    />
                  </TouchableOpacity>
                )}
              </Mutation>
            </View>
          ) : null}
        </View>
      </SafeAreaView>
      <GiftedChat
        messages={messages}
        placeholder="Escribe algo ...."
        onSend={newMessages => onSend(newMessages)}
        onInputTextChanged={text => setText({text})}
        text={text.text}
        isTyping={true}
        renderInputToolbar={renderInputToolbar}
        renderSend={renderSend}
        textInputStyle={{color: colors.rgb_153}}
        renderBubble={renderBubble}
        messageRead={true}
        messageDelivered={true}
        renderUsernameOnMessage={true}
        showUserAvatar={true}
        renderAvatarOnTop={true}
        user={{
          _id: data.usuario.id,
          name: data.usuario.nombre,
          avatar: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
        }}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  inputs: {
    borderTopWidth: 0.2,
    borderTopColor: colors.rgb_235,
    backgroundColor: 'transparent',
  },
  Send: {
    marginLeft: 'auto',
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    ...ifIphoneX(
      {
        marginTop: 0,
      },
      {
        paddingTop: 30,
      },
    ),
  },

  title: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  title1: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(12),
    fontWeight: '500',
    color: colors.rgb_153,
  },

  bacs: {
    color: new DynamicValue(colors.main, colors.main),
  },
});

const mapStateToProps = ({auth, users, user}, {exclude = []}) => {
  console.log('usersusers', users);
  return {
    qbUsers:
      (users &&
        users.users
          .filter(smuser => (auth.user ? smuser.id !== auth.user.id : true))
          .filter(smuser => exclude.indexOf(smuser.id) === -1)) ||
      [],
    user: user.user,
  };
};

const mapDispatchToProps = {
  call: webrtcCall,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatsScreen);
