import React, {useState} from 'react';
import {View, RefreshControl, ScrollView} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {CustomText} from '../components/CustomText';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {SearchBar} from 'react-native-elements';
import Profesional from '../components/Profesional';
import LoadingPLace from './../components/Placeholder';
import {Query} from 'react-apollo';
import {GET_PROFESIONAL_SEARCH} from '../query/index';
import Category from '../components/Category';
import {stylesText} from '../components/StyleText';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-dynamic-vector-icons';

export default function Search() {
  const [refreshing, setRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [Loading, setLoading] = useState('');

  const styles = useDynamicStyleSheet(dynamicStyles);

  const updateSearch = search => {
    setSearch(search);
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const icons = () => {
    return <Icon name="enviromento" type="AntDesign" size={25} color="white" />;
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Buscar" back={true} />
      </View>
      <SearchBar
        placeholder="¿Qué estás buscando?"
        onChangeText={updateSearch}
        value={search}
        containerStyle={styles.search}
        inputContainerStyle={styles.input}
        showLoading={Loading}
        autoFocus={true}
        loadingProps={Loading}
      />
      <View style={{marginBottom: dimensions.Height(1)}}>
        <CustomText
          light={colors.back_dark}
          dark={colors.rgb_153}
          style={[
            stylesText.mainText,
            {marginLeft: 15, marginBottom: 5, marginTop: 20},
          ]}>
          Categorías
        </CustomText>
        <Category />
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query query={GET_PROFESIONAL_SEARCH} variables={{search: search}}>
          {response => {
            if (response.loading) {
              setLoading(response.loading);
              return <LoadingPLace />;
            }
            if (response.error) {
              return <LoadingPLace />;
            }
            if (response) {
              return (
                <Profesional
                  navigation={Navigation}
                  data={
                    response &&
                    response.data &&
                    response.data.getProfesionalSearch
                      ? response.data.getProfesionalSearch.data
                      : ''
                  }
                />
              );
            } else {
              return <LoadingPLace />;
            }
          }}
        </Query>
      </ScrollView>
      <ActionButton
        buttonColor={colors.main}
        renderIcon={icons}
        onPress={() => Navigation.navigate('Maps')}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  search: {
    backgroundColor: 'transparent',
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },

  input: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 20,
  },
});
