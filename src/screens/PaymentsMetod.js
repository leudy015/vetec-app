import React, {useEffect, useState, useCallback} from 'react';
import {View, TouchableOpacity, FlatList, Image} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {NETWORK_INTERFACE_LINK} from '../constants/config';
import {useNavigationParam} from 'react-navigation-hooks';
import Card from './card';
import Toast from 'react-native-simple-toast';
import {stylesText} from '../components/StyleText';
import {image} from '../constants/image';

export default function Consultas() {
  const [cards, setcards] = useState('');
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const getCard = async () => {
    let res = await fetch(
      `${NETWORK_INTERFACE_LINK}/get-card?customers=${data.StripeID}`,
    );
    const card = await res.json();
    setcards(card);
    console.log(card);
  };

  const fetchMyAPI = useCallback(async () => {
    if (!data.StripeID) {
      let res = await fetch(
        NETWORK_INTERFACE_LINK +
          `/create-client?userID=${data.id}&nameclient=${data.nombre +
            data.apellidos}&email=${data.email}`,
      );
      const createcliente = await res.json();
      console.log(createcliente);
    } else {
      return null;
    }
  }, []);

  useEffect(() => {
    fetchMyAPI();
    getCard();
  }, []);

  const _renderItem = ({item}) => {
    let images = '';
    let brand = '';
    switch (item.card.brand) {
      case 'visa':
        images = image.VisaLogo;
        brand = 'Visa';
        break;
      case 'mastercard':
        images = image.MasterLogo;
        brand = 'Master Card';
        break;
      case 'amex':
        images = image.AmericanLogo;
        brand = 'American Express';
        break;
    }

    return (
      <View style={[styles.crecadr, {flexDirection: 'row'}]}>
        <Image
          source={images}
          style={{width: 60, height: 60, borderRadius: 100}}
        />
        <View style={{marginLeft: 10}}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              width: dimensions.Width(60),
            }}>
            <CustomText
              light={colors.back_suave_dark}
              dark={colors.main}
              style={stylesText.mainText}>
              {data.nombre} {data.apellidos}
            </CustomText>
            <TouchableOpacity
              onPress={() => {
                const deleteCard = async () => {
                  let res = await fetch(
                    NETWORK_INTERFACE_LINK + `/delete-card?cardID=${item.id}`,
                  );
                  const deletec = await res.json();
                  if (deletec ? deletec : false) {
                    getCard();
                    console.log('Tarjeta eliminada con éxito');
                    Toast.showWithGravity(
                      'Tarjeta eliminada con éxito',
                      Toast.LONG,
                      Toast.TOP,
                    );
                  }
                };
                deleteCard();
              }}
              style={{marginLeft: 'auto'}}>
              <Icon
                name="delete"
                type="AntDesign"
                size={20}
                style={{color: colors.ERROR}}
              />
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <CustomText
              light={colors.back_suave_dark}
              dark={colors.light_white}
              style={[stylesText.mainText, {marginTop: 10}]}>
              **** **** {item.card.last4}
            </CustomText>
            <CustomText
              light={colors.back_suave_dark}
              dark={colors.light_white}
              style={[
                stylesText.mainText,
                {marginTop: 10, marginLeft: 'auto'},
              ]}>
              {item.card.exp_month} / {item.card.exp_year}
            </CustomText>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Métodos de pago" back={true} />
      </View>
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <View style={{paddingHorizontal: dimensions.Width(4)}}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            numberOfLines={1}
            style={[stylesText.h1, {marginLeft: 15, marginBottom: 20}]}>
            Mis tarjetas
          </CustomText>
        </View>
        <View style={styles.cont}>
          <FlatList
            data={cards ? cards.data : ''}
            renderItem={item => _renderItem(item)}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />
          <View style={styles.crecadr}>
            <View style={{flexDirection: 'row', marginBottom: 15}}>
              <Icon
                name="pluscircle"
                type="AntDesign"
                size={20}
                style={{color: colors.rgb_153, marginRight: 10}}
              />
              <CustomText
                light={colors.back_suave_dark}
                dark={colors.main}
                style={[stylesText.mainText, {marginTop: -3}]}>
                Añadir tarjeta
              </CustomText>
            </View>
            <Card clientID={data.StripeID} getCard={getCard} />
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.Width(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  crecadr: {
    width: dimensions.Width(90),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    padding: dimensions.Width(5),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(4),
  },

  btncont: {
    width: dimensions.Width(85),
    padding: dimensions.Height(2),
    backgroundColor: colors.main,
    borderWidth: 1,
    borderColor: colors.main,
    borderRadius: dimensions.Width(10),
    marginTop: dimensions.Height(3),
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },

  cont: {
    justifyContent: 'center',
    alignSelf: 'center',
    paddingHorizontal: dimensions.Width(2),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(20),
  },
});
