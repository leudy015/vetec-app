import React, {useState} from 'react';
import DatePicker from 'react-native-date-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {NUEVA_MASCOTA, UPLOAD_FILE} from '../mutations';
import {Switch} from '@ant-design/react-native';
import {Mutation} from 'react-apollo';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-simple-toast';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {View, TouchableOpacity, Alert} from 'react-native';
import {CustomText} from '../components/CustomText';
import {Avatar} from 'react-native-elements';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from '../constants/config';
import {useNavigationParam} from 'react-navigation-hooks';
import Navigation from '../services/NavigationService';
import {OutlinedTextField} from 'react-native-material-textfield';
import {BubblesLoader} from 'react-native-indicator';
import {ProgressSteps, ProgressStep} from 'react-native-progress-steps';
import {stylesText} from '../components/StyleText';

const AddMascota = props => {
  const dat = useNavigationParam('data');

  const styles = useDynamicStyleSheet(dynamicStyles);

  const [date, setDate] = useState(new Date());
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  const [especie, setEspecie] = useState('');
  const [raza, setRaza] = useState('');
  const [genero, setGenero] = useState('');
  const [peso, setPeso] = useState('');
  const [microchip, setMicrochip] = useState('');
  const [vacunas, setVacunas] = useState('');
  const [alergias, setAlergias] = useState('');
  const [castrado, setCastrado] = useState(false);
  const [Loading, setLoading] = useState(false);

  const selectPhotoTapped = singleUpload => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar foto del perfil',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async response => {
      setLoading(true);
      if (response) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({variables: {imgBlob}})
            .then(res => {
              console.log('filemane: ', res.data.singleUpload.filename);
              setLoading(false);
              setAvatar(res.data.singleUpload.filename);

              const detection = async () => {
                let resp = await fetch(
                  `${NETWORK_INTERFACE_LINK}/image-detection?filename=${
                    res.data.singleUpload.filename
                  }`,
                );
                const datas = await resp.json();
                console.log('image >>>>>>>>>>>', datas[0]);
                setRaza(datas[0].className);

                let response = await fetch(
                  `${NETWORK_INTERFACE_LINK}/object-detection?filename=${
                    res.data.singleUpload.filename
                  }`,
                );
                const datos = await response.json();
                console.log(' objet >>>>>>>>>>>', datos[0]);
                setEspecie(datos[0].class);
              };
              detection();
            })
            .catch(error => {
              console.log('fs error: que me arroja', error);
              setLoading(false);
            });
        }
      }
    });
  };

  const onSwitchChange = () => {
    if (castrado === true) {
      setCastrado(false);
    } else {
      setCastrado(true);
    }
  };

  const handlePublish = async crearMascota => {
    setLoading(true);
    if (!name) {
      Alert.alert(
        'Debes un nombre a tu mascota',
        'Para continuar debes añadir el nombre de tu mascota',
        [{text: 'Vale!', onPress: () => console.log('calcel')}],

        {cancelable: false},
      );
      setLoading(false);
      return null;
    }

    if (!avatar) {
      Alert.alert(
        'Debes añadir una foto',
        'Para continuar debes añadir una foto de perfil de la mascota',
        [{text: 'Vale!', onPress: () => console.log('calcel')}],

        {cancelable: false},
      );
      setLoading(false);
      return null;
    }
    await crearMascota({
      variables: {
        input: {
          name: name,
          avatar: avatar,
          especie: especie,
          raza: raza,
          genero: genero,
          peso: peso,
          microchip: microchip,
          vacunas: vacunas,
          alergias: alergias,
          castrado: castrado,
          age: date,
          usuario: props.id,
        },
      },
    }).then(async res => {
      const cont =
        res && res.data && res.data.crearMascota
          ? res.data.crearMascota.data
          : '';
      Toast.showWithGravity('Mascota añadida con éxito', Toast.LONG, Toast.TOP);
      setLoading(false);
      props.setModalVisible(false);
      const nav = {
        masc: cont.id,
        prof: dat.id,
        UserID: dat.UserID,
      };

      if (dat.isProfesional) {
        Navigation.navigate('Nota', {data: nav});
        props.setResfresca(true);
      } else {
        props.setModalVisible(false);
        props.setResfresca(true);
      }

      console.log('mascota creada');
    });
  };

  return (
    <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
      <Mutation mutation={NUEVA_MASCOTA}>
        {(crearMascota, {loading, error, data}) => {
          if (error) console.log('mascota add in error : ', error);
          return (
            <View style={styles.formView}>
              <ProgressSteps
                completedProgressBarColor={colors.main}
                activeStepIconColor={colors.main}
                completedStepIconColor={colors.main}
                activeLabelColor={colors.main}
                activeStepIconBorderColor={colors.main}
                activeStepNumColor={colors.white}
                disabledStepNumColor={colors.rgb_153}>
                <ProgressStep
                  label="Nombre"
                  nextBtnStyle={name ? styles.applyActive : styles.apply}
                  nextBtnTextStyle={{color: colors.white}}
                  errors={name ? false : true}
                  nextBtnText="Siguiente">
                  <View style={styles.viewBox}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[stylesText.mainText, {marginBottom: 30}]}>
                      ¿Cómo se llama tu mascota?
                    </CustomText>
                    <OutlinedTextField
                      label="Nombre"
                      placeholder="Nombre"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setName(values)}
                    />
                  </View>
                </ProgressStep>
                <ProgressStep
                  label="Foto"
                  nextBtnStyle={avatar ? styles.applyActive : styles.apply}
                  nextBtnTextStyle={{color: colors.white}}
                  previousBtnStyle={styles.applyActive}
                  previousBtnTextStyle={{color: colors.white}}
                  errors={avatar ? false : true}
                  nextBtnText="Siguiente"
                  previousBtnText="Atrás">
                  <View style={styles.viewBox}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[stylesText.mainText, {marginBottom: 30}]}>
                      Sube una foto de {name}
                    </CustomText>
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Mutation mutation={UPLOAD_FILE}>
                        {singleUpload => (
                          <TouchableOpacity
                            onPress={() => selectPhotoTapped(singleUpload)}>
                            {Loading ? (
                              <View style={{left: -30}}>
                                <BubblesLoader color={colors.main} size={140} />
                              </View>
                            ) : (
                              <Avatar
                                rounded
                                size={160}
                                source={{
                                  uri: NETWORK_INTERFACE_LINK_AVATAR + avatar,
                                }}
                                containerStyle={styles.avatarst}
                                showEditButton
                              />
                            )}
                          </TouchableOpacity>
                        )}
                      </Mutation>
                    </View>
                  </View>
                </ProgressStep>
                <ProgressStep
                  label="Raza"
                  nextBtnStyle={
                    especie && raza ? styles.applyActive : styles.apply
                  }
                  nextBtnTextStyle={{color: colors.white}}
                  previousBtnStyle={styles.applyActive}
                  previousBtnTextStyle={{color: colors.white}}
                  errors={especie && raza ? false : true}
                  nextBtnText="Siguiente"
                  previousBtnText="Atrás">
                  <View style={styles.viewBox}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[stylesText.mainText, {marginBottom: 30}]}>
                      Hablanos sobre {name}
                    </CustomText>

                    <OutlinedTextField
                      label="Especie"
                      placeholder="Especie"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setEspecie(values)}
                    />

                    <OutlinedTextField
                      label="Raza"
                      placeholder="Raza"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setRaza(values)}
                    />
                  </View>
                </ProgressStep>

                <ProgressStep
                  label="Características"
                  nextBtnStyle={
                    genero && peso ? styles.applyActive : styles.apply
                  }
                  nextBtnTextStyle={{color: colors.white}}
                  previousBtnStyle={styles.applyActive}
                  previousBtnTextStyle={{color: colors.white}}
                  errors={genero && peso ? false : true}
                  nextBtnText="Siguiente"
                  previousBtnText="Atrás">
                  <View style={styles.viewBox}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[stylesText.mainText, {marginBottom: 30}]}>
                      Características de {name}
                    </CustomText>
                    <OutlinedTextField
                      label="Genero"
                      placeholder="Género"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setGenero(values)}
                    />

                    <OutlinedTextField
                      label="Peso"
                      placeholder="Peso"
                      keyboardType="number-pad"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setPeso(values)}
                      maxLength={2}
                    />

                    <OutlinedTextField
                      label="Código microchip"
                      placeholder="Código microchip (Opcional)"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setMicrochip(values)}
                    />
                  </View>
                </ProgressStep>

                <ProgressStep
                  label="Salud"
                  nextBtnStyle={
                    genero && peso ? styles.applyActive : styles.apply
                  }
                  nextBtnTextStyle={{color: colors.white}}
                  previousBtnStyle={styles.applyActive}
                  previousBtnTextStyle={{color: colors.white}}
                  errors={genero && peso ? false : true}
                  finishBtnText="Guardar"
                  previousBtnText="Atrás"
                  onSubmit={() => handlePublish(crearMascota)}>
                  <View style={styles.viewBox}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={[stylesText.mainText, {marginBottom: 30}]}>
                      Como va la salud de {name}
                    </CustomText>

                    <OutlinedTextField
                      label="Vacunas"
                      placeholder="Vacunas"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setVacunas(values)}
                    />

                    <OutlinedTextField
                      label="Alergías"
                      placeholder="Alergías"
                      keyboardType="default"
                      inputContainerStyle={{
                        color: colors.rgb_153,
                        marginTop: 10,
                        width: dimensions.Width(88),
                      }}
                      textColor={colors.rgb_153}
                      labelTextStyle={{color: colors.rgb_153}}
                      placeholderTextColor={colors.rgb_153}
                      onChangeText={values => setAlergias(values)}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: dimensions.Height(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.light_white}>
                        ¿Castrado o esterilizado? Si / No
                      </CustomText>
                      <Switch
                        checked={castrado}
                        onChange={() => onSwitchChange()}
                        style={{marginLeft: 'auto', marginRight: 50}}
                        trackColor={colors.main1}
                      />
                    </View>

                    <View
                      style={{
                        marginTop: dimensions.Height(2),
                        width: dimensions.Width(100),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={{
                          marginTop: dimensions.Height(2),
                          marginBottom: dimensions.Height(2),
                        }}>
                        Selecciona la fecha de nacimiento
                      </CustomText>
                      <DatePicker
                        style={{
                          width: dimensions.Width(100),
                          height: 60,
                        }}
                        date={date}
                        locale="ES"
                        mode="date"
                        onDateChange={setDate}
                      />
                    </View>
                  </View>
                </ProgressStep>
              </ProgressSteps>
            </View>
          );
        }}
      </Mutation>
    </KeyboardAwareScrollView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },
  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  cardinfo: {
    width: dimensions.Width(95),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
    padding: dimensions.Height(1),
  },

  avatarst: {
    marginLeft: -50,
  },

  name: {
    marginTop: dimensions.Height(1),
    alignSelf: 'center',
    fontSize: dimensions.FontSize(24),
  },

  name1: {
    fontSize: dimensions.FontSize(24),
  },

  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(0),
    marginBottom: dimensions.Height(15),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(1),
    alignSelf: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  viewBox: {
    width: dimensions.Width(100),
    height: 'auto',
    marginTop: 30,
    marginBottom: 30,
  },

  apply: {
    backgroundColor: new DynamicValue(colors.light_grey, colors.rgb_153),
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: colors.white,
    top: 30,
    left: 60,
  },

  applyActive: {
    backgroundColor: new DynamicValue(colors.main, colors.main),
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: colors.white,
    top: 30,
    left: 60,
  },
});

export default AddMascota;
