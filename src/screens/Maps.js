import React, {useEffect, useState} from 'react';
import {View, PermissionsAndroid, TouchableOpacity} from 'react-native';
import Navigation from './../services/NavigationService';
import MapView, {Marker} from 'react-native-maps';
import {dimensions} from '../themes';
import ProfesionalMaps from '../components/Profesionalmaps';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';
import Icon from 'react-native-dynamic-vector-icons';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {colors} from '../themes';

async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Vetec App',
        message:
          'Vetec quieres utilizar tu ubicación para mostrarte profesionales cerca de ti.',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the location');
    } else {
      console.log('location permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
}

export default function Maps() {
  const [city, setCity] = useState('');
  const [lat, setLat] = useState('');
  const [lgn, setLgn] = useState('');

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    RNLocation.requestPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
        rationale: {
          title: 'Vetec necesita usar tu ubicación',
          message:
            'Necesitamos tu ubicación para mostrarte profesionales cerca de ti',
          buttonPositive: 'OK',
          buttonNegative: 'Cancelar',
        },
      },
    });
    Geolocation.getCurrentPosition(info => {
      setLat(info.coords.latitude);
      setLgn(info.coords.longitude);
    });
  }, []);

  useEffect(() => {
    requestLocationPermission();

    const SetUbicacion = () => {
      let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lgn}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
      fetch(apiUrlWithParams)
        .then(response => response.json())
        .then(data => {
          let cityFound = false;

          for (let index = 0; index < data.results.length; index++) {
            for (
              let i = 0;
              i < data.results[index].address_components.length;
              i++
            ) {
              if (
                data.results[index].address_components[i].types.includes(
                  'locality',
                )
              ) {
                setCity(data.results[index].address_components[i].long_name);
                cityFound = true;
              }

              if (cityFound) break;
            }

            if (cityFound) break;
          }
        })
        .catch(error => {
          // this.setState({ locationFilterChecked: false });
          console.log('error in product-plan getting lat, lng: ', error);
        });
    };

    SetUbicacion();
  });

  console.log(lat, lgn);

  const latitudes = lat ? lat : 42.344184;
  const longitudes = lgn ? lgn : -3.7297124;

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => Navigation.goBack(null)}
        style={{
          marginLeft: 15,
          position: 'absolute',
          ...ifIphoneX(
            {
              marginTop: 50,
            },
            {
              marginTop: 40,
            },
          ),
          zIndex: 100,
        }}>
        <Icon name="close" type="AntDesign" size={25} color={colors.ERROR} />
      </TouchableOpacity>
      <View
        style={{height: dimensions.Height(95), width: dimensions.Width(100)}}>
        <MapView
          style={{
            height: dimensions.Height(100),
            width: dimensions.Width(100),
          }}
          region={{
            latitude: latitudes,
            longitude: longitudes,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker coordinate={{latitude: latitudes, longitude: longitudes}} />
        </MapView>

        <View style={styles.cars}>
          <ProfesionalMaps navigation={Navigation} ciudad={city} />
        </View>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  cars: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    position: 'absolute',
    bottom: 0,
  },
});
