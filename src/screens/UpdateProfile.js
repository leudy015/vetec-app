import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, Alert, PermissionsAndroid} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {useNavigationParam} from 'react-navigation-hooks';
import ImagePicker from 'react-native-image-picker';
import {Avatar} from 'react-native-elements';
import {Button} from './../components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ACTUALIZAR_USUARIO, UPLOAD_FILE, ELIMINAR_USUARIO} from '../mutations';
import {Mutation} from 'react-apollo';
import {Switch} from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {validateEmail} from '../constants/emailValidator';
import {OutlinedTextField} from 'react-native-material-textfield';
import {stylesText} from '../components/StyleText';
import Geolocation from '@react-native-community/geolocation';
import RNLocation from 'react-native-location';

async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Vetec App',
        message:
          'Vetec quieres utilizar tu ubicación para mostrarte profesionales cerca de ti.',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the location');
    } else {
      console.log('location permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
}

export default function UpdateProfile() {
  const data = useNavigationParam('data');
  const [email, setEmail] = useState(data.email);
  const [nombre, setNombre] = useState(data.nombre);
  const [apellidos, setApellidos] = useState(data.apellidos);
  const [ciudad, setCiudad] = useState(data.ciudad);
  const [telefono, setTelefono] = useState(data.telefono);
  const [notificacion, setNotificacion] = useState(data.notificacion);
  const [avatar, setAvatar] = useState(data.avatar);
  const [city, setCity] = useState(null);
  const [lat, setLat] = useState(null);
  const [lgn, setLgn] = useState(null);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const onSwitchChange = () => {
    if (notificacion === true) {
      setNotificacion(false);
    } else {
      setNotificacion(true);
    }
  };

  let very = {
    id: data.id,
    fromProfile: true,
  };

  useEffect(() => {
    RNLocation.requestPermission({
      ios: 'whenInUse', // or 'always'
      android: {
        detail: 'coarse', // or 'fine'
        rationale: {
          title: 'Vetec necesita usar tu ubicación',
          message:
            'Necesitamos tu ubicación para mostrarte profesionales cerca de ti',
          buttonPositive: 'OK',
          buttonNegative: 'Cancelar',
        },
      },
    });
    Geolocation.getCurrentPosition(info => {
      setLat(info.coords.latitude);
      setLgn(info.coords.longitude);
    });
  }, []);

  useEffect(() => {
    requestLocationPermission();

    const SetUbicacion = () => {
      let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lgn}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
      fetch(apiUrlWithParams)
        .then(response => response.json())
        .then(data => {
          let cityFound = false;

          for (let index = 0; index < data.results.length; index++) {
            for (
              let i = 0;
              i < data.results[index].address_components.length;
              i++
            ) {
              if (
                data.results[index].address_components[i].types.includes(
                  'locality',
                )
              ) {
                setCity(data.results[index].address_components[i].long_name);
                cityFound = true;
              }

              if (cityFound) break;
            }

            if (cityFound) break;
          }
        })
        .catch(error => {
          // this.setState({ locationFilterChecked: false });
          console.log('error in product-plan getting lat, lng: ', error);
        });
    };

    SetUbicacion();
  });

  const handleEliminar = eliminarUsuario => {
    return new Promise((resolve, reject) => {
      eliminarUsuario({variables: {id: data.id}}).then(async ({data}) => {
        if (data && data.eliminarUsuario && data.eliminarUsuario.success) {
          setTimeout(async () => {
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('id');
            console.log(data.eliminarUsuario.message);
            setTimeout(() => Navigation.navigate('Landing'), 300);
          }, 200);
          resolve();
        } else if (
          data &&
          data.eliminarUsuario &&
          !data.eliminarUsuario.success
        )
          setTimeout(() => {
            console.log(data.eliminarUsuario.message);
          }, 500);
        reject();
      });
    }).catch(() => console.log('error confirming eliminar usuario!'));
  };

  const handleSubmit = async mutation => {
    const input = {
      id: data.id,
      nombre: nombre,
      avatar: avatar,
      apellidos: apellidos,
      email: email,
      telefono: telefono,
      ciudad: ciudad ? ciudad : city,
      notificacion: notificacion,
    };

    if (!validateEmail(email)) {
      Alert.alert(
        'Email no valido',
        'Introduce un email valido para continuar.',
        [
          {
            text: 'Ok',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
    } else {
      if (!avatar) {
        Alert.alert(
          'Debes añadir una foto de perfil',
          'Para guardar los cambios en tu perfil debes añadir una foto de perfil.',
          [
            {
              text: 'Ok',
              onPress: () => console.log('calcel'),
            },
          ],

          {cancelable: false},
        );
        return null;
      } else {
        mutation({variables: {input}})
          .then(res => {
            console.log('done', res);
            Toast.showWithGravity(
              'Perfil actualizado con éxito',
              Toast.LONG,
              Toast.TOP,
            );
          })
          .catch(err => console.log(err));
      }
    }
  };

  const selectPhotoTapped = singleUpload => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar Avatar',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async response => {
      if (response) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({variables: {imgBlob}})
            .then(res => {
              console.log('filemane: ', res.data.singleUpload.filename);
              setAvatar(res.data.singleUpload.filename);
            })
            .catch(error => {
              console.log('fs error: que me arroja', error);
            });
        }
      }
    });
  };

  return (
    <Mutation mutation={ACTUALIZAR_USUARIO}>
      {mutation => {
        return (
          <View style={styles.container}>
            <View>
              <Headers
                navigation={Navigation}
                title="Actualizar perfil"
                back={true}
              />
            </View>
            <CustomText
              light={colors.black}
              dark={colors.white}
              numberOfLines={1}
              style={[stylesText.h1, {marginLeft: 15, marginBottom: 20}]}>
              Editar datos
            </CustomText>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
              <View style={{alignSelf: 'center'}}>
                <Mutation mutation={UPLOAD_FILE}>
                  {singleUpload => (
                    <TouchableOpacity
                      onPress={() => selectPhotoTapped(singleUpload)}>
                      <Avatar
                        rounded
                        size={120}
                        source={{
                          uri: NETWORK_INTERFACE_LINK_AVATAR + avatar,
                        }}
                        containerStyle={styles.avatarst}
                        showEditButton
                      />
                    </TouchableOpacity>
                  )}
                </Mutation>
              </View>
              <CustomText
                light={colors.back_dark}
                dark={colors.light_white}
                style={[styles.name, {marginBottom: 50}]}>
                {data.nombre} {data.apellidos}
              </CustomText>
              <View style={styles.formView}>
                <View style={styles.cars}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.texto}>
                    Información personal
                  </CustomText>
                  <OutlinedTextField
                    label="Nombre"
                    keyboardType="default"
                    inputContainerStyle={{color: colors.rgb_153}}
                    defaultValue={nombre}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setNombre(values)}
                  />

                  <OutlinedTextField
                    label="Apellidos"
                    keyboardType="default"
                    defaultValue={apellidos}
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setApellidos(values)}
                  />

                  <OutlinedTextField
                    label="Ciudad"
                    keyboardType="default"
                    defaultValue={ciudad ? ciudad : city}
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setCiudad(values)}
                  />
                </View>

                <View style={styles.cars}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.texto}>
                    Información de contacto
                  </CustomText>
                  <OutlinedTextField
                    label="Número móvil"
                    keyboardType="phone-pad"
                    defaultValue={telefono}
                    inputContainerStyle={{color: colors.rgb_153}}
                    textColor={colors.rgb_153}
                    maxLength={11}
                    onFocus={() =>
                      Navigation.navigate('VerifyPhone', {data: very})
                    }
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setTelefono(values)}
                  />

                  <OutlinedTextField
                    label="Email"
                    keyboardType="email-address"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    defaultValue={email}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setEmail(values)}
                  />
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: dimensions.Height(4),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.light_white}>
                    Activar notificaciones
                  </CustomText>
                  <Switch
                    checked={notificacion}
                    onChange={() => onSwitchChange()}
                    style={{marginLeft: 'auto'}}
                    trackColor={colors.main1}
                  />
                </View>

                <View style={styles.signupButtonContainer}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={styles.buttonView}
                    onPress={() => handleSubmit(mutation)}
                    title="Guardar cambios"
                    titleStyle={styles.buttonTitle}
                  />

                  <View style={{alignSelf: 'center'}}>
                    <Mutation mutation={ELIMINAR_USUARIO}>
                      {eliminarUsuario => {
                        return (
                          <TouchableOpacity
                            onPress={() =>
                              Alert.alert(
                                '¿Deseas eliminar tu cuenta?',
                                'Al eliminar tu cuenta se eliminarán todos los datos y historial clínico de tu mascota.',
                                [
                                  {
                                    text: 'Cancelar',
                                    onPress: () => console.log('calcel'),
                                  },
                                  {
                                    text: 'Eliminar',
                                    onPress: () =>
                                      handleEliminar(eliminarUsuario),
                                  },
                                ],

                                {cancelable: false},
                              )
                            }>
                            <CustomText
                              style={{
                                color: colors.ERROR,
                                marginTop: dimensions.Height(4),
                              }}>
                              Eliminar cuenta
                            </CustomText>
                          </TouchableOpacity>
                        );
                      }}
                    </Mutation>
                  </View>
                </View>
              </View>
            </KeyboardAwareScrollView>
          </View>
        );
      }}
    </Mutation>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  ted: {
    fontSize: dimensions.FontSize(18),
    marginTop: 20,
  },

  cars: {
    width: dimensions.Width(88),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: dimensions.Width(2),
    margin: 2,
    marginBottom: dimensions.Height(5),
    padding: 20,
  },

  texto: {
    fontSize: dimensions.FontSize(18),
    marginBottom: dimensions.Height(3),
  },

  cardinfo: {
    width: dimensions.Width(90),
    height: dimensions.Height(40),
    backgroundColor: new DynamicValue(colors.light_white, colors.back_dark),
    marginLeft: dimensions.Width(5),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 12,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatarst: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 4,
  },

  name: {
    marginTop: dimensions.Height(1),
    alignSelf: 'center',
    fontSize: dimensions.FontSize(24),
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(15),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  noverify: {
    width: dimensions.Width(100),
    height: 70,
    backgroundColor: '#ffe58f',
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 15,
    flexDirection: 'row',
  },
  btns: {
    borderColor: colors.orange,
    borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 15,
    borderRadius: 55,
  },
});
