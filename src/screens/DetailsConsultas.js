import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Linking,
  Modal,
  SafeAreaView,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {Button} from './../components/Button';
import {Avatar} from 'react-native-elements';
import {
  CREATE_NOTIFICATION,
  PROFESSIONAL_CONSULTA_PROCEED,
} from './../mutations';
import {useMutation} from '@apollo/react-hooks';
import {withApollo, Mutation} from 'react-apollo';
import moment from 'moment';
import 'moment/locale/es';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from '../constants/config';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import StarRating from 'react-native-star-rating';
import {TextareaItem} from '@ant-design/react-native';
import Toast from 'react-native-simple-toast';
import io from 'socket.io-client';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {stylesText} from '../components/StyleText';
import {
  ConversationOptions,
  Freshchat,
  FreshchatUser,
} from 'react-native-freshchat-sdk';

var conversationOptions = new ConversationOptions();
conversationOptions.tags = ['premium'];
conversationOptions.filteredViewTitle = 'Premium Support';

const socket = io(NETWORK_INTERFACE_LINK, {
  forceNew: true,
});

const window = Dimensions.get('window');

function DetailsPro() {
  const [id, setId] = useState(null);
  const [messageTexteresolucion] = useState(
    'El cliente ha abierto una reclamación de la consulta pronto tendrás noticias de la resolución',
  );
  const [modalVisible, setModalVisible] = useState(false);
  const [val, setVal] = useState('');
  const [starCount, setStarCount] = useState(0);
  const [messageTexte] = useState(
    'Enhora buena el paciente ha valorado tu trabajo.',
  );

  const openModal = () => {
    setModalVisible(true);
  };

  const onStarRatingPress = rating => {
    setStarCount(rating);
  };

  const onChange = val => {
    setVal(val);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  socket.on('connect', () => {
    socket.emit('online_user', {
      usuario: data.usuario.id,
      profesional: data.profesional.id,
      id: data.usuario.id,
    });
  });

  var freshchatUser = new FreshchatUser();
  freshchatUser.firstName = data.usuario.nombre;
  freshchatUser.lastName = data.usuario.apellidos;
  freshchatUser.email = data.usuario.email;
  freshchatUser.phoneCountryCode = '+43';
  freshchatUser.phone = data.usuario.telefono;

  Freshchat.setUser(freshchatUser, error => {
    console.log(error);
  });

  const SendPushNotificationresolucion = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification-profesional?IdOnesignal=${
        data.profesional.UserID
      }&textmessage=${messageTexteresolucion}`,
    ).catch(err => console.log(err));
  };

  const handleClickMail = async () => {
    Linking.canOpenURL(
      `mailto:info@vetec.es?subject=Incidente con consulta No. ${data.id}`,
    ).then(async supported => {
      if (supported) {
        Linking.openURL(
          `mailto:info@vetec.es?subject=Incidente con consulta No. ${data.id}`,
        );
        console.log('calling: ' + data.id);

        const NotificationInput = {
          consulta: data.id,
          user: data.profesional.id,
          usuario: id,
          profesional: data.profesional.id,
          type: 'valored_order',
        };
        await createNotification({variables: {input: NotificationInput}})
          .then(async results => {
            console.log('results', results);
          })
          .catch(err => {
            console.log('err', err);
          });

        SendPushNotificationresolucion();
      } else {
        console.log("Don't know how to open URI: " + data.id);
      }
    });
  };

  const Navigationachat = () => {
    socket.emit('available', {
      user: data.usuario.id,
      prof: data.profesional.id,
    });
    Navigation.navigate('ChatsScreens', {data: data});
  };

  const valorarprofesional = consultaProceed => {
    const formData = {
      consultaID: data.id,
      estado: 'Valorada',
      progreso: '100',
      status: 'success',
      coment: val,
      rate: starCount,
    };
    const {coment, rate} = formData;
    if ((rate == '', coment == '')) {
      Alert.alert(
        'Error al valorara el profesional',
        'Debes comentar la experiencia para poder valorara este profesional',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return null;
    }
    console.log('esto es lo qie se esya enviadop', formData);
    consultaProceed({variables: formData})
      .then(async res => {
        if (
          res &&
          res.data &&
          res.data.consultaProceed &&
          res.data.consultaProceed.success
        ) {
          Toast.showWithGravity(
            'Profesional valorado éxitosamente',
            Toast.LONG,
            Toast.TOP,
          );
          console.log('Has valorado el profesional éxitosamente', res);
          setTimeout(() => {
            setTimeout(() => {
              setModalVisible(!modalVisible);
            }, 2000);
          });

          const NotificationInput = {
            consulta: data.id,
            user: data.profesional.id,
            usuario: id,
            profesional: data.profesional.id,
            type: 'valored_order',
          };
          createNotification({variables: {input: NotificationInput}})
            .then(async results => {
              console.log('results', results);
            })
            .catch(err => {
              console.log('err', err);
            });

          const SendPushNotificationvalorar = () => {
            fetch(
              `${NETWORK_INTERFACE_LINK}/send-push-notification-profesional?IdOnesignal=${
                data.profesional.UserID
              }&textmessage=${messageTexte}`,
            ).catch(err => console.log(err));
            console.log(data.profesional.UserID, messageTexte);
          };

          SendPushNotificationvalorar();
        }
      })
      .catch(err => {
        console.log(
          'Algo salió mal. Por favor intente nuevamente en un momento.',
          err,
        );
      });
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              blurRadius={10}
              style={styles.background}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
              }}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
              }}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <Icon
                  name="close"
                  type="AntDesign"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.stickySectionText}>
                {data.profesional.nombre} {data.profesional.apellidos}
                {''}{' '}
                {data.profesional.isVerified ? (
                  <Icon
                    name="verified"
                    type="Octicons"
                    size={14}
                    style={{
                      alignSelf: 'center',
                      marginTop: 0,
                      marginLeft: 0,
                      color: colors.main,
                    }}
                  />
                ) : null}
              </CustomText>
            </View>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.white}
                style={[
                  stylesText.mainText,
                  {maxWidth: dimensions.Width(50), width: 'auto'},
                ]}>
                {data.profesional.nombre} {data.profesional.apellidos}
              </CustomText>
              {data.profesional.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={14}
                  style={{
                    alignSelf: 'center',
                    marginTop: 5,
                    marginLeft: 7,
                    color: colors.main,
                  }}
                />
              ) : null}
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {
                    color: colors.rgb_153,
                    marginLeft: 'auto',
                  },
                ]}>
                {data.profesional.Precio}€ /Total
              </CustomText>
            </View>

            <View style={{flexDirection: 'row'}}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[
                  stylesText.secondaryText,
                  {marginTop: dimensions.Height(1)},
                ]}>
                {data.profesional.profesion}
              </CustomText>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: dimensions.Height(1),
                    marginLeft: 'auto',
                    color: colors.rgb_153,
                  },
                ]}>
                <Icon
                  name="enviromento"
                  type="AntDesign"
                  size={14}
                  color={colors.main}
                />{' '}
                {data.profesional.ciudad}
              </CustomText>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: dimensions.Height(1),
                  marginBottom: dimensions.Height(3),
                },
              ]}>
              {data.profesional.experiencia} de experiencia
            </CustomText>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: dimensions.Width(4),
              borderTopColor: colors.rgb_235,
              borderTopWidth: 0.5,
            }}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.mainText, {marginTop: 8}]}>
              Estado
            </CustomText>
            <CustomText
              style={[
                stylesText.secondaryText,
                {
                  color: colors.rgb_102,
                  marginLeft: 'auto',
                  marginTop: 8,
                },
              ]}>
              {data.estado}
            </CustomText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: dimensions.Width(4),
              borderBottomColor: colors.rgb_235,
              borderBottomWidth: 0.5,
              paddingBottom: 10,
            }}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.mainText, {marginTop: 8}]}>
              Fecha
            </CustomText>
            <CustomText
              style={[
                stylesText.secondaryText,
                {
                  color: colors.rgb_102,
                  marginLeft: 'auto',
                  marginTop: 8,
                },
              ]}>
              {moment(Number(data.created_at)).format('LLL')}
            </CustomText>
          </View>

          <View
            style={{
              marginTop: dimensions.Height(3),
              paddingHorizontal: dimensions.Width(4),
              alignSelf: 'center',
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={[
                stylesText.mainText,
                {
                  marginTop: 8,
                },
              ]}>
              Mascota
            </CustomText>

            <TouchableOpacity
              style={styles.cardinfo}
              onPress={() =>
                Navigation.navigate('DetailsMascota', {data: data.mascota})
              }>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(4),
                }}>
                <Avatar
                  rounded
                  size={50}
                  source={{
                    uri: NETWORK_INTERFACE_LINK_AVATAR + data.mascota.avatar,
                  }}
                  containerStyle={styles.avatarmascota}
                />
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(4),
                }}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.name}>
                  {data.mascota.name}
                </CustomText>
                <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                  {moment(data.mascota.age).format('ll')}
                </CustomText>
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(4),
                }}>
                <Icon
                  name="right"
                  type="AntDesign"
                  size={20}
                  color={colors.rgb_153}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingHorizontal: dimensions.Width(4),
              marginTop: dimensions.Height(3),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={[
                stylesText.mainText,
                {
                  marginTop: 8,
                },
              ]}>
              Sintomas o problema con la mascota
            </CustomText>

            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {
                  marginTop: 10,
                },
              ]}>
              {data.nota}
            </CustomText>
          </View>
        </View>
      </ParallaxScrollView>
      <View style={styles.fixs}>
        {data.estado === 'Aceptado' ? (
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigationachat()}
              title="Chat"
              titleStyle={styles.buttonTitle}
            />
          </View>
        ) : null}

        {data.estado !== 'Aceptado' ? (
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView1}
              onPress={() => Freshchat.showConversations(conversationOptions)}
              title="Ayuda"
              titleStyle={styles.buttonTitle}
            />
          </View>
        ) : null}

        {data.estado === 'Completada' ? (
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView1}
              onPress={() => openModal()}
              title="Valorar"
              titleStyle={styles.buttonTitle}
            />
          </View>
        ) : null}

        <Modal
          style={{height: dimensions.Height(80)}}
          animationType="slide"
          presentationStyle="formSheet"
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => Alert.alert('Seguro que deseas salir')}>
          <View style={styles.container}>
            <View style={{height: dimensions.Height(100)}}>
              <SafeAreaView style={styles.headers}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                  <View style={{alignItems: 'flex-start', marginLeft: 20}} />
                  <View style={{marginLeft: 'auto', flexDirection: 'row'}}>
                    <TouchableOpacity
                      onPress={() => setModalVisible(!modalVisible)}>
                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={{marginRight: 20}}>
                        <Icon
                          type="AntDesign"
                          name="close"
                          size={25}
                          color={colors.ERROR}
                        />
                      </CustomText>
                    </TouchableOpacity>
                  </View>
                </View>
              </SafeAreaView>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View
                  style={{
                    flex: 1,
                    paddingHorizontal: dimensions.Width(4),
                    marginTop: dimensions.Height(4),
                  }}>
                  <CustomText
                    light={colors.rgb_153}
                    dark={colors.rgb_153}
                    style={{
                      fontSize: dimensions.FontSize(18),
                      fontWeight: '200',
                    }}>
                    Valora el servios brindado por el profeisonal para ayudar a
                    otros clientes como tu.
                  </CustomText>
                  <View
                    style={{
                      marginLeft: 15,
                      marginTop: 20,
                      borderBottomWidth: 0.5,
                      borderBottomColor: colors.rgb_153,
                      paddingBottom: 15,
                    }}>
                    <Avatar
                      containerStyle={{}}
                      rounded
                      source={{
                        uri:
                          NETWORK_INTERFACE_LINK_AVATAR +
                          data.profesional.avatar,
                      }}
                    />

                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={{
                        fontSize: dimensions.FontSize(18),
                        fontWeight: '200',
                        marginTop: 10,
                      }}>
                      {data.profesional.nombre} {data.profesional.apellidos}
                    </CustomText>
                  </View>

                  <View
                    style={{
                      marginTop: dimensions.Height(5),
                      alignSelf: 'center',
                    }}>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={{
                        fontSize: dimensions.FontSize(18),
                        fontWeight: '200',
                        marginBottom: 20,
                      }}>
                      Dinos cual fue tu experiencia con el servicio.
                    </CustomText>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      rating={starCount}
                      selectedStar={rating => onStarRatingPress(rating)}
                      fullStarColor={colors.orange}
                    />
                  </View>
                  <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                    <View style={{marginTop: dimensions.Height(5)}}>
                      <TextareaItem
                        rows={5}
                        placeholder="Comentarío"
                        count={160}
                        value={val}
                        onChange={onChange}
                        style={{
                          backgroundColor: 'transparent',
                          color: colors.rgb_153,
                        }}
                        placeholderTextColor={colors.rgb_153}
                      />
                    </View>
                    <View style={{alignSelf: 'center'}}>
                      <Mutation mutation={PROFESSIONAL_CONSULTA_PROCEED}>
                        {consultaProceed => (
                          <View style={styles.signupButtonContainer1}>
                            <Button
                              light={colors.white}
                              dark={colors.white}
                              containerStyle={styles.buttonView}
                              onPress={() =>
                                valorarprofesional(consultaProceed)
                              }
                              title="Valorar"
                              titleStyle={styles.buttonTitle}
                            />
                          </View>
                        )}
                      </Mutation>
                    </View>
                  </KeyboardAwareScrollView>
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 50,
      },
      {
        left: 45,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
  },

  name: {
    marginTop: dimensions.Height(0),
    fontSize: dimensions.FontSize(24),
  },

  fixedSectionText1: {
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  title: {
    fontSize: dimensions.FontSize(18),
    fontWeight: 'bold',
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },
  prof: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 20,
  },

  cardop: {
    width: dimensions.Width(92),
    height: dimensions.Height(10),
    paddingHorizontal: dimensions.Width(4),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  rating: {
    alignSelf: 'center',
    fontSize: dimensions.FontSize(50),
    fontWeight: 'bold',
  },

  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(12),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(3),
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },

  buttonView1: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },

  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  cardinfo: {
    width: dimensions.Width(94),
    height: 80,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
  },
});

export default withApollo(DetailsPro);
