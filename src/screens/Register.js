import React, {useState} from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {colors, dimensions} from './../themes';
import {OutlinedTextField} from 'react-native-material-textfield';
import {CustomText} from './../components/CustomText';
import {Button} from './../components/Button';
import {Mutation, withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import {NUEVO_USUARIO} from './../mutations';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Headers from '../components/Header';
import Icon from 'react-native-dynamic-vector-icons';
import WebViweb from '../components/Webview';
import {LineDotsLoader} from 'react-native-indicator';
import {stylesText} from '../components/StyleText';
import {validateEmail} from '../constants/emailValidator';

function Register() {
  const [nombre, setNombre] = useState('');
  const [apellidos, setApellidos] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [names, setNames] = useState('eye');
  const [visibleModal, setvisibleModal] = useState(false);
  const [Loading, setLoading] = useState(false);

  const setmostrar = () => {
    if (secureTextEntry) {
      setSecureTextEntry(false);
      setNames('eye-off');
    } else {
      setSecureTextEntry(true);
      setNames('eye');
    }
  };

  const abrirweb = () => {
    setvisibleModal(true);
  };

  const cerrarweb = () => {
    setvisibleModal(false);
  };

  const handleSubmit = async crearUsuario => {
    setLoading(true);
    if (!validateEmail(email)) {
      Alert.alert(
        'Email invalido',
        'Introduce un email valido para continuar',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      setLoading(false);
    } else {
      if ((!password, !email, !nombre, !apellidos)) {
        Alert.alert(
          'Error con el registro',
          'Todos los campos son obligatorio para el registro de usuario',
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
        setLoading(false);
        return null;
      }
      await crearUsuario({
        variables: {
          input: {
            nombre: nombre,
            apellidos: apellidos,
            email: email,
            password: password,
          },
        },
      }).then(async ({data: res}) => {
        if (res && res.crearUsuario && res.crearUsuario.success) {
          setLoading(false);
          console.log(res.crearUsuario.message);
          const datss = res && res.crearUsuario && res.crearUsuario.data;
          if (datss) {
            const createclient = async () => {
              let res = await fetch(
                NETWORK_INTERFACE_LINK +
                  `/create-client?userID=${datss.id}&nameclient=${datss.nombre +
                    datss.apellidos}&email=${datss.email}`,
              );
              const deletec = await res.json();
              console.log(deletec);
            };
            const saveEmail = async () => {
              let res = await fetch(
                NETWORK_INTERFACE_LINK +
                  `/save-email?email=${datss.email}&name=${datss.nombre +
                    datss.apellidos}`,
              );
              const deletec = await res.json();
              console.log(deletec);
            };
            createclient();
            saveEmail();
          } else {
            setLoading(false);
            return null;
          }

          let very = {
            id: datss.id,
          };

          Navigation.navigate('VerifyPhone', {data: very});
          console.log(datss.id);
        } else {
          console.log(res.crearUsuario.message);
          setLoading(false);
          Alert.alert(
            'Registro de usuario',
            res.crearUsuario.message,
            [{text: 'OK', onPress: () => console.log('OK Pressed')}],
            {cancelable: false},
          );
        }
      });
    }
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <Mutation mutation={NUEVO_USUARIO}>
      {crearUsuario => {
        return (
          <View style={styles.container}>
            <View>
              <Headers
                navigation={Navigation}
                title="Crear una cuenta"
                back={true}
                rigth={true}
              />
            </View>
            <View style={styles.contenedor}>
              <KeyboardAwareScrollView
                keyboardShouldPersistTaps="always"
                showsVerticalScrollIndicator={false}
                style={{marginBottom: dimensions.Height(10)}}>
                <View
                  style={{
                    marginTop: dimensions.Height(5),
                    marginLeft: dimensions.Width(8),
                    marginBottom: dimensions.Height(5),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    Bienvenido de
                  </CustomText>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    a Vetec !
                  </CustomText>
                </View>
                <View style={styles.formView}>
                  <OutlinedTextField
                    label="Nombre"
                    placeholder="Nombre"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setNombre(values)}
                  />
                  <OutlinedTextField
                    label="Apellidos"
                    placeholder="Apellidos"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setApellidos(values)}
                  />
                  <OutlinedTextField
                    label="Email"
                    placeholder="Email"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setEmail(values)}
                  />
                  <OutlinedTextField
                    label="Contraseña"
                    keyboardType="default"
                    secureTextEntry={secureTextEntry}
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholder="Contraseña"
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setPassword(values)}
                  />
                  <TouchableOpacity
                    onPress={() => setmostrar()}
                    style={{
                      alignSelf: 'flex-end',
                      marginTop: dimensions.Height(0),
                      marginBottom: dimensions.Height(0),
                    }}>
                    <CustomText light={colors.back_dark} dark={colors.rgb_102}>
                      <Icon
                        name={names}
                        type="MaterialCommunityIcons"
                        size={20}
                        color={colors.rgb_102}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={stylesText.secondaryText}>
                    Al registrarme he leído y estoy de acuerdo con los:{' '}
                  </CustomText>
                  <TouchableOpacity onPress={() => abrirweb()}>
                    <CustomText
                      light={colors.main}
                      dark={colors.main}
                      style={stylesText.secondaryText}>
                      Téminos y condiciones
                    </CustomText>
                  </TouchableOpacity>
                  <WebViweb
                    url="https://vetec.es/condiciones"
                    visibleModal={visibleModal}
                    cerrarweb={cerrarweb}
                  />
                  <View style={styles.signupButtonContainer}>
                    {Loading ? (
                      <LineDotsLoader color={colors.main} />
                    ) : (
                      <Button
                        light={colors.white}
                        dark={colors.white}
                        containerStyle={styles.buttonView}
                        onPress={() => handleSubmit(crearUsuario)}
                        title="Registrarme"
                        titleStyle={styles.buttonTitle}
                      />
                    )}
                  </View>

                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                    }}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={stylesText.secondaryText}>
                      ¿Ya tienes una cuenta?
                    </CustomText>
                  </View>

                  <TouchableOpacity
                    onPress={() => Navigation.navigate('Login')}
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                      marginBottom: dimensions.Height(10),
                    }}>
                    <CustomText
                      light={colors.main}
                      dark={colors.main}
                      style={stylesText.secondaryText}>
                      Iniciar sesión
                    </CustomText>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>
          </View>
        );
      }}
    </Mutation>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(15),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row',
  },
  rememberText: {
    fontSize: dimensions.FontSize(18),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  h1: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'left',
  },
});

export default connect(
  null,
  {},
)(withApollo(Register));
