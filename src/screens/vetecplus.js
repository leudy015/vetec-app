import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/HeaderPlus';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {CustomText} from './../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import {ScrollView} from 'react-native-gesture-handler';
import {Button} from './../components/Button';
import {useNavigationParam} from 'react-navigation-hooks';
import Swiper from 'react-native-swiper';
import WebViweb from '../components/Webview';
import {stylesText} from '../components/StyleText';
 
export default function Consultas() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');
  const [visibleModal, setvisibleModal] = useState(false);

  const abrirweb = () => {
    setvisibleModal(true);
  };

  const cerrarweb = () => {
    setvisibleModal(false);
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <CustomText
          style={[
            stylesText.mainText,
            {
              textAlign: 'center',
              paddingHorizontal: dimensions.Width(4),
              marginTop: 15,
            },
          ]}
          light={colors.back_dark}
          dark={colors.white}>
          Descubre todas las ventajas que tienen los miembros de{' '}
          <CustomText
            style={stylesText.mainText}
            light={colors.main}
            dark={colors.main}>
            Vetec PLUS
          </CustomText>
        </CustomText>
        <Swiper style={styles.wrapper} autoplay={true} index={0}>
          <View style={styles.viewBox}>
            <Icon
              name="dog"
              type="MaterialCommunityIcons"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Mascotas ilimitadas, todas las que quieras
            </CustomText>
          </View>

          <View style={styles.viewBox}>
            <Icon
              name="checkcircleo"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Verificación de la salud de tu mascota online
            </CustomText>
          </View>
          <View style={styles.viewBox}>
            <Icon
              name="message1"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Consultas a través del chat ilimitada
            </CustomText>
          </View>
          <View style={styles.viewBox}>
            <Icon
              name="videocamera"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Hasta 3 consultas de Video llamadas
            </CustomText>
          </View>
          <View style={styles.viewBox}>
            <Icon
              name="hearto"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Medicamentos para pulgas, garrapatas, y paracitos
            </CustomText>
          </View>
          <View style={styles.viewBox}>
            <Icon
              name="solution1"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Acesoramiento personalizado
            </CustomText>
          </View>
          <View style={styles.viewBox}>
            <Icon
              name="closecircleo"
              type="AntDesign"
              size={40}
              style={styles.icons}
            />
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.back_dark}
              dark={colors.white}>
              Sin compromiso, cancela cuando quieras
            </CustomText>
          </View>
        </Swiper>
        <View style={styles.card}>
          <CustomText
            style={[
              stylesText.mainText,
              {
                textAlign: 'center',
                paddingHorizontal: dimensions.Width(4),
                marginTop: 5,
              },
            ]}
            light={colors.back_dark}
            dark={colors.white}>
            Suscripción por un mes
          </CustomText>
          <CustomText
            style={styles.precio}
            light={colors.main}
            dark={colors.main}>
            19,99€ / mes
          </CustomText>
          <CustomText
            style={[
              stylesText.secondaryText,
              {
                textAlign: 'center',
                paddingHorizontal: dimensions.Width(4),
                marginTop: 15,
              },
            ]}
            light={colors.rgb_102}
            dark={colors.rgb_153}>
            Paga 19,99 cada mes
          </CustomText>
        </View>
        <View style={styles.signupButtonContainer}>
          <Button
            light={colors.white}
            dark={colors.white}
            containerStyle={styles.buttonView}
            onPress={() => Navigation.navigate('MyVetecplus', {data: data})}
            title="Suscribirme"
            titleStyle={styles.buttonTitle}
          />
          <WebViweb
            url="https://vetec.es/condiciones"
            visibleModal={visibleModal}
            cerrarweb={cerrarweb}
          />
        </View>
        <View style={{marginBottom: dimensions.Height(10)}}>
          <CustomText
            style={[
              stylesText.secondaryText,
              {
                textAlign: 'center',
                paddingHorizontal: dimensions.Width(4),
                marginTop: 25,
              },
            ]}
            light={colors.rgb_102}
            dark={colors.rgb_153}>
            Las suscripción se renovará automáticamente cada mes y podras
            cancelar en cualquier momento. Si te suscribes aceptas los
          </CustomText>
          <TouchableOpacity onPress={() => abrirweb()}>
            <CustomText
              style={[
                stylesText.mainText,
                {
                  textAlign: 'center',
                  paddingHorizontal: dimensions.Width(4),
                  marginTop: 5,
                },
              ]}
              light={colors.main}
              dark={colors.main}>
              Términos y Condiciones de Vetec
            </CustomText>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: dimensions.Width(100),
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: dimensions.Height(5),
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 20,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(3),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },

  precio: {
    fontSize: 30,
    textAlign: 'center',
    marginTop: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(10),
    fontWeight: 'bold',
  },

  desmas: {
    marginTop: dimensions.Height(1),
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(0),
    marginBottom: dimensions.Height(0),
  },

  desmass: {
    marginTop: dimensions.Height(3),
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(5),
  },

  card: {
    marginTop: dimensions.Height(2),
    borderWidth: 0.5,
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: dimensions.Width(95),
    height: 'auto',
    padding: dimensions.Height(2),
    borderRadius: 10,
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  wrapper: {
    height: 150,
    marginTop: dimensions.Height(5),
    marginBottom: dimensions.Height(5),
  },

  icons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});
