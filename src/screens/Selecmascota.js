import React, {useEffect, useState} from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Modal,
  Alert,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {CustomText} from '../components/CustomText';
import MascotaforPayment from '../components/MascotaforPayment';
import {useNavigationParam} from 'react-navigation-hooks';
import AsyncStorage from '@react-native-community/async-storage';
import Informacion from '../components/informacion';
import AddMascota from './AddMascota';
import {stylesText} from '../components/StyleText';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function SelecionarMascota() {
  const [id, setId] = useState(null);

  const [ModalVisible, setModalVisible] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const openModal = () => {
    setModalVisible(true);
  };

  const openModalMas = () => {
    setShowModal(true);
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  const data = useNavigationParam('data');

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => Navigation.goBack(null)}
            style={{marginLeft: 5}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.title}>
            Selecciona una mascota
          </CustomText>
          <TouchableOpacity
            onPress={() => openModal()}
            style={{marginLeft: 'auto', marginRight: 15}}>
            <Icon
              name="questioncircleo"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalVisible}
          presentationStyle="formSheet">
          <SafeAreaView style={styles.headers1}>
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <View style={{alignItems: 'flex-start', marginLeft: 10}} />
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => setModalVisible(!ModalVisible)}>
                  <CustomText style={{marginRight: 20}}>
                    <Icon
                      type="AntDesign"
                      name="close"
                      size={25}
                      color={colors.green_main}
                    />
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
          <Informacion />
        </Modal>
      </SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{marginTop: dimensions.Height(2)}}>
          <MascotaforPayment id={data.id} />
        </View>
        <Modal
          animationType="slide"
          presentationStyle="formSheet"
          transparent={false}
          visible={showModal}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.containerModal}>
            <SafeAreaView
              style={{
                width: dimensions.ScreenWidth,
                height: dimensions.Height(6),
              }}>
              <TouchableOpacity onPress={() => setShowModal(false)}>
                <View
                  style={{
                    alignSelf: 'center',
                    marginLeft: 'auto',
                    marginRight: dimensions.Width(4),
                    marginTop: dimensions.Height(2),
                  }}>
                  <Icon
                    name="close"
                    type="AntDesign"
                    size={25}
                    color={colors.ERROR}
                  />
                </View>
              </TouchableOpacity>
            </SafeAreaView>
            <AddMascota
              id={id}
              setModalVisible={showModal => setShowModal(showModal)}
            />
          </View>
        </Modal>
        <View style={styles.signupButtonContainer}>
          <TouchableOpacity
            style={styles.cardinfo}
            onPress={() => openModalMas()}>
            <View
              style={{
                alignSelf: 'center',
                marginLeft: dimensions.Width(4),
              }}>
              <Icon
                name="pluscircleo"
                type="AntDesign"
                size={25}
                style={styles.bacs}
              />
            </View>
            <View
              style={{
                alignSelf: 'center',
                marginLeft: dimensions.Width(4),
              }}>
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={stylesText.mainText}>
                Añadir mascota
              </CustomText>
              <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                Añade una mascota para continuar
              </CustomText>
            </View>
            <View
              style={{
                alignSelf: 'center',
                marginLeft: 'auto',
                marginRight: dimensions.Width(4),
              }}>
              <Icon
                name="right"
                type="AntDesign"
                size={20}
                color={colors.rgb_153}
              />
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(11),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        paddingTop: 0,
      },
      {
        paddingTop: 30,
      },
    ),
  },
  headers1: {
    width: dimensions.ScreenWidth,
    ...ifIphoneX(
      {
        height: dimensions.Height(7),
      },
      {
        height: dimensions.Height(9),
      },
    ),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },
  name: {
    fontSize: dimensions.FontSize(18),
    marginTop: dimensions.Height(2.5),
    marginLeft: dimensions.Width(3),
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  cardinfo: {
    width: dimensions.Width(95),
    height: 80,
    borderRadius: 10,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 30,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(1),
    alignSelf: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

export default SelecionarMascota;
