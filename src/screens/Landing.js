import React from 'react';
import {View, ScrollView, Image} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {image} from '../constants/image';
import {CustomText} from '../components/CustomText';
import {Button} from './../components/Button';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';
import {stylesText} from '../components/StyleText';

function Landing({appStart}) {
  appStart(QBConfig);

  setTimeout(() => {
    appStart(QBConfig);
  }, 1000);
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{marginTop: dimensions.Height(5)}}>
          <Image
            source={image.LandingImg}
            style={{width: 300, height: 300, alignSelf: 'center'}}
          />
        </View>

        <View style={{alignSelf: 'center'}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.text}>
            Bienvenido a Vetec
          </CustomText>
        </View>

        <View
          style={{
            alignSelf: 'center',
            marginTop: 10,
            paddingHorizontal: dimensions.Width(4),
          }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, {textAlign: 'center'}]}>
            Los mejores Veterinarios en la palma de tus manos, encuentra
            profesionales de confianza y realiza tus consultas de forma fácil,
            segura y desde tu casa.
          </CustomText>
        </View>

        <View
          style={{
            marginTop: dimensions.Height(5),
            marginBottom: dimensions.Height(8),
          }}>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('Home')}
              title="Explorar"
              titleStyle={styles.buttonTitle}
            />
          </View>

          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('LoginSocial')}
              title="Iniciar sesión"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  text: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
  },
});

export default connect(
  null,
  {appStart},
)(Landing);
