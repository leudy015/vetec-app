import React, {useEffect, useState, useCallback} from 'react';
import {View} from 'react-native';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {
  GiftedChat,
  Send,
  Bubble,
  InputToolbar,
} from 'react-native-gifted-chat';
import { NETWORK_INTERFACE_LINK } from '../constants/config';
import Icon from 'react-native-dynamic-vector-icons';
import 'react-native-get-random-values';
import io from 'socket.io-client';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function Assistant(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [text, setText] = useState({text: ''});
  const [messages, setMessages] = useState([]);


  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  useEffect(() => {
    socket.on('connect', () => {
      socket.emit('online_user_assitant', {
        usuario: props.user
      });
    });

   socket.on('assistant_messages', datas => {
      setMessages(messages => [...datas, ...messages]);
    });
  }, []);

  const onSend = useCallback(newMessages => {
    setMessages(messages => [...newMessages, ...messages]);
    const messagechat = newMessages[0];
    socket.emit('private_message_assistant', {
      messagechat,
      receptor: props.user,
    });
  }, []);

  const renderInputToolbar = props => {
    return <InputToolbar {...props} containerStyle={styles.inputs} />;
  };

  const renderSend = props => {
    return (
      <Send {...props}>
        <View style={{marginRight: 5, marginTop: 5}}>
          <Icon
            type="MaterialCommunityIcons"
            name="send-circle"
            size={40}
            color={colors.main}
            style={styles.Send}
          />
        </View>
      </Send>
    );
  };

  const renderBubble = props => {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: colors.black,
            fontWeight: '200',
          },
          left: {
            color: colors.black,
            fontWeight: '200',
          },
        }}
        wrapperStyle={{
          left: {
            backgroundColor: colors.rgb_235,
          },
          right: {
            backgroundColor: colors.main,
          },
        }}
      />
    );
  };

  return (
    <View style={styles.container}>
      <GiftedChat
        messages={messages}
        placeholder="¿Qué le pasa a tu mascota?"
        onSend={newMessages => onSend(newMessages)}
        onInputTextChanged={text => setText({text})}
        text={text.text}
        isTyping={true}
        renderInputToolbar={renderInputToolbar}
        renderSend={renderSend}
        textInputStyle={{color: colors.rgb_153}}
        renderBubble={renderBubble}
        messageRead={true}
        messageDelivered={true}
        renderUsernameOnMessage={true}
        showUserAvatar={true}
        renderAvatarOnTop={true}
        user={{
          _id: props.user,
          name: props.name,
        }}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  inputs: {
    borderTopWidth: 0.2,
    borderTopColor: colors.rgb_235,
    backgroundColor: 'transparent',
  },
  Send: {
    marginLeft: 'auto',
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    ...ifIphoneX(
      {
        marginTop: 0
      },
      {
        paddingTop: 30,
      },
    ),
    
  },

  title: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  title1: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(12),
    fontWeight: '500',
    color: colors.rgb_153,
  },

  bacs: {
    color: new DynamicValue(colors.main, colors.main),
  },
});


export default Assistant;