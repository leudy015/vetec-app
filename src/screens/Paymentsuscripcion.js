import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Vibration,
  TouchableOpacity,
  FlatList,
  Alert,
  ScrollView,
} from 'react-native';
import Navigation from './../services/NavigationService';
import Headers from '../components/HeaderPlus';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {CustomText} from './../components/CustomText';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import {useNavigationParam} from 'react-navigation-hooks';
import AddCard from './cardsuscription';
import Icon from 'react-native-dynamic-vector-icons';
import {stylesText} from '../components/StyleText';

export default function Consultas() {
  const [cards, setcards] = useState('');

  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const ONE_SECOND_IN_MS = 1000;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  const getCard = async () => {
    let res = await fetch(
      NETWORK_INTERFACE_LINK + `/get-card?customers=${data.StripeID}`,
    );
    const card = await res.json();
    setcards(card);
  };

  const fetchMyAPI = useCallback(async () => {
    if (!data.StripeID) {
      const createclient = async () => {
        let res = await fetch(
          NETWORK_INTERFACE_LINK +
            `/create-client?userID=${data.id}&nameclient=${data.nombre}&email=${
              data.email
            }`,
        );
        const createcliente = await res.json();
        console.log(createcliente);
      };
      createclient();
    } else {
      return null;
    }
  }, []);

  useEffect(() => {
    fetchMyAPI();
    getCard();
  }, []);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.crecadr}
        onPress={() => {
          const pagar = async () => {
            let paga = await fetch(
              NETWORK_INTERFACE_LINK +
                `/create-suscription?customers=${item.customer}&UserID=${
                  data.id
                }`,
            );
            const procederalpago = await paga.json();
            console.log(procederalpago);
            if (procederalpago.success) {
              Navigation.navigate('Thanksuscriptions', {data: data.id});
            } else {
              Vibration.vibrate(PATTERN);
              Alert.alert(
                'Error con tu método de pago',
                'Hubo un error con tu método de pago vuelve a intentarlo por favor',
                [
                  {
                    text: 'Volver a intentarlo',
                    onPress: () => console.log('calcel'),
                  },
                ],

                {cancelable: false},
              );
            }
          };
          pagar();
        }}>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.main}
            style={stylesText.mainText}>
            {data.nombre} {data.apellidos}
          </CustomText>
        </View>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={styles.brand}>
            {item.card.brand}
          </CustomText>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={[stylesText.mainText, {marginTop: 10, marginLeft: 10}]}>
            **** **** {item.card.last4}
          </CustomText>
          <CustomText
            light={colors.back_suave_dark}
            dark={colors.light_white}
            style={[stylesText.mainText, {marginTop: 10, marginLeft: 'auto'}]}>
            {item.card.exp_month} / {item.card.exp_year}
          </CustomText>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} />
      </View>
      <View style={styles.contenedor} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <CustomText
          style={styles.dess}
          light={colors.back_dark}
          dark={colors.white}>
          Confirmar el pago de{' '}
          <CustomText
            style={styles.dess1}
            light={colors.main}
            dark={colors.main}>
            Vetec PLUS
          </CustomText>
        </CustomText>
        <View style={styles.cont}>
          <FlatList
            data={cards && cards.data}
            renderItem={item => _renderItem(item)}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />
          <View style={styles.crecadr}>
            <View style={{flexDirection: 'row', marginBottom: 15}}>
              <Icon
                name="pluscircle"
                type="AntDesign"
                size={20}
                style={{color: colors.rgb_153, marginRight: 10}}
              />
              <CustomText
                light={colors.back_suave_dark}
                dark={colors.main}
                style={[stylesText.mainText, {marginTop: -3}]}>
                Añadir tarjeta
              </CustomText>
            </View>
            <AddCard
              clientID={data.StripeID}
              userID={data.id}
              getCard={getCard}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(25),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: dimensions.Width(100),
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: dimensions.Height(5),
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 20,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(3),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 140,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(25),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },
  cont: {
    justifyContent: 'center',
    alignSelf: 'center',
    paddingHorizontal: dimensions.Width(2),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(20),
  },
  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  crecadr: {
    width: dimensions.Width(90),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    padding: dimensions.Width(5),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    marginHorizontal: dimensions.Width(2),
  },

  brand: {
    fontSize: 18,
    marginTop: 10,
    borderWidth: 1,
    padding: 2,
    borderColor: new DynamicValue(colors.back_suave_dark, colors.light_white),
    borderRadius: 5,
    fontWeight: 'bold',
    width: 45,
    paddingLeft: 6,
  },
});
