import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import Share from 'react-native-share';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {stylesText} from '../components/StyleText';
import WebViweb from '../components/Webview';
import {string} from 'prop-types';

const window = Dimensions.get('window');

export default function DetailsAdopcion() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');
  const [visibleModal, setvisibleModal] = useState(false);

  const abrirweb = () => {
    setvisibleModal(true);
  };

  const cerrarweb = () => {
    setvisibleModal(false);
  };

  const url = 'vetec://vet';
  const title = `${data.nombre} ${data.apellidos}`;
  const message = 'Hola mira esta adorable mascota para adoptar.';
  const icon = 'data:<data_type>/<file_extension>;base64,<base64_data>';
  const options = Platform.select({
    ios: {
      activityItemSources: [
        {
          // For sharing url with custom title.
          placeholderItem: {type: 'url', content: url},
          item: {
            default: {type: 'url', content: url},
          },
          subject: {
            default: title,
          },
          linkMetadata: {originalUrl: url, url, title},
        },
        {
          // For sharing text.
          placeholderItem: {type: 'text', content: message},
          item: {
            default: {type: 'text', content: message},
            message: null, // Specify no text to share via Messages app.
          },
          linkMetadata: {
            // For showing app icon on share preview.
            title: message,
          },
        },
        {
          // For using custom icon instead of default text icon at share preview when sharing with message.
          placeholderItem: {
            type: 'url',
            content: icon,
          },
          item: {
            default: {
              type: 'text',
              content: `${message} ${url}`,
            },
          },
          linkMetadata: {
            title: message,
            icon: icon,
          },
        },
      ],
    },
    default: {
      title,
      subject: title,
      message: `${message} ${url}`,
    },
  });

  const onShare = async () => {
    Share.open(options);
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image style={styles.background} source={{uri: data.avatar}} />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <Icon
                  name="close"
                  type="AntDesign"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.fixedSectionText1}>
              <TouchableOpacity onPress={() => onShare()} style={styles.back}>
                <Icon
                  name="share"
                  type="Feather"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.stickySectionText}>
              {data.name}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.white}
                style={[
                  stylesText.mainText,
                  {maxWidth: dimensions.Width(40), width: 'auto'},
                ]}>
                {data.name}
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginLeft: 'auto',
                  },
                ]}>
                {data.age}
              </CustomText>
            </View>

            <View style={{flexDirection: 'row'}}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText, {marginTop: 5}]}>
                {data.especie}
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {marginTop: 5, marginLeft: 'auto'},
                ]}>
                {data.genero}
              </CustomText>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, {marginTop: 5}]}>
              {data.raza}
            </CustomText>
            <View style={{marginLeft: 'auto'}} />
          </View>

          <View style={{width: '100%', justifyContent: 'flex-start'}}>
            <View
              style={{
                justifyContent: 'flex-start',
                flexDirection: 'row',
                marginTop: 20,
                marginLeft: 15,
              }}>
              <View style={styles.qubo}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={[stylesText.terciaryText, {marginBottom: 5}]}>
                  <Icon
                    name="dog"
                    type="MaterialCommunityIcons"
                    size={20}
                    color={colors.main}
                  />
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    textAlign: 'center',
                  }}>
                  {data.peso} KG
                </CustomText>
              </View>
              <View style={styles.qubo}>
                <CustomText
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    marginBottom: 5,
                  }}>
                  <Icon
                    name="stethoscope"
                    type="MaterialCommunityIcons"
                    size={20}
                    color={colors.main1}
                  />
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={[
                    stylesText.terciaryText,
                    {marginBottom: 5, textAlign: 'center'},
                  ]}>
                  {data.alergias} Alergia
                </CustomText>
              </View>
              <View style={styles.qubo}>
                <CustomText
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '400',
                    marginBottom: 5,
                  }}>
                  <Icon
                    name="scissors"
                    type="Entypo"
                    size={20}
                    color={colors.ERROR}
                  />
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={[
                    stylesText.terciaryText,
                    {marginBottom: 5, textAlign: 'center'},
                  ]}>
                  {data.castrado} Castrado
                </CustomText>
              </View>
            </View>
          </View>

          <View style={{marginTop: dimensions.Height(2)}}>
            <TouchableOpacity style={styles.btncont} onPress={() => abrirweb()}>
              <CustomText
                light={colors.main}
                dark={colors.white}
                style={styles.te}>
                ¡Lo quiero adoptar!
              </CustomText>
            </TouchableOpacity>
            <WebViweb
              url={data.url}
              visibleModal={visibleModal}
              cerrarweb={cerrarweb}
            />
          </View>

          <View
            style={{
              marginTop: dimensions.Height(3),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={stylesText.mainText}>
              Descripción
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.secondaryText, {marginTop: 20}]}>
              {data.descripcion}
            </CustomText>
          </View>
          <View
            style={{
              marginTop: dimensions.Height(1),
              flex: 1,
              marginBottom: dimensions.Height(45),
              flexDirection: 'row',
            }}
          />
        </View>
      </ParallaxScrollView>
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },

  btncont: {
    width: dimensions.Width(91),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.main),
    borderWidth: 1,
    borderColor: colors.main,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 100,
      },
      {
        left: 75,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
    flexDirection: 'row',
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },
  prof: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 10,
  },

  cardop: {
    width: dimensions.Width(92),
    height: 90,
    paddingHorizontal: dimensions.Width(4),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  rating: {
    alignSelf: 'center',
    fontSize: dimensions.FontSize(50),
    fontWeight: 'bold',
  },

  textw: {
    fontSize: dimensions.FontSize(16),
    marginTop: 10,
  },

  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(12),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(3),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
  },

  back1: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        padding: 12,
      },
      {
        padding: 6,
      },
    ),
    borderRadius: 10,
    marginLeft: 'auto',
  },

  fotos: {
    width: 80,
    height: 80,
  },
});
