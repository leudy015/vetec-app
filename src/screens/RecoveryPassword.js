import React, {useState} from 'react';
import {View, Alert, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {colors, dimensions} from './../themes';
import {OutlinedTextField} from 'react-native-material-textfield';
import {CustomText} from './../components/CustomText';
import {Button} from './../components/Button';
import {withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Headers from '../components/Header';
import Toast from 'react-native-simple-toast';
import {LineDotsLoader} from 'react-native-indicator';
import {stylesText} from '../components/StyleText';
import {validateEmail} from '../constants/emailValidator';

function Login() {
  const [email, setEmail] = useState('');
  const [Loading, setLoading] = useState(false);

  const handleSubmit = () => {
    setLoading(true);
    if (!validateEmail(email)) {
      Alert.alert(
        'Email invalido',
        'Introduce un email valido para continuar',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      setLoading(false);
    } else {
      const emails = email;
      if (!emails) {
        Alert.alert(
          'Ohhh algo va mal ',
          'por favor escribe tu email para continuar',
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
        setLoading(false);
        return null;
      }
      if (emails) {
        fetch(`${NETWORK_INTERFACE_LINK}/forgotpassword?email=${emails}`)
          .then(res => {
            console.log(res);
            if (res.ok) {
              setLoading(false);
              Toast.showWithGravity(
                'Email enviado con éxito',
                Toast.LONG,
                Toast.TOP,
              );
              Navigation.navigate('Login');
            } else {
              setLoading(false);
              Alert.alert('Error con tu Email', 'Aún no tenemos este email', [
                {
                  text: 'Regístrarme',
                  onPress: () => Navigation.navigate('Register'),
                  style: 'cancel',
                },
                {
                  text: 'Volver a intentarlo',
                  onPress: () => console.log('Volver a intentar'),
                },
              ]);
            }
          })
          .catch(err => console.log('err:', err));
      }
    }
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Recuperar contraseña"
          back={true}
          rigth={true}
        />
      </View>
      <View style={styles.contenedor}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
          style={{marginBottom: dimensions.Height(10)}}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              marginLeft: dimensions.Width(8),
              marginBottom: dimensions.Height(5),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              ¿Olvidaste tu
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              contraseña?
            </CustomText>
          </View>

          <View
            style={{
              marginLeft: dimensions.Width(8),
              marginRight: dimensions.Width(4),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={stylesText.secondaryText}>
              Ingresa el email asociado a tu cuenta y te enviaremos un enlace
              para restablecer tu contraseña.
            </CustomText>
          </View>

          <View style={styles.formView}>
            <OutlinedTextField
              label="Email"
              placeholder="Email"
              keyboardType="email-address"
              inputContainerStyle={{color: colors.rgb_153}}
              textColor={colors.rgb_153}
              labelTextStyle={{color: colors.rgb_153}}
              placeholderTextColor={colors.rgb_153}
              onChangeText={values => setEmail(values)}
            />

            <View style={styles.signupButtonContainer}>
              {Loading ? (
                <LineDotsLoader color={colors.main} />
              ) : (
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => handleSubmit()}
                  title="Enviar enlace"
                  titleStyle={styles.buttonTitle}
                />
              )}
            </View>

            <View
              style={{alignSelf: 'center', marginTop: dimensions.Height(5)}}>
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={stylesText.secondaryText}>
                ¿Aún no tienes una cuenta?
              </CustomText>
            </View>

            <TouchableOpacity
              onPress={() => Navigation.navigate('Register')}
              style={{
                alignSelf: 'center',
                marginTop: dimensions.Height(5),
                marginBottom: dimensions.Height(10),
              }}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={stylesText.secondaryText}>
                Regístrarme
              </CustomText>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.Height(100),
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(15),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row',
  },
  rememberText: {
    fontSize: dimensions.FontSize(18),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  h1: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'left',
  },
});

export default connect(
  null,
  {},
)(withApollo(Login));
