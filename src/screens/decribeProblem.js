import React, {useEffect, useState} from 'react';
import {
  View,
  SafeAreaView,
  Alert,
  ScrollView,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {CustomText} from '../components/CustomText';
import {useNavigationParam} from 'react-navigation-hooks';
import {Button} from './../components/Button';
import AsyncStorage from '@react-native-community/async-storage';
import {useMutation} from 'react-apollo';
import {CREAR_MODIFICAR_CONSULTA} from '../mutations';
import {TextareaItem} from '@ant-design/react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {withApollo} from 'react-apollo';
import Informacion from '../components/informacion';
import {LineDotsLoader} from 'react-native-indicator';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function Nota() {
  const [id, setId] = useState(null);
  const [val, setVal] = useState('');
  const [ModalVisible, setModalVisible] = useState(false);
  const [Loading, setLoading] = useState(false);

  const [crearModificarConsulta] = useMutation(CREAR_MODIFICAR_CONSULTA);

  const openModal = () => {
    setModalVisible(true);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const onChange = val => {
    setVal(val);
  };

  const crearConsultaModificar = () => {
    setLoading(true);
    const input = {
      profesional: data.prof,
      usuario: id,
      mascota: data.masc,
      nota: val,
    };

    if (!val) {
      Alert.alert(
        'Upps debes describir el problema de tu mascota',
        'Para continuar con la consulta debes describir el peoblema de tu mascota',
        [
          {
            text: 'OK',
            onPress: () => console.log('ok'),
          },
        ],
        {cancelable: false},
      );
      setLoading(false);
      return null;
    }
    crearModificarConsulta({variables: {input}})
      .then(res => {
        if (res.data.crearModificarConsulta.success) {
          Navigation.navigate('ConsultasScreen', {
            data: res.data.crearModificarConsulta.data,
          });
          setLoading(false);
        } else {
          Alert.alert(
            'Algo va mal',
            'Intentalo de nuevo por favor',
            [
              {
                text: 'OK',
                onPress: () => console.log('ok'),
              },
            ],
            {cancelable: false},
          );
          setLoading(false);
        }
      })
      .catch(er => {
        console.log('Algo va mal lo sentimos', er);
        setLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => Navigation.goBack(null)}
            style={{marginLeft: 5}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.title}>
            ¿Qué le pasa a tu mascota?
          </CustomText>
          <TouchableOpacity
            onPress={() => openModal()}
            style={{marginLeft: 'auto', marginRight: 15}}>
            <Icon
              name="questioncircleo"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalVisible}
          presentationStyle="formSheet">
          <SafeAreaView style={styles.headers1}>
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <View style={{alignItems: 'flex-start', marginLeft: 10}} />
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => setModalVisible(!ModalVisible)}>
                  <CustomText style={{marginRight: 20}}>
                    <Icon
                      type="AntDesign"
                      name="close"
                      size={25}
                      color={colors.green_main}
                    />
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
          <Informacion />
        </Modal>
      </SafeAreaView>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{marginTop: dimensions.Height(2)}}>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps="always"
            style={{paddingHorizontal: dimensions.Width(4)}}>
            <View style={{marginTop: dimensions.Height(5)}}>
              <TextareaItem
                rows={5}
                placeholder="¿Qué le pasa a tu mascota?"
                count={240}
                value={val}
                onChange={onChange}
                style={{
                  backgroundColor: 'transparent',
                  color: colors.rgb_153,
                }}
                placeholderTextColor={colors.rgb_153}
              />
            </View>
          </KeyboardAwareScrollView>
        </View>

        {Loading ? (
          <View style={{marginTop: 20}}>
            <LineDotsLoader color={colors.main} />
          </View>
        ) : (
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => crearConsultaModificar()}
              title="Continuar"
              titleStyle={styles.buttonTitle}
            />
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(11),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        paddingTop: 0,
      },
      {
        paddingTop: 30,
      },
    ),
  },

  headers1: {
    width: dimensions.ScreenWidth,
    ...ifIphoneX(
      {
        height: dimensions.Height(7),
      },
      {
        height: dimensions.Height(9),
      },
    ),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },
  name: {
    fontSize: dimensions.FontSize(18),
    marginTop: dimensions.Height(2.5),
    marginLeft: dimensions.Width(3),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(1),
    alignSelf: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

export default withApollo(Nota);
