import React from 'react';
import {View, ScrollView, Alert} from 'react-native';
import {connect} from 'react-redux';
import {colors, dimensions} from './../themes';
import {CustomText} from './../components/CustomText';
import {Button} from './../components/Button';
import AsyncStorage from '@react-native-community/async-storage';
import {withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import {AUTENTICAR_USUARIO} from './../mutations';
import {setUser} from './../actionCreators/user';
import {loginRequest, usersCreate, usersUpdate} from './../actionCreators';
import Headers from '../components/Header';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import TwitterButton from './../services/Twitter';
import {GoogleSignin} from '@react-native-community/google-signin';
import {axiosPost} from '../services/networkRequestService';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import {useMutation} from '@apollo/react-hooks';
GoogleSignin.configure({
  iosClientId:
    '54110211332-7bd3o68k3gbu2vdfkijg0plum30dljj8.apps.googleusercontent.com',
});
import {showError} from '../services/FlashMessageService';
import {SignInWithAppleButton} from 'react-native-apple-authentication';

function LoginSocial({setUser, createUser, updateUser, signIn}) {
  const [autenticarUsuario] = useMutation(AUTENTICAR_USUARIO);

  const submit = (login, username) => {
    new Promise((resolve, reject) => {
      signIn({login, resolve, reject});
    })
      .then(action => {
        checkIfUsernameMatch(username, action.payload.user);
      })
      .catch(action => {
        const {error} = action;
        if (error.toLowerCase().indexOf('unauthorized') > -1) {
          new Promise((resolve, reject) => {
            createUser({
              fullName: username,
              login,
              password: 'quickblox',
              resolve,
              reject,
            });
          })
            .then(() => {
              this.submit(login, username);
              console.log('user create succefull');
            })
            .catch(userCreateAction => {
              const {error} = userCreateAction;
              if (error) {
                showError('Failed to create user account', error);
              }
            });
        } else {
          showError('Failed to sign in', error);
        }
      });
  };

  const checkIfUsernameMatch = (username, user) => {
    const {updateUser} = this.props;
    const update =
      user.fullName !== username
        ? new Promise((resolve, reject) =>
            updateUser({
              fullName: username,
              login: user.login,
              resolve,
              reject,
            }),
          )
        : Promise.resolve();
    update.then(this.connectAndRedirect).catch(action => {
      if (action && action.error) {
        showError('Failed to update user', action.error);
      }
    });
  };

  const googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      let firstName = userInfo.user.givenName;
      let lastName = userInfo.user.familyName;
      let email = userInfo.user.email;
      let token = userInfo.user.id;
      socialLogin({firstName, lastName, email, token});
    } catch (error) {
      console.log(error);
    }
  };

  const facebookLogin = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(
            'Login success with permissions: ' +
              result.grantedPermissions.toString(),
          );

          const responseInfoCallback = (error, result) => {
            if (error) {
              console.log(error);
              Alert.alert('Facebook request failed. Try Again.');
            } else {
              //alert(JSON.stringify(result))
              getFacebookUserInfo(result);
            }
          };

          const infoRequest = new GraphRequest(
            '/me?fields=email,name,picture.height(480)',
            null,
            responseInfoCallback,
          );
          // Start the graph request.
          console.log('start  graph request');
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      function(error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  const appleSignIn = result => {
    if (!result) {
      console.log('el usuario cancelo el proceso');
    } else {
      let email = result.email ? result.email : result.user;
      let token = result.user;
      console.log(email, token);
      socialLogin({email, token});
      console.log('Resssult', result);
    }
  };

  const getFacebookUserInfo = result => {
    let name = result.name.split(' ');
    let firstName = name[0];
    let lastName = name[1] || '';
    let email = result.email;
    let token = result.id;
    console.log(result);
    socialLogin({firstName, lastName, email, token});
  };

  const socialLogin = postData => {
    axiosPost(`${NETWORK_INTERFACE_LINK}/api/v1/auth/social/mobile`, postData)
      .then(res => {
        let email = res.data.nuevoUsuario.email;
        let password = res.data.token;

        autenticarUsuario({variables: {email, password}})
          .then(async results => {
            const das =
              results && results.data && results.data.autenticarUsuario
                ? results.data.autenticarUsuario.data
                : '';
            await AsyncStorage.setItem('token', das.token);
            await AsyncStorage.setItem('id', das.id);

            setUser(das);

            let new_email = email;

            console.log('new_email', new_email, email);

            submit(new_email, new_email);

            let very = {
              id: das.id,
              fromSocial: true,
            };

            if (das.verifyPhone) {
              Navigation.navigate('Home');
            } else {
              Navigation.navigate('VerifyPhone', {data: very});
            }
          })
          .catch(error => {
            alert(`error : ${error}`);
          });
      })
      .catch(err => {
        alert(JSON.stringify(err));
      });
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Iniciar sesión"
          back={true}
          rigth={true}
        />
      </View>
      <View style={styles.contenedor}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{marginBottom: dimensions.Height(10)}}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              marginLeft: dimensions.Width(8),
              marginBottom: dimensions.Height(5),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              Bienvenido
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              a Vetec !
            </CustomText>
          </View>
          <View style={styles.formView}>
            <View style={{marginVertical: 10}}>
              <Icon.Button
                name="facebook"
                backgroundColor="#3b5998"
                onPress={() => facebookLogin()}>
                <CustomText
                  style={{
                    fontFamily: 'Arial',
                    fontSize: 22,
                    paddingVertical: 10,
                    color: colors.white,
                  }}>
                  Continúa con Facebook
                </CustomText>
              </Icon.Button>
            </View>
            <View style={{marginVertical: 10}}>
              <TwitterButton onSuccess={data => socialLogin(data)} />
            </View>
            <View style={{marginVertical: 10}}>
              <Icon.Button
                name="google"
                backgroundColor={colors.ERROR}
                onPress={() => googleSignIn()}>
                <CustomText
                  style={{
                    fontFamily: 'Arial',
                    fontSize: 22,
                    paddingVertical: 10,
                    color: colors.white,
                  }}>
                  Continúa con Google
                </CustomText>
              </Icon.Button>
            </View>

            <View style={{marginVertical: 10}}>
              {SignInWithAppleButton(styles.appleBtn, appleSignIn)}
            </View>

            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => Navigation.navigate('Login')}
                title="Continúa con tu Email"
                titleStyle={styles.buttonTitle}
              />
            </View>
            <View />
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(20),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row',
  },
  rememberText: {
    fontSize: dimensions.FontSize(18),
  },

  appleBtn: {
    height: 60,
    width: dimensions.Width(84),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(2),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  h1: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'left',
  },
});

const mapDispatchToProps = {
  setUser: setUser,
  signIn: loginRequest,
  createUser: usersCreate,
  updateUser: usersUpdate,
};

export default connect(
  null,
  mapDispatchToProps,
)(withApollo(LoginSocial));
