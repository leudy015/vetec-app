import React, {Component} from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {ApolloProvider} from 'react-apollo';
import {ApolloClient} from 'apollo-client';
import {createHttpLink} from 'apollo-link-http';
import {setContext} from 'apollo-link-context';
import {InMemoryCache} from 'apollo-cache-inmemory';
import NavigationService from './services/NavigationService';
import {AuthContainer, UnAuthContainer} from './navigation';
import {NETWORK_INTERFACE, NETWORK_INTERFACE_LINK} from './constants/config';
import {DarkModeProvider} from 'react-native-dark-mode';
import NetInfo from '@react-native-community/netinfo';
import * as StoreReview from 'react-native-store-review';
import OneSignal from 'react-native-onesignal';
import Nointernet from './../noInternet';
import configureStore from './store';
import rootSaga from './sagas';
import QuickActions from 'react-native-quick-actions';
import {StatusBar, Platform} from 'react-native';
import {useScreens} from 'react-native-screens';
import {Freshchat, FreshchatConfig} from 'react-native-freshchat-sdk';

var freshchatConfig = new FreshchatConfig(
  '10b50a03-c65f-415d-9bb5-9d2fabd4e036',
  '23fd5dc4-eeef-4c3c-ab9e-66d6aaa813e6',
);
freshchatConfig.domain = 'msdk.eu.freshchat.com';
freshchatConfig.teamMemberInfoVisible = true;
freshchatConfig.cameraCaptureEnabled = true;
freshchatConfig.gallerySelectionEnabled = true;
freshchatConfig.responseExpectationEnabled = true;
freshchatConfig.showNotificationBanner = true; //iOS only
freshchatConfig.notificationSoundEnabled = true; //iOS only
freshchatConfig.themeName = 'CustomTheme.plist'; //iOS only
freshchatConfig.stringsBundle = 'FCCustomLocalizable'; //iOS only

Freshchat.init(freshchatConfig);

if (Platform.OS === 'ios') {
  QuickActions.setShortcutItems([
    {
      type: 'Orders', // Required
      title: 'Añadir mascota', // Optional, if empty, `type` will be used instead
      subtitle: 'Añade una mascota nueva',
      icon: 'Add', // Icons instructions below
    },

    {
      type: 'Orders', // Required
      title: 'Consultas', // Optional, if empty, `type` will be used instead
      subtitle: 'Revisa tus consultas',
      icon: 'Date', // Icons instructions below
    },

    {
      type: 'Orders', // Required
      title: 'Chats', // Optional, if empty, `type` will be used instead
      subtitle: 'Conversaciones con tu vets',
      icon: 'Message', // Icons instructions below
    },

    {
      type: 'Orders', // Required
      title: 'Favoritos', // Optional, if empty, `type` will be used instead
      subtitle: 'Mira los vet en favoritos',
      icon: 'Love', // Icons instructions below
    },
  ]);
}

const {runSaga, store, persistor} = configureStore();
runSaga(rootSaga);

// Subscribe
const unsubscribe = NetInfo.addEventListener(state => {
  console.log('Connection type', state.type);
  console.log('Is connected?', state.isConnected);
});
// Unsubscribe
unsubscribe();

const httpLink = createHttpLink({
  uri: NETWORK_INTERFACE,
});

const authLink = setContext(async (_, {headers}) => {
  // get the authentication token from local storage if it exists
  const token = await AsyncStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? token : '',
    },
  };
});

const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: 'fetching',
      device: '',
      Isconected: false,
    };

    OneSignal.init('e461eb6c-b20a-4a47-8923-ff44868ed8b2', {
      kOSSettingsKeyAutoPrompt: true,
    });
    OneSignal.inFocusDisplaying(0);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }
  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened = async () => {
    const id = await AsyncStorage.getItem('id');
    NavigationService.navigate('Chats', {
      data: id,
    });
  };

  componentWillUnmount() {
    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    }
  }

  onIds = async device => {
    const id = await AsyncStorage.getItem('id');
    const Userid = device && device.userId;
    if (Userid && id) {
      fetch(
        `${NETWORK_INTERFACE_LINK}/save-userid-notification?UserId=${Userid}&id=${id}`,
      ).catch(err => console.log(err));
    } else {
      return null;
    }
  };

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('token');
      console.log(value);
      if (value !== null) {
        this.setState({token: value});
        global.token = value;
      } else {
        this.setState({token: false});
      }
    } catch (e) {
      // error reading value
      this.setState({token: false});
    }
    this.onIds();
    NetInfo.fetch().then(state => {
      this.setState({
        Isconected: state.isConnected,
      });
    });
  }

  render() {
    if (this.state.token === 'fetching') {
      return null;
    }
    if (!this.state.Isconected) {
      return <Nointernet />;
    } else
      return (
        <DarkModeProvider>
          <Provider store={store}>
            <PersistGate loading={true} persistor={persistor}>
              <StatusBar translucent backgroundColor="transparent" />
              <ApolloProvider client={apolloClient}>
                {this.state.token === false ? (
                  <UnAuthContainer
                    ref={navigatorRef => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                ) : (
                  <AuthContainer
                    ref={navigatorRef => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                )}
              </ApolloProvider>
            </PersistGate>
          </Provider>
        </DarkModeProvider>
      );
  }
}

///////////cd android && ./gradlew clean && ./gradlew bundleRelease
