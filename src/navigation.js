import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

/////////Screens//////////////

import Home from './screens/Home';
import Landing from './screens/Landing';
import Login from './screens/Login';
import LoginSocial from './screens/LoginSocial';
import Register from './screens/Register';
import Recovery from './screens/RecoveryPassword';
import Search from './screens/Search';
import Maps from './screens/Maps';
import Chats from './screens/Chats';
import Profile from './screens/Profile';
import DetailsPro from './screens/DetailsProfesional';
import UpdateProfile from './screens/UpdateProfile';
import DetailsMascota from './screens/DetailsMascora';
import Consultas from './screens/Consultas';
import ConsultasDetails from './screens/DetailsConsultas';
import ConsultasScreen from './screens/ConsultaScreen';
import SelecMascota from './screens/Selecmascota';
import PaymentPage from './screens/Paymentpage';
import Notifications from './screens/Notification';
import Cupon from './screens/Cupon';
import ChatsScreens from './screens/ChatsScreens';
import Configuraciones from './screens/Configuraciones';
import Nota from './screens/decribeProblem';
import CategoriaPro from './screens/ProCategoria';
import MayFavorites from './screens/Profavorite';
import MyPayment from './screens/PaymentsMetod';
import Thank from './screens/thank';
import Error from './screens/eRror';
import Vetecplus from './screens/vetecplus';
import MyVetecplus from './screens/SelecmyVet';
import PaymentVet from './screens/Paymentsuscripcion';
import Thanksuscriptions from './screens/thanksuscripcion';
import MyareaPlus from './screens/Myareaplus';
import CallScreen from './screens/CallScreen';
import CheckConnection from './screens/CheckConnection';
import Adoptar from './screens/Adopcion';
import DetailsAdoptar from './screens/DetailsAdopcion';
import VerifyPhone from './screens/VerifyPhone';
import Entrecode from './screens/EnterCode';
///////////end//////////////

const AuthAppNavigator = createStackNavigator(
  {
    Home,
    Search,
    Adoptar,
    DetailsAdoptar,
    Maps,
    CheckConnection,
    VerifyPhone,
    Entrecode,
    Chats,
    CallScreen,
    PaymentVet,
    MyPayment,
    Thanksuscriptions,
    MyareaPlus,
    DetailsPro,
    UpdateProfile,
    Profile,
    Vetecplus,
    MyVetecplus,
    DetailsMascota,
    Consultas,
    ConsultasDetails,
    ConsultasScreen,
    SelecMascota,
    PaymentPage,
    Notifications,
    Cupon,
    ChatsScreens,
    Configuraciones,
    CategoriaPro,
    MayFavorites,
    Error,
    Thank,
    Nota,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.ModalTransition,
    },
  },

  {
    initialRouteName: Home,
  },
);

const UnAuthAppNavigator = createStackNavigator(
  {
    Landing,
    Home,
    Adoptar,
    VerifyPhone,
    Entrecode,
    DetailsAdoptar,
    CallScreen,
    CheckConnection,
    Search,
    Login,
    LoginSocial,
    Register,
    Recovery,
    Maps,
    DetailsPro,
    CategoriaPro,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.ModalTransition,
    },
  },

  {
    initialRouteName: Landing,
  },
);

export const UnAuthSwitch = createSwitchNavigator({
  UnAuth: UnAuthAppNavigator,
  Auth: AuthAppNavigator,
});

export const UnAuthContainer = createAppContainer(UnAuthSwitch);

export const AuthSwitch = createSwitchNavigator({
  Auth: AuthAppNavigator,
  UnAuth: UnAuthAppNavigator,
});

export const AuthContainer = createAppContainer(AuthSwitch);
