import React, {useState} from 'react';
import {View, FlatList, TouchableOpacity, Image} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from './CustomText';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import moment from 'moment';
import 'moment/locale/es';
import NoData from './NoData';
import {stylesText} from '../components/StyleText';
import {image} from './../constants/image';
import Star from '../components/star';

export const Opiniones = props => {
  const [line, setLine] = useState(3);
  const styles = useDynamicStyleSheet(dynamicStyles);

  console.log('data rating', props.data);

  const _renderItem = ({item}) => {
    return (
      <View style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.avatar}>
            {item.customer.avatar ? (
              <Image
                source={{
                  uri: NETWORK_INTERFACE_LINK_AVATAR + item.customer.avatar,
                }}
                style={styles.avatar}
              />
            ) : (
              <Image source={image.Avatar} style={styles.avatar} />
            )}
          </View>
          <View style={{marginLeft: dimensions.Width(4)}}>
            <CustomText
              numberOfLines={1}
              light={colors.rgb_102}
              dark={colors.light_white}
              style={stylesText.mainText}>
              {item.customer.nombre} {item.customer.apellidos}
            </CustomText>
            <View style={{flexDirection: 'row', width: 230}}>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {color: colors.main, marginTop: 7},
                ]}>
                {moment(item.updated_at).fromNow()}
              </CustomText>
              <Star
                star={item.rate}
                styles={{marginLeft: 'auto', marginTop: 10}}
              />
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: 10,
                    left: 8,
                    color: colors.rgb_153,
                  },
                ]}>
                ({item.rate})
              </CustomText>
            </View>
            <CustomText
              style={[
                stylesText.secondaryText,
                {color: colors.rgb_153, marginTop: 5},
              ]}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.customer.ciudad ? item.customer.ciudad : 'España'}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            paddingHorizontal: dimensions.Width(2),
          }}>
          <CustomText
            numberOfLines={line}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={[stylesText.secondaryText, {marginTop: 15, width: '100%'}]}>
            {item.coment}
          </CustomText>
          {line === 3 ? (
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => setLine(100)}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText]}>
                Ver más
              </CustomText>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => setLine(3)}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[stylesText.secondaryText]}>
                Ver menos
              </CustomText>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };
  return (
    <FlatList
      data={props.data}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      ListEmptyComponent={
        <NoData menssge="Este profesional no tiene valoraciones" />
      }
    />
  );
};

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: 370,
    height: 'auto',
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    borderRadius: 10,
    shadowColor: new DynamicValue(colors.rgb_153, colors.black),
    shadowOffset: {width: 0, height: 3},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },
});
