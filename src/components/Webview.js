import React, {useState} from 'react';
import {View, SafeAreaView, TouchableOpacity, Modal} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../components/CustomText';
import {WebView} from 'react-native-webview';
import {LineDotsLoader} from 'react-native-indicator';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function CustoWebview(props) {
  const [setShowModal] = useState(false);

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <Modal
      animationType="slide"
      presentationStyle="formSheet"
      visible={props.visibleModal}
      onRequestClose={() => setShowModal(true)}>
      <SafeAreaView style={styles.headers1}>
        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View style={{alignItems: 'flex-start', marginLeft: 10}} />
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => props.cerrarweb()}>
              <CustomText style={{marginRight: 20}}>
                <Icon
                  type="AntDesign"
                  name="close"
                  size={25}
                  color={colors.green_main}
                />
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
      <WebView
        useWebKit={true}
        source={{
          uri: props.url,
        }}
        startInLoadingState={true}
        renderLoading={() => <LineDotsLoader />}
      />
    </Modal>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  headers1: {
    width: dimensions.ScreenWidth,
    ...ifIphoneX(
      {
        height: dimensions.Height(7),
      },
      {
        height: dimensions.Height(9),
      },
    ),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
});

export default CustoWebview;
