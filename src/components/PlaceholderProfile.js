import React from 'react';
import {SafeAreaView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dark-mode';
import {colors, dimensions} from './../themes';

export default function Loading() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <SafeAreaView style={{alignSelf: 'center'}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(53),
            height: 40,
            borderRadius: 5,
            marginTop: 35,
          }}
        />
        <View style={{marginTop: 40}}>
          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />

          <View
            style={{
              width: dimensions.Width(93),
              height: 40,
              borderRadius: 5,
              marginTop: 35,
            }}
          />
        </View>
      </SkeletonPlaceholder>
    </SafeAreaView>
  );
}
