import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Alert,
  Vibration,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';
import {Query} from 'react-apollo';
import {GET_PROFESIONAL_CITY} from '../query/index';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import AsyncStorage from '@react-native-community/async-storage';
import {Mutation} from 'react-apollo';
import {CREAR_USUARIO_FAVORITE, ELIMINAR_USUARIO_FAVORITE} from '../mutations';
import Toast from 'react-native-simple-toast';
import {LineDotsLoader} from 'react-native-indicator';

const ProfesionalMaps = props => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const ONE_SECOND_IN_MS = 1000;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  const styles = useDynamicStyleSheet(dynamicStyles);

  const _renderItem = ({item}, refetch) => {
    refetch();

    const isNavigation = () => {
      if (id) {
        Navigation.navigate('SelecMascota', {data: item});
      } else {
        Alert.alert(
          'Para continuar debes iniciar sesión',
          'Debes iniciar sesión para continuar con la contratación',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
      }
    };

    const anadirFavorito = (
      crearUsuarioFavoritoPro,
      profesionalId,
      usuarioId,
    ) => {
      if (!id) {
        Alert.alert(
          'Upps debes iniciar sesión',
          'Para añadir este profesional a favorito debes iniciar sesión',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
        return null;
      } else {
        crearUsuarioFavoritoPro({
          variables: {profesionalId, usuarioId},
        }).then(async ({data: res}) => {
          if (
            res &&
            res.crearUsuarioFavoritoPro &&
            res.crearUsuarioFavoritoPro.success
          ) {
            Toast.showWithGravity(
              'Profesional añadido con éxito a al lista de deseos',
              Toast.LONG,
              Toast.TOP,
            );
            console.log('Producto añadido con éxito');
            refetch();
            Vibration.vibrate(PATTERN);
          } else if (
            res &&
            res.crearUsuarioFavoritoPro &&
            !res.crearUsuarioFavoritoPro.success
          )
            return null;
        });
      }
    };

    let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
    item.professionalRatinglist.forEach(start => {
      if (start.rate == 1) rating['1'] += 1;
      else if (start.rate == 2) rating['2'] += 1;
      else if (start.rate == 3) rating['3'] += 1;
      else if (start.rate == 4) rating['4'] += 1;
      else if (start.rate == 5) rating['5'] += 1;
    });

    const ar =
      (5 * rating['5'] +
        4 * rating['4'] +
        3 * rating['3'] +
        2 * rating['2'] +
        1 * rating['1']) /
      item.professionalRatinglist.length;
    let averageRating = 0;
    if (item.professionalRatinglist.length) {
      averageRating = ar.toFixed(1);
    }
    return (
      <TouchableOpacity
        onPress={() => Navigation.navigate('DetailsPro', {data: item})}
        style={styles.card}>
        <View style={{paddingHorizontal: dimensions.Width(2)}}>
          <View style={{marginTop: dimensions.Height(0)}}>
            <Image
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar}}
              style={styles.avatar}
            />
          </View>
          <View style={{marginTop: dimensions.Width(2)}}>
            <View style={{flexDirection: 'row', width: dimensions.Height(15)}}>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={styles.title}>
                {item.nombre} {item.apellidos}{' '}
              </CustomText>
              {item.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={12}
                  style={{
                    alignSelf: 'center',
                    marginTop: 3,
                    paddingLeft: 5,
                    color: colors.main,
                  }}
                />
              ) : null}
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomText style={[styles.text, {color: colors.main}]}>
                {item.profesion}
              </CustomText>
              <CustomText style={[styles.text, {marginLeft: 'auto'}]}>
                <Icon
                  name="star"
                  type="AntDesign"
                  size={14}
                  color={colors.orange}
                />{' '}
                ({averageRating})
              </CustomText>
            </View>
            <CustomText style={styles.text}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.ciudad}
            </CustomText>
            <View style={{marginLeft: 'auto'}}>
              {item.anadidoFavorito ? (
                <Mutation
                  mutation={ELIMINAR_USUARIO_FAVORITE}
                  variables={{id: item.id}}>
                  {eliminarUsuarioFavoritoPro => {
                    return (
                      <TouchableOpacity
                        onPress={() => {
                          eliminarUsuarioFavoritoPro({
                            variables: {id: item.id},
                          }).then(res => {
                            Toast.showWithGravity(
                              'Profesional eliminado con éxito de la lista de deseos',
                              Toast.LONG,
                              Toast.TOP,
                            );
                            refetch();
                            Vibration.vibrate(PATTERN);
                            if (
                              res &&
                              res.data &&
                              res.data.eliminarUsuarioFavoritoPro
                                ? res.data.eliminarUsuarioFavoritoPro.success
                                : ''
                            ) {
                              console.log(
                                res &&
                                  res.data &&
                                  res.data.eliminarUsuarioFavoritoPro
                                  ? res.data.eliminarUsuarioFavoritoPro.message
                                  : '',
                              );
                            } else if (
                              res &&
                              res.data &&
                              res.data.eliminarUsuarioFavoritoPro
                                ? !res.data.eliminarUsuarioFavoritoPro.success
                                : ''
                            )
                              console.log(
                                res &&
                                  res.data &&
                                  res.data.eliminarUsuarioFavoritoPro
                                  ? !res.data.eliminarUsuarioFavoritoPro.message
                                  : '',
                              );
                          });
                        }}>
                        <Icon
                          name="heart"
                          type="AntDesign"
                          size={16}
                          style={{
                            alignSelf: 'center',
                            marginTop: -3,
                            marginRight: 15,
                            color: colors.ERROR,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  }}
                </Mutation>
              ) : (
                <Mutation mutation={CREAR_USUARIO_FAVORITE}>
                  {crearUsuarioFavoritoPro => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          anadirFavorito(crearUsuarioFavoritoPro, item.id, id)
                        }>
                        <Icon
                          name="hearto"
                          type="AntDesign"
                          size={16}
                          style={{
                            alignSelf: 'center',
                            marginTop: -3,
                            marginRight: 15,
                            color: colors.rgb_153,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  }}
                </Mutation>
              )}
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <TouchableOpacity>
            <CustomText style={styles.textt}>
              {item.Precio}€ /Consulta
            </CustomText>
          </TouchableOpacity>
          <View style={{marginLeft: 'auto', marginTop: dimensions.Height(1)}}>
            <View style={{marginTop: dimensions.Height(5)}}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={isNavigation}
                title="Consultar"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={GET_PROFESIONAL_CITY} variables={{city: props.ciudad}}>
      {response => {
        if (response.loading) {
          return <LineDotsLoader color={colors.main} />;
        }
        if (response.error) {
          return <LineDotsLoader color={colors.main} />;
        }
        if (response) {
          return (
            <FlatList
              data={
                response && response.data && response.data.getProfesionalCity
                  ? response.data.getProfesionalCity.data
                  : ''
              }
              renderItem={item => _renderItem(item, response.refetch)}
              keyExtractor={item => item.id}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          );
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(40),
    height: 260,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(5),
    paddingHorizontal: dimensions.Width(2),
    borderRadius: 15,
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  text: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1),
  },

  textt: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1.5),
  },
  title: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(30),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
});

export default ProfesionalMaps;
