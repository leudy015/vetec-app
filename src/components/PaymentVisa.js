import React from 'react';
import {Image} from 'react-native';
import {image} from './../constants/image';
import {DynamicValue, useDynamicValue} from 'react-native-dark-mode';
const lightLogo = image.Tarjeta;
const darkLogo = image.Visa;

const logoUri = new DynamicValue(lightLogo, darkLogo);

export const PaymentVisa = props => {
  const source = useDynamicValue(logoUri);
  return <Image source={source} style={{width: 90, height: 25}} />;
};
