import React, {useState} from 'react';
import {View, TouchableOpacity, Image, Modal, SafeAreaView} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {image} from './../constants';
import Info from '../screens/infoSuscricion';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const pinkyGradient = ['#647DEE', '#8E54E9'];

const Headers = props => {
  const [modalVisible, setModalVisible] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const openModal = () => {
    setModalVisible(true);
  };

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={pinkyGradient}
      style={styles.headers}>
      <View style={{flexDirection: 'row', marginTop: dimensions.Height(8)}}>
        <TouchableOpacity
          onPress={() => Navigation.goBack(null)}
          style={{marginLeft: 5}}>
          <Icon name="arrowleft" type="AntDesign" size={30} color="white" />
        </TouchableOpacity>
        <Image source={image.Vetecplus} style={styles.logo} />
        <TouchableOpacity
          onPress={() => openModal()}
          style={{marginLeft: 'auto', marginRight: 8}}>
          <Icon
            name="questioncircleo"
            type="AntDesign"
            size={25}
            color="white"
          />
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible}
          presentationStyle="formSheet">
          <View style={styles.containerModal}>
            <SafeAreaView
              style={{
                width: dimensions.ScreenWidth,
                height: dimensions.Height(6),
              }}>
              <TouchableOpacity onPress={() => setModalVisible(false)}>
                <View
                  style={{
                    alignSelf: 'center',
                    marginLeft: 'auto',
                    marginRight: dimensions.Width(4),
                    marginTop: dimensions.Height(2),
                  }}>
                  <Icon
                    name="close"
                    type="AntDesign"
                    size={25}
                    color={colors.ERROR}
                  />
                </View>
              </TouchableOpacity>
            </SafeAreaView>
            <Info calcelarsus={props.calcelarsus} plus={props.plus} />
          </View>
        </Modal>
      </View>
      <View style={styles.contenedor} />
    </LinearGradient>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(18),
    backgroundColor: colors.white,
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
  },

  logo: {
    marginLeft: 'auto',
    ...ifIphoneX(
      {
        width: 250,
        height: 50,
        marginTop: 0,
      },
      {
        width: 180,
        height: 50,
        marginTop: -10,
      },
    ),
  },
});

export default Headers;
