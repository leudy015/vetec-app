import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView} from 'react-native';
import Navigation from './../services/NavigationService';
import BottomBar from 'react-native-bottom-bar';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDarkModeContext,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import AsyncStorage from '@react-native-community/async-storage';
import {Badge} from '@ant-design/react-native';

const backgroundColors = {
  light: colors.white,
  dark: colors.back_dark,
};

export const ButtomBar = props => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const mainColor = '#373f4c';
  const pinkyGradient = ['#647DEE', '#8E54E9'];
  const mode = useDarkModeContext();
  const backgroundColor = backgroundColors[mode];

  const NaviG = () => {
    if (id) {
      Navigation.navigate('Profile', {data: id});
    } else {
      Navigation.navigate('Landing');
    }
  };

  const NaviM = () => {
    if (id) {
      Navigation.navigate('Chats', {data: id});
    } else {
      Navigation.navigate('Landing');
    }
  };

  const renderMainIcon = () => {
    return (
      <TouchableOpacity onPress={() => Navigation.navigate('Maps')}>
        <Icon name="enviroment" type="AntDesign" size={40} color="white" />
      </TouchableOpacity>
    );
  };

  const renderFirstIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              right: 16,
              top: 8,
            },
            android: {
              right: 8,
              top: 10,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => Navigation.navigate('Home')}>
          <Icon
            name="home"
            type="AntDesign"
            size={30}
            style={styles.colorDark}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderSecondIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              right: 24,
              bottom: 3,
              top: 5,
            },
            android: {
              top: 3,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => Navigation.navigate('Search')}>
          <Icon
            name="search1"
            type="AntDesign"
            size={30}
            style={styles.colorDark}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderThirdIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              left: 24,
              bottom: 3,
              top: 5,
            },
            android: {
              top: 5,
              left: 3,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => NaviM()}>
          <Badge text={props.count} overflowCount={9}>
            <Icon
              name="message1"
              type="AntDesign"
              size={30}
              style={styles.colorDark}
            />
          </Badge>
        </TouchableOpacity>
      </View>
    );
  };

  const renderFourthIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              left: 16,
              top: 10,
            },
            android: {
              left: 8,
              top: 8,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => NaviG()}>
          <Icon
            name="user"
            type="AntDesign"
            size={30}
            style={styles.colorDark}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <BottomBar
          shapeColor={backgroundColor}
          mainIcon={renderMainIcon()}
          mainIconColor={mainColor}
          mainIconGradient={pinkyGradient}
          mainIconComponent={''}
          miniButtonsColor={mainColor}
          firstIconComponent={renderFirstIconComponent()}
          secondIconComponent={renderSecondIconComponent()}
          thirdIconComponent={renderThirdIconComponent()}
          fourthIconComponent={renderFourthIconComponent()}
        />
      </View>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  colorDark: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});
