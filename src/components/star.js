import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';
import {colors} from '../themes';

export default function Star(props) {
  const {star, styles} = props;
  function RenderStart() {
    switch (Number(star)) {
      case 0:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      case 1:
      case 1.1:
      case 1.2:
      case 1.3:
      case 1.4:
      case 1.5:
      case 1.5:
      case 1.6:
      case 1.7:
      case 1.8:
      case 1.9:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      case 2:
      case 2.1:
      case 2.2:
      case 2.3:
      case 2.4:
      case 2.5:
      case 2.5:
      case 2.6:
      case 2.7:
      case 2.8:
      case 2.9:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      case 3:
      case 3.1:
      case 3.2:
      case 3.3:
      case 3.4:
      case 3.5:
      case 3.5:
      case 3.6:
      case 3.7:
      case 3.8:
      case 3.9:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      case 4:
      case 4.1:
      case 4.2:
      case 4.3:
      case 4.4:
      case 4.5:
      case 4.5:
      case 4.6:
      case 4.7:
      case 4.8:
      case 4.9:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      case 5:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="star"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
      default:
        return (
          <View style={{flexDirection: 'row'}}>
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
            <Icon
              name="staro"
              type="AntDesign"
              size={15}
              color={colors.orange}
            />
          </View>
        );
        break;
    }
  }
  return <View style={styles}>{RenderStart()}</View>;
}
