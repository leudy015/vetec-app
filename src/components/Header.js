import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
  Dimensions,
} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Badge} from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {GET_NOTIFICATIONS, USER_DETAIL} from '../query';
import {Query} from 'react-apollo';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {Avatar} from 'react-native-elements';
import {NETWORK_INTERFACE_LINK_AVATAR} from './../constants/config';

const pinkyGradient = ['#647DEE', '#8E54E9'];

export default (Headers = props => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const contacto = () => {
    Linking.canOpenURL('mailto:info@vetec.es').then(supported => {
      if (supported) {
        Linking.openURL('mailto:info@vetec.es');
      } else {
        console.log("Don't know how to open URI: " + 'mailto:info@vetec.es');
      }
    });
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={pinkyGradient}
        style={styles.headers}>
        <View style={styles.hei}>
          {props.back ? (
            <View>
              {props.atrasprofile ? (
                <TouchableOpacity
                  onPress={() => Navigation.goBack(null)}
                  style={{marginLeft: 10}}>
                  <Icon
                    name="arrowleft"
                    type="AntDesign"
                    size={25}
                    color="white"
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => Navigation.goBack(null)}
                  style={{marginLeft: 10}}>
                  <Icon
                    name="arrowleft"
                    type="AntDesign"
                    size={25}
                    color="white"
                  />
                </TouchableOpacity>
              )}
            </View>
          ) : (
            <View>
              {id ? (
                <Query query={USER_DETAIL} variables={{id: id}}>
                  {({loading, error, data, refetch}) => {
                    refetch();
                    if (error) {
                      console.log('Response Error-------', error);
                      return <ActivityIndicator style={{marginLeft: 10}} />;
                    }
                    if (loading) {
                      return <ActivityIndicator style={{marginLeft: 10}} />;
                    }
                    if (data) {
                      const userDatas = data.getUsuario;
                      console.log('user', userDatas);
                      return (
                        <View>
                          <TouchableOpacity
                            onPress={() =>
                              Navigation.navigate('Profile', {data: id})
                            }
                            style={{marginLeft: 10}}>
                            {userDatas.avatar ? (
                              <Avatar
                                rounded
                                size="small"
                                source={{
                                  uri:
                                    NETWORK_INTERFACE_LINK_AVATAR +
                                    userDatas.avatar,
                                }}
                                containerStyle={{
                                  borderWidth: 2,
                                  borderColor: colors.white,
                                  ...ifIphoneX(
                                    {
                                      marginTop: 0,
                                    },
                                    {
                                      marginTop: -5,
                                    },
                                  ),
                                }}
                              />
                            ) : (
                              <TouchableOpacity
                                onPress={() =>
                                  Navigation.navigate('Profile', {data: id})
                                }
                                style={{marginLeft: 10}}>
                                <Icon
                                  name="user"
                                  type="AntDesign"
                                  size={25}
                                  color="white"
                                />
                              </TouchableOpacity>
                            )}
                          </TouchableOpacity>
                        </View>
                      );
                    }
                  }}
                </Query>
              ) : (
                <TouchableOpacity
                  onPress={() => Navigation.navigate('LoginSocial')}
                  style={{marginLeft: 10}}>
                  <Icon name="user" type="AntDesign" size={25} color="white" />
                </TouchableOpacity>
              )}
            </View>
          )}
          <CustomText
            light={colors.white}
            dark={colors.white}
            style={styles.title}>
            {props.title}
          </CustomText>
          {!props.rigth ? (
            <Query query={GET_NOTIFICATIONS} variables={{Id: id}}>
              {(data, error, loading, refetch) => {
                refetch = refetch;
                if (loading) {
                  return <ActivityIndicator />;
                }
                if (error) {
                  return console.log('Response Error-------', error);
                }
                if (data) {
                  const total =
                    data && data.data && data.data.getNotifications
                      ? data.data.getNotifications.notifications
                      : '';
                  return (
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() => Navigation.navigate('Search')}
                        style={{
                          marginLeft: 'auto',
                          marginRight: 20,
                        }}>
                        <Icon
                          name="search1"
                          type="AntDesign"
                          size={25}
                          color="white"
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          Navigation.navigate('Notifications', {data: id})
                        }
                        style={{
                          marginLeft: 'auto',
                          marginRight: total.length ? 25 : 10,
                        }}>
                        <Badge text={total.length} overflowCount={9}>
                          <Icon
                            name="bells"
                            type="AntDesign"
                            size={25}
                            color="white"
                          />
                        </Badge>
                      </TouchableOpacity>
                    </View>
                  );
                }
              }}
            </Query>
          ) : (
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => contacto()}
                style={{marginLeft: 'auto', marginRight: 20}}>
                <Icon
                  name="customerservice"
                  type="AntDesign"
                  size={25}
                  color="white"
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => contacto()}
                style={{marginLeft: 'auto', marginRight: 20}}>
                <Icon
                  name="questioncircleo"
                  type="AntDesign"
                  size={25}
                  color="white"
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </LinearGradient>
      <View style={styles.contenedor} />
    </View>
  );
});

const height = Dimensions.get('window').height;

const dynamicStyles = new DynamicStyleSheet({
  headers: {
    width: dimensions.ScreenWidth,
    ...ifIphoneX(
      {
        height: dimensions.Height(15),
      },
      {
        height: height < 600 ? dimensions.Height(25) : dimensions.Height(18),
      },
    ),
  },

  hei: {
    flexDirection: 'row',
    ...ifIphoneX(
      {
        marginTop: 50,
      },
      {
        marginTop: 50,
      },
    ),
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    left: 20,
    fontSize: dimensions.FontSize(18),
    fontWeight: '400',
    fontFamily: 'Helvetica',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(12),
      },
      {
        marginTop: height < 600 ? dimensions.Height(20) : dimensions.Height(14),
      },
    ),
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
  },
});
