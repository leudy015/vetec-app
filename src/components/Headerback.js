import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {DynamicStyleSheet, useDynamicStyleSheet} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';

const pinkyGradient = ['#647DEE', '#8E54E9'];

export default (Headers = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={pinkyGradient}
      style={styles.headers}>
      <View style={{flexDirection: 'row', marginTop: dimensions.Height(6)}}>
        <TouchableOpacity
          onPress={() => Navigation.goBack(null)}
          style={{marginLeft: 15}}>
          <Icon name="arrowleft" type="AntDesign" size={30} color="white" />
        </TouchableOpacity>
        <CustomText
          light={colors.white}
          dark={colors.white}
          style={styles.title}>
          {props.title}
        </CustomText>
        <TouchableOpacity
          onPress={() => Navigation.navigate('Cupon')}
          style={{marginLeft: 'auto', marginRight: 15}}>
          <Icon name="gift" type="AntDesign" size={30} color="white" />
        </TouchableOpacity>
      </View>
    </LinearGradient>
  );
});

const dynamicStyles = new DynamicStyleSheet({
  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(20),
    backgroundColor: colors.white,
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },
});
