import React, {useEffect, useState} from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Avatar} from 'react-native-elements';
import {GET_MASCOTAS} from '../query';
import {SwipeAction} from '@ant-design/react-native';
import {Query, Mutation} from 'react-apollo';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import 'moment/locale/es';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import Toast from 'react-native-simple-toast';
import {ELIMINAR_MASCOTA} from '../mutations';
import {stylesText} from './StyleText';
import {LineDotsLoader} from 'react-native-indicator';

export default function Mascota(props) {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const _renderItem = ({item}, refetch) => {
    refetch();
    return (
      <View
        style={{
          marginBottom: dimensions.Height(2),
        }}>
        <Mutation mutation={ELIMINAR_MASCOTA}>
          {eliminarMascota => {
            return (
              <SwipeAction
                autoClose
                style={{
                  backgroundColor: 'transparent',
                }}
                right={[
                  {
                    text: 'Eliminar',
                    style: {
                      backgroundColor: colors.ERROR,
                      color: colors.white,
                      height: 'auto',
                    },
                    onPress: () =>
                      eliminarMascota({
                        variables: {id: item.id},
                      }).then(() => {
                        Toast.showWithGravity(
                          'Mascota eliminada con éxito',
                          Toast.LONG,
                          Toast.TOP,
                        );
                        refetch();
                      }),
                  },
                ]}
                onOpen={() => console.log('open')}
                onClose={() => console.log('close')}>
                <TouchableOpacity
                  style={styles.cardinfo}
                  onPress={() =>
                    Navigation.navigate('DetailsMascota', {
                      data: item,
                    })
                  }>
                  <View
                    style={{
                      alignSelf: 'center',
                      marginLeft: dimensions.Width(4),
                    }}>
                    <Avatar
                      rounded
                      size={50}
                      source={{
                        uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar,
                      }}
                      containerStyle={styles.avatar}
                    />
                  </View>
                  <View
                    style={{
                      alignSelf: 'center',
                      marginLeft: dimensions.Width(4),
                    }}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.white}
                      style={stylesText.mainText}>
                      {item.name}
                    </CustomText>
                    <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                      {moment(item.age).format('ll')}
                    </CustomText>
                  </View>
                  <View
                    style={{
                      alignSelf: 'center',
                      marginLeft: 'auto',
                      marginRight: dimensions.Width(4),
                    }}>
                    <Icon
                      name="right"
                      type="AntDesign"
                      size={20}
                      color={colors.rgb_153}
                    />
                  </View>
                </TouchableOpacity>
              </SwipeAction>
            );
          }}
        </Mutation>
      </View>
    );
  };
  return (
    <Query query={GET_MASCOTAS} variables={{usuarios: id}}>
      {response => {
        if (response.loading) {
          return <LineDotsLoader color={colors.main} />;
        }
        if (response.error) {
          return <LineDotsLoader color={colors.main} />;
        }
        if (response) {
          return (
            <FlatList
              data={
                response && response.data && response.data.getMascota
                  ? response.data.getMascota.list
                  : ''
              }
              renderItem={item => _renderItem(item, response.refetch)}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
            />
          );
        } else {
          return <LineDotsLoader color={colors.main} />;
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(93),
    height: 80,
    borderRadius: 10,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },
});
