import React from 'react';
import {StyleSheet, Text, View, Vibration} from 'react-native';
import QB from 'quickblox-react-native-sdk';
import WebRTCView from 'quickblox-react-native-sdk/RTCView';
import InCallManager from 'react-native-incall-manager';
import CallScreenButton from './CallScreenButton';
import NavigationService from '../services/NavigationService';
import {colors} from '../themes/colors';
import {image} from '../constants/image';
import AsyncStorage from '@react-native-community/async-storage';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.darkBackground,
    flex: 1,
    width: '100%',
    height: '100%',
  },

  videoStyle: {
    flex: 1,
    height: '100%',
    ...ifIphoneX(
      {
        width: '162%',
      },
      {
        width: '135%',
      },
    ),
  },
  opponentsContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginTop: 100,
    width: '100%',
    height: '100%',
  },
  videosContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  opponentView: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    width: '100%',
  },
  circleView: {
    alignItems: 'center',
    borderRadius: 30,
    height: 60,
    justifyContent: 'center',
    marginBottom: 16,
    width: 60,
  },
  circleText: {
    color: colors.white,
    fontSize: 25,
    lineHeight: 30,
    textAlign: 'center',
  },
  buttons: {
    alignItems: 'center',
    bottom: 30,
    flexDirection: 'row',
    justifyContent: 'space-around',
    left: 0,
    padding: 5,
    position: 'absolute',
    right: 0,
    width: '100%',
    backgroundColor: 'transparent',
    zIndex: 100,
  },
  usernameText: {
    color: colors.white,
    fontSize: 17,
    fontWeight: '600',
    lineHeight: 20,
    width: '100%',
    textAlign: 'center',
  },
  statusText: {
    color: '#b3bed4',
    fontSize: 15,
    lineHeight: 18,
  },
  buttonActive: {
    backgroundColor: '#6d7c94',
  },
  buttonImageActive: {
    tintColor: colors.inputShadow,
  },
});

const ONE_SECOND_IN_MS = 500;

const PATTERN = [1 * ONE_SECOND_IN_MS];

const PeerStateText = {
  [QB.webrtc.RTC_PEER_CONNECTION_STATE.NEW]: 'Llamando...',
  [QB.webrtc.RTC_PEER_CONNECTION_STATE.CONNECTED]: 'Conectado',
  [QB.webrtc.RTC_PEER_CONNECTION_STATE.DISCONNECTED]: 'Desconectado',
  [QB.webrtc.RTC_PEER_CONNECTION_STATE.FAILED]: 'Fallo de conexión',
  [QB.webrtc.RTC_PEER_CONNECTION_STATE.CLOSED]: 'Conexión cerrada',
};

export default class CallScreen extends React.Component {
  isIncomingCall = this.props.session.initiatorId !== this.props.currentUser.id;

  state = {
    loudspeaker: false,
    muteAudio: false,
    muteVideo: false,
    id: null,
  };

  async componentDidMount() {
    const id = await AsyncStorage.getItem('id');
    this.setState({id: id});
    const {getUsers, onCall, session} = this.props;
    if (session) {
      if (!onCall) {
        const startOpts = {
          ringback: '_BUNDLE_',
          media:
            session.type === QB.webrtc.RTC_SESSION_TYPE.AUDIO
              ? 'audio'
              : 'video',
        };
        if (this.isIncomingCall) {
          InCallManager.startRingtone();
        } else {
          InCallManager.start(startOpts);
        }
      }
      const userIds = session.opponentsIds.concat(session.initiatorId).join();
      getUsers({
        append: true,
        filter: {
          field: QB.users.USERS_FILTER.FIELD.ID,
          type: QB.users.USERS_FILTER.TYPE.NUMBER,
          operator: QB.users.USERS_FILTER.OPERATOR.IN,
          value: userIds,
        },
      });
    }
  }

  componentDidUpdate() {
    const {session} = this.props;
    if (!session) {
      NavigationService.navigate('Home');
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {loudspeaker, muteAudio, muteVideo} = this.state;
    const {caller, onCall, opponentsLeftCall, session, users} = this.props;
    if (onCall !== nextProps.onCall) {
      if (!onCall && nextProps.onCall && !this.isIncomingCall) {
        InCallManager.stopRingback();
      }
    }
    if (session && !nextProps.session && !onCall) {
      if (this.isIncomingCall) {
        InCallManager.stopRingtone();
      } else {
        InCallManager.stop({
          busytone: '_BUNDLE_',
        });
      }
    }
    return (
      onCall !== nextProps.onCall ||
      users.length !== nextProps.users.length ||
      session !== nextProps.session ||
      loudspeaker !== nextProps.loudspeaker ||
      muteAudio !== nextState.muteAudio ||
      muteVideo !== nextState.muteVideo ||
      opponentsLeftCall.length !== nextProps.opponentsLeftCall.length ||
      caller !== nextProps.caller
    );
  }

  componentWillUnmount() {
    InCallManager.stop();
  }
  acceptHandler = async () => {
    const {accept, session} = this.props;
    accept({sessionId: session.id});
    InCallManager.stopRingtone();
    Vibration.vibrate(PATTERN);
  };

  callEndHandler = () => {
    Vibration.vibrate(PATTERN);
    const {hangUp, reject, session} = this.props;
    if (
      this.isIncomingCall &&
      session.state < QB.webrtc.RTC_SESSION_STATE.CONNECTED
    ) {
      reject({sessionId: session.id});
      this.currentCallId = null;
    } else {
      hangUp({sessionId: session.id});
    }
  };

  getCurrentCallId = () => {
    if (!this.currentCallId) {
      this.currentCallId = uuidv4();
    }

    return this.currentCallId;
  };

  toggleAudio = () => {
    const {muteAudio} = this.state;
    const {session, toggleAudio} = this.props;
    toggleAudio({
      sessionId: session.id,
      enable: muteAudio,
    });
    this.setState({muteAudio: !muteAudio});
  };

  toggleVideo = () => {
    const {muteVideo} = this.state;
    const {session, toggleVideo} = this.props;
    toggleVideo({
      sessionId: session.id,
      enable: muteVideo,
    });
    this.setState({muteVideo: !muteVideo});
  };

  switchAudioOutput = () => {
    const {loudspeaker} = this.state;
    const {switchAudio} = this.props;
    const output = loudspeaker
      ? QB.webrtc.AUDIO_OUTPUT.EARSPEAKER
      : QB.webrtc.AUDIO_OUTPUT.LOUDSPEAKER;
    switchAudio({output});
    this.setState({loudspeaker: !loudspeaker});
  };

  switchCamera = () => {
    const {session, switchCamera} = this.props;
    switchCamera({sessionId: session.id});
  };

  getOpponentsCircles = () => {
    const {currentUser, peers, session, users} = this.props;
    const userIds = session.opponentsIds
      .concat(session.initiatorId)
      .filter(userId => userId !== currentUser.id);
    return (
      <View style={styles.opponentsContainer}>
        {userIds.map(userId => {
          const user = users.find(user => user.id === userId);
          const username = user
            ? user.fullName || user.login || user.email
            : '';
          const backgroundColor =
            user && user.color ? user.color : colors.primaryDisabled;
          const peerState = peers[userId] || 0;
          return (
            <View key={userId} style={styles.opponentView}>
              <View style={[styles.circleView, {backgroundColor}]}>
                <Text style={styles.circleText}>{username.charAt(0)}</Text>
              </View>
              <Text style={styles.usernameText}>{username}</Text>
              <Text style={styles.statusText}>{PeerStateText[peerState]}</Text>
            </View>
          );
        })}
      </View>
    );
  };

  getVideoViews = () => {
    const {currentUser, opponentsLeftCall, session} = this.props;
    if (session.type === QB.webrtc.RTC_SESSION_TYPE.VIDEO) {
      const opponentsIds = session.opponentsIds
        .concat(session.initiatorId)
        .filter(
          id => opponentsLeftCall.indexOf(id) === -1 && id !== currentUser.id,
        );
      const myVideoStyle = {
        height: 180,
        width: 80,
        marginLeft: '65%',
        position: 'absolute',
        borderRadius: 20,
        ...ifIphoneX(
          {
            marginTop: 80,
          },
          {
            marginTop: 40,
          },
        ),
        zIndex: 100,
      };
      return (
        <React.Fragment>
          {opponentsIds.map(userId => (
            <WebRTCView
              key={userId}
              sessionId={session.id}
              style={styles.videoStyle}
              userId={userId}
            />
          ))}
          {this.state.muteVideo ? (
            <View style={[myVideoStyle, {backgroundColor: colors.black}]} />
          ) : (
            <WebRTCView
              key={currentUser.id}
              mirror
              sessionId={session.id}
              style={myVideoStyle}
              userId={currentUser.id}
            />
          )}
        </React.Fragment>
      );
    } else {
      return this.getOpponentsCircles();
    }
  };

  getButtons = () => {
    const {loudspeaker, muteAudio, muteVideo} = this.state;
    const {session} = this.props;
    return (
      <View style={styles.buttons}>
        <CallScreenButton
          buttonStyle={muteAudio ? styles.buttonActive : undefined}
          image={image.MIC_OFF}
          imageStyle={muteAudio ? styles.buttonImageActive : undefined}
          onPress={this.toggleAudio}
          text={muteAudio ? 'Micró of' : 'Micró'}
        />
        {session.type === QB.webrtc.RTC_SESSION_TYPE.VIDEO ? (
          <CallScreenButton
            buttonStyle={muteVideo ? styles.buttonActive : undefined}
            image={image.CAM_OFF}
            imageStyle={muteVideo ? styles.buttonImageActive : undefined}
            onPress={this.toggleVideo}
            text={muteVideo ? 'Camara off' : 'Camara on'}
          />
        ) : null}
        {session.type === QB.webrtc.RTC_SESSION_TYPE.VIDEO ? (
          <CallScreenButton
            onPress={this.switchCamera}
            image={image.SWITCH_CAMERA}
            text="Girar cam"
          />
        ) : null}
        <CallScreenButton
          image={image.CALL_END}
          buttonStyle={{
            backgroundColor: colors.redBackground,
          }}
          onPress={this.callEndHandler}
          text="Finalizar"
        />
        {session.type === QB.webrtc.RTC_SESSION_TYPE.AUDIO ? (
          <CallScreenButton
            buttonStyle={loudspeaker ? styles.buttonActive : undefined}
            image={image.SPEAKER}
            imageStyle={loudspeaker ? styles.buttonImageActive : undefined}
            onPress={this.switchAudioOutput}
            text={loudspeaker ? 'Speaker' : 'Mic'}
          />
        ) : null}
      </View>
    );
  };

  callScreen = () => {
    return (
      <View style={styles.videosContainer}>
        {this.getVideoViews()}
        {this.getButtons()}
      </View>
    );
  };

  dialingScreen = () => {
    const {caller, session} = this.props;
    if (!session) return null;
    let username = '';
    if (caller) {
      username = caller.fullName || caller.login || caller.email;
    }
    const circleBackground = {
      backgroundColor:
        caller && caller.color ? caller.color : colors.primaryDisabled,
    };
    return (
      <View style={{flex: 1, width: '100%'}}>
        {this.isIncomingCall ? (
          <View style={styles.opponentsContainer}>
            <View style={[styles.opponentView, {width: '100%'}]}>
              <View style={[styles.circleView, circleBackground]}>
                <Text style={styles.circleText}>{username.charAt(0)}</Text>
              </View>
              <Text numberOfLines={1} style={styles.usernameText}>
                {username}
              </Text>
              <Text style={styles.statusText}>Llamando...</Text>
            </View>
          </View>
        ) : (
          this.getOpponentsCircles()
        )}
        <View style={styles.buttons}>
          <CallScreenButton
            buttonStyle={{
              backgroundColor: colors.redBackground,
            }}
            image={image.CALL_END}
            onPress={this.callEndHandler}
            text={this.isIncomingCall ? 'Rechazar' : 'Finalizar'}
          />
          {this.isIncomingCall ? (
            <CallScreenButton
              buttonStyle={{
                backgroundColor: colors.lightGreen,
              }}
              image={
                session.type === QB.webrtc.RTC_SESSION_TYPE.AUDIO
                  ? image.CALL_ACCEPT
                  : image.VIDEO_ACCEPT
              }
              onPress={this.acceptHandler}
              text="Aceptar"
            />
          ) : null}
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.props.onCall ? this.callScreen() : this.dialingScreen()}
      </View>
    );
  }
}
