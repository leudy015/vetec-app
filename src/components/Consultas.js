import React, {useEffect, useState} from 'react';
import {View, FlatList, TouchableOpacity, Image, Alert} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';
import {Query} from 'react-apollo';
import {CONSULTA_BY_USUARIO} from '../query/index';
import AsyncStorage from '@react-native-community/async-storage';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from '../constants/config';
import {connect} from 'react-redux';
import NoData from './NoData';
import io from 'socket.io-client';
import * as StoreReview from 'react-native-store-review';
import * as Progress from 'react-native-progress';
import {usersGet} from '../actionCreators';
import LoadingPLace from './Placeholder';
import {stylesText} from './StyleText';
import Star from '../components/star';

function Consulta({getUsers}) {
  const [id, setId] = useState('');

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    }
  }, []);

  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}, refetch) => {
    refetch();
    socket.on('connect', () => {
      socket.emit('online_user', {
        usuario: item.usuario.id,
        profesional: item.profesional.id,
        id: item.profesional.id,
      });
    });

    let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
    item.profesional.professionalRatinglist.forEach(start => {
      if (start.rate === 1) rating['1'] += 1;
      else if (start.rate === 2) rating['2'] += 1;
      else if (start.rate === 3) rating['3'] += 1;
      else if (start.rate === 4) rating['4'] += 1;
      else if (start.rate === 5) rating['5'] += 1;
    });

    const ar =
      (5 * rating['5'] +
        4 * rating['4'] +
        3 * rating['3'] +
        2 * rating['2'] +
        1 * rating['1']) /
      item.profesional.professionalRatinglist.length;
    let averageRating = 0;
    if (item.profesional.professionalRatinglist.length) {
      averageRating = ar.toFixed(1);
    }

    const Navigationachat = () => {
      socket.emit('available', {
        user: item.usuario.id,
        prof: item.profesional.id,
      });
      Navigation.navigate('ChatsScreens', {data: item});
    };

    const isNavigation = () => {
      if (id) {
        Navigation.navigate('SelecMascota', {data: item.profesional});
      } else {
        Alert.alert(
          'Para continuar debes iniciar sesión',
          'Debes iniciar sesión para continuar con la contratación',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
      }
    };

    return (
      <TouchableOpacity
        onPress={() => Navigation.navigate('ConsultasDetails', {data: item})}
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.avatar}>
            <Image
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar,
              }}
              style={styles.avatar}
            />
          </View>
          <View style={{marginLeft: dimensions.Width(2)}}>
            <View
              style={{
                flexDirection: 'row',
                width: '90%',
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={[stylesText.mainText, {width: dimensions.Width(25)}]}>
                {item.profesional.nombre} {item.profesional.apellidos}
              </CustomText>
              {item.profesional.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={12}
                  style={{
                    alignSelf: 'center',
                    marginTop: 3,
                    marginLeft: 3,
                    color: colors.main,
                  }}
                />
              ) : null}

              <CustomText
                style={stylesText.secondaryText}
                numberOfLines={1}
                style={{
                  color: colors.main,
                  marginLeft: 'auto',
                  marginRight: 10,
                  width: 80,
                }}>
                {item.estado}
              </CustomText>
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                style={[
                  stylesText.secondaryText,
                  {color: colors.main, marginTop: dimensions.Height(1)},
                ]}>
                {item.profesional.profesion}
              </CustomText>
              <Star
                star={averageRating}
                styles={{marginLeft: 'auto', marginTop: 10}}
              />
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginRight: 40,
                    marginTop: dimensions.Height(1),
                  },
                ]}>
                ({averageRating})
              </CustomText>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Width(2)},
              ]}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.profesional.ciudad}
            </CustomText>
          </View>
        </View>
        <CustomText
          light={colors.rgb_153}
          dark={colors.rgb_153}
          style={[
            stylesText.mainText,
            {
              marginTop: dimensions.Height(1),
              marginLeft: 5,
              marginBottom: 10,
            },
          ]}>
          {item.profesional.Precio}€ /Total
        </CustomText>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          {item.estado !== 'Valorada' ? (
            <View style={{marginLeft: 0, marginTop: 5}}>
              <Progress.Circle
                size={20}
                indeterminate={true}
                color={colors.main}
              />
            </View>
          ) : (
            <View style={{marginLeft: 0, marginTop: 5}}>
              <Progress.Circle
                size={20}
                indeterminate={false}
                color={colors.main}
              />
            </View>
          )}
          {item.estado === 'Pendiente de pago' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() =>
                    Navigation.navigate('PaymentPage', {data: item})
                  }
                  title="Completar el pago"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}
          {item.estado === 'Nueva' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.nueva}>
                <Icon
                  name="clockcircleo"
                  type="AntDesign"
                  size={14}
                  color={colors.orange}
                  style={{marginRight: 5}}
                />
                <CustomText
                  light={colors.back_suave_dark}
                  dark={colors.rgb_153}>
                  Estamos esperando a que el veterinario acepte la consulta, no
                  suele tardar más de 5 minutos. ¡Gracias!
                </CustomText>
              </View>
            </View>
          ) : null}
          {item.estado === 'Aceptado' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => Navigationachat()}
                  title="Inicar consulta"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}
          {item.estado === 'Completada' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() =>
                    Navigation.navigate('ConsultasDetails', {data: item})
                  }
                  title="Valorar"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}

          {item.estado === 'Valorada' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => isNavigation()}
                  title="Repetir"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={CONSULTA_BY_USUARIO} variables={{usuarios: id}}>
      {response => {
        if (response.loading) {
          return <LoadingPLace />;
        }
        if (response.error) {
          return <LoadingPLace />;
        }
        if (response) {
          return (
            <FlatList
              data={
                response && response.data && response.data.getConsultaByUsuario
                  ? response.data.getConsultaByUsuario.list
                  : ''
              }
              renderItem={item => _renderItem(item, response.refetch)}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={
                <NoData menssge="Aún no has realizado nigúna consulta" />
              }
            />
          );
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  card: {
    width: dimensions.Width(92),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(45),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },

  nueva: {
    flexDirection: 'row',
    width: dimensions.Width(65),
    margin: 10,
  },
});

const mapDispatchToProps = {
  getUsers: usersGet,
};

export default connect(
  null,
  mapDispatchToProps,
)(Consulta);
