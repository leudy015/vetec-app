import React, {useEffect, useState} from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Avatar} from 'react-native-elements';
import {GET_MASCOTAS} from '../query';
import {ActivityIndicator} from '@ant-design/react-native';
import {Query} from 'react-apollo';
import _get from 'lodash.get';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigationParam} from 'react-navigation-hooks';
import {withApollo} from 'react-apollo';
import moment from 'moment';
import 'moment/locale/es';
import NoData from './NoData';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';

const MascotaforPayment = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const data = useNavigationParam('data');

  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.cardinfo}
        onPress={() => Navigation.navigate('DetailsMascota', {data: item})}>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <Avatar
            rounded
            size={50}
            source={{uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar}}
            containerStyle={styles.avatar}
          />
        </View>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.name1}>
            {item.name}
          </CustomText>
          <CustomText light={colors.back_dark} dark={colors.rgb_153}>
            {moment(item.age).format('ll')}
          </CustomText>
        </View>
        <View
          style={{
            alignSelf: 'center',
            marginLeft: 'auto',
            marginRight: dimensions.Width(4),
          }}>
          <Icon
            name="right"
            type="AntDesign"
            size={20}
            color={colors.rgb_153}
          />
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={GET_MASCOTAS} variables={{usuarios: id}}>
      {({loading, error, data}) => {
        if (loading) {
          return <ActivityIndicator />;
        }
        if (error) {
          return console.log('error in mascota', error);
        }
        if (data) {
          return (
            <FlatList
              data={data && data.getMascota ? data.getMascota.list : ''}
              renderItem={item => _renderItem(item)}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={
                <NoData menssge="Aún no has añadido una mascota" />
              }
            />
          );
        } else {
          return <ActivityIndicator />;
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(93),
    height: 80,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(0),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },
});

export default withApollo(MascotaforPayment);
