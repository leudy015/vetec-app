import React from 'react';
import {Image} from 'react-native';
import {image} from '../constants/image';
import {DynamicValue, useDynamicValue} from 'react-native-dark-mode';
const lightLogo = image.Iconplusnoche;
const darkLogo = image.Iconplusdia;

const logoUri = new DynamicValue(lightLogo, darkLogo);

export const IconPlus = () => {
  const source = useDynamicValue(logoUri);
  return <Image source={source} style={{width: 40, height: 40}} />;
};
