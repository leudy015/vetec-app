import React from 'react';
import {View, ScrollView} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from '../components/CustomText';
import _get from 'lodash.get';

const Informacion = () => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <CustomText
        style={{
          fontSize: dimensions.FontSize(16),
          color: colors.slight_black,
          textAlign: 'center',
          marginBottom: 15,
        }}>
        Ayuda
      </CustomText>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{marginRight: 20, marginBottom: dimensions.Height(30)}}>
          <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
            <Icon
              type="AntDesign"
              name="creditcard"
              style={{alignSelf: 'center'}}
              size={30}
              color={colors.rgb_153}
            />
            <View style={{marginLeft: 20, alignSelf: 'center'}}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                Métodos de pago
              </CustomText>
              <CustomText
                style={{
                  fontSize: dimensions.FontSize(14),
                  color: colors.slight_black,
                  fontWeight: '200',
                }}>
                Aceptamos las principales tarjetas de crédito y débito, incluida
                Visa, Mastercard y American Express.
              </CustomText>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
            <Icon
              type="AntDesign"
              name="lock1"
              style={{alignSelf: 'center'}}
              size={30}
              color={colors.rgb_153}
            />
            <View style={{marginLeft: 20, alignSelf: 'center'}}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                ¿Qué pasa si el profesional no cumple con la consulta?
              </CustomText>
              <CustomText
                style={{
                  fontSize: dimensions.FontSize(14),
                  color: colors.slight_black,
                  fontWeight: '200',
                }}>
                Retenemos tu dinero hasta 7 días para garantizar la sastifación
                del servicio.
              </CustomText>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
            <Icon
              type="AntDesign"
              name="Safety"
              style={{alignSelf: 'center'}}
              size={30}
              color={colors.rgb_153}
            />
            <View style={{marginLeft: 20, alignSelf: 'center'}}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                Seguridad
              </CustomText>
              <CustomText
                style={{
                  fontSize: dimensions.FontSize(14),
                  color: colors.slight_black,
                  fontWeight: '200',
                }}>
                Utilizamos las última tecnología segura SSL.
              </CustomText>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
            <Icon
              type="AntDesign"
              name="customerservice"
              style={{alignSelf: 'center'}}
              size={30}
              color={colors.rgb_153}
            />
            <View style={{marginLeft: 20, alignSelf: 'center'}}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                Estamos aquí para ayudarte
              </CustomText>
              <CustomText
                style={{
                  fontSize: dimensions.FontSize(14),
                  color: colors.slight_black,
                  fontWeight: '200',
                }}>
                Si tienes mas dudas o preguntas puede contactarnos en cualquier
                momento info@vetec.es
              </CustomText>
            </View>
          </View>

          <View style={{flexDirection: 'row', padding: 20, margin: 10}}>
            <Icon
              type="AntDesign"
              name="idcard"
              style={{alignSelf: 'center'}}
              size={30}
              color={colors.rgb_153}
            />
            <View style={{marginLeft: 20, alignSelf: 'center'}}>
              <CustomText
                light={colors.black}
                dark={colors.white}
                style={{fontSize: dimensions.FontSize(18), marginBottom: 10}}>
                ¿Cómo realizo mi consulta?
              </CustomText>
              <CustomText
                style={{
                  fontSize: dimensions.FontSize(14),
                  color: colors.slight_black,
                  fontWeight: '200',
                }}>
                Una vez realizado el pago te redigiremos a la pagina mis
                consultas de tu perfil y hay tendrás habilitado el boto de chat,
                "Sujeto a aprobación del profesional."
              </CustomText>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  headers1: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(13),
    backgroundColor: colors.white,
  },
  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  buttonView: {
    alignSelf: 'center',
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(3),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  tarjeta: {
    padding: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.rgb_235,
  },

  instruction: {
    textAlign: 'center',
    marginBottom: 5,
  },
  token: {
    padding: 20,
    textAlign: 'center',
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(10),
  },
  bacs: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});

export default Informacion;
