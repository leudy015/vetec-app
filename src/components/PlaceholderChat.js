import React from 'react';
import {SafeAreaView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dark-mode';
import {colors, dimensions} from './../themes';

export default function Loading() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <SafeAreaView style={{alignSelf: 'center'}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginTop: 50}}>
          <View
            style={{
              width: 80,
              height: 80,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 15}}>
            <View
              style={{
                width: dimensions.Width(40),
                height: 20,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 15,
                borderRadius: 5,
                marginTop: 15,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    </SafeAreaView>
  );
}
