import React, {useState, useEffect} from 'react';
import {ActivityIndicator, Text, View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {Avatar} from 'react-native-elements';
import {CustomText} from './CustomText';
import {stylesText} from './StyleText';
import Icon from 'react-native-dynamic-vector-icons';
import CountDown from 'react-native-countdown-component';
import Navigationservice from './../services/NavigationService';
import AsyncStorage from '@react-native-community/async-storage';

const Loading = (props, {message}) => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const datos = props.data;
  return (
    <View style={styles.container}>
      <View style={{marginTop: dimensions.Height(25), alignItems: 'center'}}>
        <Avatar
          rounded
          size={120}
          source={{
            uri: NETWORK_INTERFACE_LINK_AVATAR + datos.avatar,
          }}
          containerStyle={styles.avatar}
        />

        <View style={{flexDirection: 'row'}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[stylesText.mainText, {textAlign: 'center', marginTop: 15}]}>
            {datos.nombre} {datos.apellidos}
          </CustomText>
          {datos.isVerified ? (
            <Icon
              name="verified"
              type="Octicons"
              size={12}
              style={{
                alignSelf: 'center',
                marginTop: 15,
                marginLeft: 5,
                color: colors.main,
              }}
            />
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginBottom: 15,
            alignItems: 'center',
          }}>
          <Icon name="star" type="AntDesign" size={20} color={colors.orange} />
          <Icon name="star" type="AntDesign" size={20} color={colors.orange} />
          <Icon name="star" type="AntDesign" size={20} color={colors.orange} />
          <Icon name="star" type="AntDesign" size={20} color={colors.orange} />
          <Icon name="star" type="AntDesign" size={20} color={colors.orange} />
        </View>
      </View>
      <View>
        <View
          style={{flexDirection: 'row', alignSelf: 'center', marginTop: 15}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.mainText,
              {textAlign: 'center', paddingHorizontal: 15},
            ]}>
            Conectando...
          </CustomText>
          <ActivityIndicator color={colors.main} size="small" />
        </View>

        <View
          style={{
            marginTop: 50,
            marginBottom: 15,
            alignItems: 'center',
            paddingHorizontal: 30,
          }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={[
              stylesText.mainText,
              {textAlign: 'center', paddingHorizontal: 30},
            ]}>
            Conectando con el profesional esto puede tardar entre uno a dos
            minutos.
          </CustomText>
          <CountDown
            until={60 * 1 + 0}
            size={8}
            onFinish={() => Navigationservice.navigate('Home')}
            digitStyle={{
              backgroundColor: 'transparent',
              marginTop: 29,
            }}
            digitTxtStyle={{
              color: colors.rgb_153,
              fontWeight: '200',
              fontSize: 14,
            }}
            timeToShow={['M', 'S']}
            timeLabels={false}
          />
          <View style={{marginTop: dimensions.Height(2)}}>
            <TouchableOpacity
              style={styles.btncont}
              onPress={() => Navigationservice.navigate('Home')}>
              <CustomText
                light={colors.main}
                dark={colors.white}
                style={styles.te}>
                Volver a intentarlo
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {message ? (
        <Text style={{color: colors.rgb_153, textAlign: 'center'}}>
          {message}
        </Text>
      ) : null}
    </View>
  );
};

Loading.navigationOptions = {title: 'Loading'};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignItems: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    flex: 1,
    width: '100%',
  },
  btncont: {
    width: dimensions.Width(85),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.main),
    borderWidth: 1,
    borderColor: colors.main,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
});

export default Loading;
