import React from 'react';
import {TextInput, View, Image, TouchableOpacity, Text} from 'react-native';
import {dimensions, colors} from './../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';

export const CustomTextInput = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <View style={[styles.content, props.containerStyle]}>
      {props.right ? (
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center'}}
          onPress={() => props.rightIconPress()}>
          <Image
            style={{alignSelf: 'center'}}
            resizeMode="contain"
            source={props.right}
          />
        </TouchableOpacity>
      ) : null}
      <TextInput
        {...props}
        style={[props.textInputStyle, styles.textInputStyle]}
      />
      <TouchableOpacity style={styles.apply} onPress={() => props.onPress()}>
        <Text>Aplicar</Text>
      </TouchableOpacity>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputStyle: {
    paddingVertical: 0,
    height: 60,
    borderColor: colors.grey,
    width: '65.5%',
    paddingHorizontal: dimensions.Width(4),
  },
  hashTrix: {
    color: colors.red,
    marginVertical: dimensions.Height(1),
  },
  text: {
    color: new DynamicValue(colors.white, colors.black),
    fontSize: dimensions.FontSize(14),
    justifyContent: 'center',
    textAlignVertical: 'center',
    alignContent: 'center',
    marginVertical: dimensions.Height(2),
  },
  content: {
    flexDirection: 'row',
    flex: 1,
    borderRadius: dimensions.Width(2),
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
  },

  apply: {
    backgroundColor: new DynamicValue(colors.light_grey, colors.rgb_153),
    paddingHorizontal: 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginLeft: 'auto',
  },
});
