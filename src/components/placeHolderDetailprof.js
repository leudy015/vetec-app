import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dark-mode';
import {colors, dimensions} from './../themes';

export default function PlaceHolder() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  const back = useDynamicValue(colors.white, colors.back_dark);
  return (
    <View style={{alignSelf: 'center', backgroundColor: back, height: '110%'}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            height: dimensions.ScreenHeight,
            alignItems: 'center',
          }}>
          <View style={{marginTop: 20}}>
            <View
              style={{
                width: dimensions.Width(100),
                height: 350,
                marginTop: -20,
              }}
            />

            <View style={{flexDirection: 'row'}}>
              <View>
                <View
                  style={{
                    width: dimensions.Width(50),
                    height: 20,
                    marginTop: 30,
                    borderRadius: 30,
                    marginLeft: 15,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(40),
                    height: 15,
                    marginTop: 15,
                    borderRadius: 30,
                    marginLeft: 15,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(30),
                    height: 8,
                    marginTop: 15,
                    borderRadius: 30,
                    marginLeft: 15,
                  }}
                />
              </View>
              <View style={{marginLeft: 'auto', marginRight: 15}}>
                <View
                  style={{
                    width: dimensions.Width(20),
                    height: 20,
                    marginTop: 30,
                    borderRadius: 30,
                  }}
                />
                <View
                  style={{
                    width: dimensions.Width(15),
                    height: 10,
                    marginTop: 10,
                    borderRadius: 30,
                  }}
                />
              </View>
            </View>
            <View style={{marginTop: 40, flexDirection: 'row', marginLeft: 15}}>
              <View
                style={{
                  width: dimensions.Width(20),
                  height: dimensions.Width(20),
                  borderRadius: 10,
                  marginRight: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(20),
                  height: dimensions.Width(20),
                  borderRadius: 10,
                  marginRight: 20,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(20),
                  height: dimensions.Width(20),
                  borderRadius: 10,
                  marginRight: 20,
                }}
              />
            </View>
            <View
              style={{
                width: dimensions.Width(90),
                height: 8,
                marginTop: 30,
                borderRadius: 30,
                marginLeft: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(80),
                height: 8,
                marginTop: 15,
                borderRadius: 30,
                marginLeft: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(70),
                height: 8,
                marginTop: 15,
                borderRadius: 30,
                marginLeft: 15,
              }}
            />
          </View>
          <View
            style={{
              width: dimensions.Width(95),
              height: 90,
              marginTop: 35,
              borderRadius: 15,
            }}
          />

          <View>
            <View
              style={{
                width: dimensions.Width(90),
                height: 8,
                marginTop: 30,
                borderRadius: 30,
                marginLeft: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(80),
                height: 8,
                marginTop: 15,
                borderRadius: 30,
                marginLeft: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(100),
                height: 8,
                marginTop: 15,
                borderRadius: 30,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}
