import React, {useState} from 'react';
import {View, FlatList, TouchableOpacity, Image} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';
import NoData from './NoData';
import {stylesText} from './StyleText';
import WebViweb from '../components/Webview';

export default function Card(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [visibleModal, setvisibleModal] = useState(false);

  const abrirweb = () => {
    setvisibleModal(true);
  };

  const cerrarweb = () => {
    setvisibleModal(false);
  };

  const _renderItem = ({item}) => {
    console.log('item card', item);
    return (
      <TouchableOpacity
        onPress={() =>
          Navigation.navigate('DetailsAdoptar', {
            data: item,
          })
        }
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View style={styles.avatar}>
            <Image
              source={{
                uri: item.avatar,
              }}
              style={styles.avatar}
            />
          </View>
          <View
            style={{
              marginLeft: dimensions.Width(4),
            }}>
            <View
              style={{
                flexDirection: 'row',
                width: dimensions.Width(65),
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.light_white}
                style={[
                  stylesText.mainText,
                  {
                    maxWidth: dimensions.Width(42),
                    width: 'auto',
                  },
                ]}>
                {item.name}
              </CustomText>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={[
                  stylesText.secondaryText,
                  {
                    marginLeft: 'auto',
                    marginRight: 25,
                  },
                ]}>
                <Icon
                  name="heart"
                  type="AntDesign"
                  size={16}
                  style={{
                    alignSelf: 'center',
                    marginTop: 0,

                    color: colors.ERROR,
                  }}
                />{' '}
                {''} Adorable
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: dimensions.Width(65),
              }}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[
                  stylesText.secondaryText,
                  {
                    marginTop: dimensions.Height(1),
                  },
                ]}>
                Edad: {item.age}
              </CustomText>
            </View>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1)},
              ]}>
              Género: {item.genero}
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1)},
              ]}>
              Raza: {item.raza}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingTop: dimensions.Width(3),
          }}>
          <View style={{marginLeft: 'auto'}}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => abrirweb()}
                title="¡Lo quiero adoptar!"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
        <WebViweb
          url={item.url}
          visibleModal={visibleModal}
          cerrarweb={cerrarweb}
        />
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={props.data}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <NoData menssge="Aún no tenemos ningún profesional" />
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(96),
    height: 'auto',
    alignSelf: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatar: {
    width: 90,
    height: 90,
    borderRadius: 13,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(87),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
});
