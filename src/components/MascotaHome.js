import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  Alert,
  Dimensions,
} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Avatar} from 'react-native-elements';
import {Query} from 'react-apollo';
import {GET_MASCOTAS} from '../query/index';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import AsyncStorage from '@react-native-community/async-storage';
import {LineDotsLoader} from 'react-native-indicator';
import Icon from 'react-native-dynamic-vector-icons';
import AddMascota from '../screens/AddMascota';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const pinkyGradient = ['#647DEE', '#8E54E9'];

export default function MascotaHome() {
  const [id, setId] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const openModal = () => {
    setModalVisible(true);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}, refetch) => {
    refetch();
    return (
      <View>
        <TouchableOpacity
          onPress={() => Navigation.navigate('DetailsMascota', {data: item})}
          style={{marginLeft: dimensions.Height(2)}}>
          <Avatar
            rounded
            size={100}
            source={{uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar}}
            containerStyle={{alignSelf: 'center', marginTop: 3}}
          />
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={pinkyGradient}
            style={styles.mascota}>
            <CustomText numberOfLines={1} style={styles.names}>
              {item.name}
            </CustomText>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View style={{flex: 1, flexDirection: 'row'}}>
      <Query query={GET_MASCOTAS} variables={{usuarios: id}}>
        {response => {
          if (response.loading) {
            return <LineDotsLoader color={colors.main} />;
          }
          if (response.error) {
            return console.log('Response Error-------', response.error);
          }
          if (response) {
            return (
              <FlatList
                data={
                  response && response.data && response.data.getMascota
                    ? response.data.getMascota.list
                    : ''
                }
                renderItem={item => _renderItem(item, response.refetch)}
                keyExtractor={item => item.id}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              />
            );
          } else {
            return <LineDotsLoader color={colors.main} />;
          }
        }}
      </Query>
      <View style={{width: 120}}>
        <TouchableOpacity onPress={() => openModal()} style={styles.adds}>
          <Icon
            name="pluscircleo"
            type="AntDesign"
            size={40}
            style={styles.plus}
          />
        </TouchableOpacity>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={pinkyGradient}
          style={styles.mascotas}>
          <CustomText
            numberOfLines={1}
            style={styles.names}
            onPress={() => openModal()}>
            Añadir
          </CustomText>
        </LinearGradient>
      </View>
      <View style={{width: 120}}>
        <TouchableOpacity onPress={() => Navigation.navigate('Adoptar')} style={styles.adds}>
          <Icon
            name="dog-side"
            type="MaterialCommunityIcons"
            size={40}
            style={styles.plus}
          />
        </TouchableOpacity>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={pinkyGradient}
          style={styles.mascotas}>
          <CustomText
            numberOfLines={1}
            style={styles.names}
            onPress={() => Navigation.navigate('Adoptar')}>
            Adoptar
          </CustomText>
        </LinearGradient>
      </View>
      <Modal
        animationType="slide"
        presentationStyle="formSheet"
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.containerModal}>
          <SafeAreaView
            style={{
              width: dimensions.ScreenWidth,
              height: dimensions.Height(6),
            }}>
            <TouchableOpacity onPress={() => setModalVisible(false)}>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(4),
                  marginTop: dimensions.Height(2),
                }}>
                <Icon
                  name="close"
                  type="AntDesign"
                  size={25}
                  color={colors.ERROR}
                />
              </View>
            </TouchableOpacity>
          </SafeAreaView>
          <AddMascota
            id={id}
            setModalVisible={modalVisible => setModalVisible(modalVisible)}
          />
        </View>
      </Modal>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  names: {
    padding: 10,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    width: '100%',
  },
  mascota: {
    marginTop: -25,
    borderRadius: 20,
    width: 100,
  },

  mascotas: {
    borderRadius: 20,
    width: 100,
    marginTop: -22,
    ...ifIphoneX(
      {
        marginLeft: 18,
      },
      {
        marginLeft: 14,
      },
    ),
  },

  adds: {
    borderRadius:
      Math.round(
        Dimensions.get('window').width + Dimensions.get('window').height,
      ) / 2,
    width: 100,
    height: 100,
    backgroundColor: new DynamicValue(colors.lightGray, colors.back_suave_dark),
    marginLeft: dimensions.Height(2),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },

  plus: {
    color: new DynamicValue(colors.rgb_153, colors.rgb_153),
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.light_white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },
});
