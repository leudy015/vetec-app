import React from 'react';
import {View, TouchableOpacity, FlatList} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors, dimensions} from './../themes';
import {CustomText} from './CustomText';
import {Query} from 'react-apollo';
import {GET_CATEGORIES} from '../query';
import Navigation from '../services/NavigationService';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Icon from 'react-native-dynamic-vector-icons';
import {stylesText} from './StyleText';
import {LineDotsLoader} from 'react-native-indicator';

const Category = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const renderItem = item => {
    return (
      <KeyboardAwareScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}>
        <TouchableOpacity
          style={styles.card}
          onPress={() => Navigation.navigate('CategoriaPro', {data: item})}>
          <View style={styles.icon}>
            <Icon
              type={item.font}
              name={item.image}
              style={{color: colors.main1, marginLeft: 10, marginTop: 3}}
              size={30}
            />
          </View>
          <View>
            <CustomText
              light={colors.blue_main}
              dark={colors.white}
              numberOfLines={1}
              style={[
                stylesText.mainText,
                {
                  marginTop: 10,
                  paddingHorizontal: 20,
                  width: 165,
                },
              ]}>
              {item.title}
            </CustomText>

            <CustomText
              light={colors.rgb_153}
              dark={colors.light_grey}
              numberOfLines={2}
              style={
                (stylesText.terciaryText,
                [
                  {
                    marginTop: dimensions.Height(1),
                    alignSelf: 'flex-start',
                    paddingHorizontal: 20,
                    textAlign: 'left',
                    width: 165,
                  },
                ])
              }>
              {item.description}
            </CustomText>
          </View>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  };
  return (
    <Query query={GET_CATEGORIES}>
      {(response, error) => {
        if (response.loading) {
          return <LineDotsLoader color={colors.main} />;
        }
        if (error) {
          return console.log('Response Error-------', error);
        }
        if (response) {
          return (
            <FlatList
              data={
                response && response.data ? response.data.getCategories : ''
              }
              horizontal={true}
              keyExtractor={item => item.id}
              showsHorizontalScrollIndicator={false}
              renderItem={({item}) => renderItem(item)}
            />
          );
        }
      }}
    </Query>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  card: {
    height: 100,
    width: 220,
    padding: 10,
    justifyContent: 'center',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    alignSelf: 'center',
    flexDirection: 'row',
    margin: 10,
    borderRadius: 10,
    borderRightWidth: 5,
    borderRightColor: colors.main,
  },

  icon: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft: 15,
    backgroundColor: '#e1defe',
    borderRadius: 10,
  },
});

export default Category;
