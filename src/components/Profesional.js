import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Alert,
  Vibration,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';
import AsyncStorage from '@react-native-community/async-storage';
import NoData from './NoData';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {Mutation} from 'react-apollo';
import {ELIMINAR_USUARIO_FAVORITE, CREAR_USUARIO_FAVORITE} from '../mutations';
import Toast from 'react-native-simple-toast';
import {stylesText} from './StyleText';
import Start from './star';

export default function Profesional(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const ONE_SECOND_IN_MS = 100;

  const PATTERN = [1 * ONE_SECOND_IN_MS];

  const _renderItem = ({item}) => {
    const isNavigation = () => {
      if (id) {
        Navigation.navigate('SelecMascota', {data: item});
      } else {
        Alert.alert(
          'Para continuar debes iniciar sesión',
          'Debes iniciar sesión para continuar con la contratación',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
      }
    };

    let rating = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
    item.professionalRatinglist.forEach(start => {
      if (start.rate === 1) rating['1'] += 1;
      else if (start.rate === 2) rating['2'] += 1;
      else if (start.rate === 3) rating['3'] += 1;
      else if (start.rate === 4) rating['4'] += 1;
      else if (start.rate === 5) rating['5'] += 1;
    });

    const ar =
      (5 * rating['5'] +
        4 * rating['4'] +
        3 * rating['3'] +
        2 * rating['2'] +
        1 * rating['1']) /
      item.professionalRatinglist.length;
    let averageRating = 0;
    if (item.professionalRatinglist.length) {
      averageRating = ar.toFixed(1);
    }

    const anadirFavorito = (
      crearUsuarioFavoritoPro,
      profesionalId,
      usuarioId,
    ) => {
      if (!id) {
        Alert.alert(
          'Upps debes iniciar sesión',
          'Para añadir este profesional a favorito debes iniciar sesión',
          [
            {
              text: 'Iniciar sesión',
              onPress: () => Navigation.navigate('LoginSocial'),
            },
            {
              text: 'Regístrarme',
              onPress: () => Navigation.navigate('Register'),
            },
          ],
          {cancelable: false},
        );
        return null;
      } else {
        crearUsuarioFavoritoPro({
          variables: {profesionalId, usuarioId},
        }).then(async ({data: res}) => {
          if (
            res &&
            res.crearUsuarioFavoritoPro &&
            res.crearUsuarioFavoritoPro.success
          ) {
            Toast.showWithGravity(
              'Profesional añadido con éxito a al lista de deseos',
              Toast.LONG,
              Toast.TOP,
            );
            Vibration.vibrate(PATTERN);
            props.refetch();
          } else {
            return null;
          }
        });
      }
    };

    const favouritesDelete = async (eliminarUsuarioFavoritoPro, id) => {
      await eliminarUsuarioFavoritoPro({variables: {id: id}}).then(res => {
        props.refetch();
        Toast.showWithGravity(
          'Profesional eliminado con éxito de la lista de deseos',
          Toast.LONG,
          Toast.TOP,
        );
        Vibration.vibrate(PATTERN);
      });
    };

    return (
      <TouchableOpacity
        onPress={() => Navigation.navigate('DetailsPro', {data: item})}
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View style={styles.avatar}>
            <Image
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar}}
              style={styles.avatar}
            />
          </View>
          <View style={{marginLeft: dimensions.Width(4)}}>
            <View
              style={{
                flexDirection: 'row',
                width: dimensions.Width(65),
              }}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.light_white}
                style={[
                  stylesText.mainText,
                  {maxWidth: dimensions.Width(42), width: 'auto'},
                ]}>
                {item.nombre} {item.apellidos}{' '}
              </CustomText>
              {item.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={12}
                  style={{
                    alignSelf: 'center',
                    marginTop: 0,
                    paddingLeft: 5,
                    color: colors.main,
                  }}
                />
              ) : null}
              <View style={{marginLeft: 'auto'}}>
                {item.anadidoFavorito ? (
                  <Mutation mutation={ELIMINAR_USUARIO_FAVORITE}>
                    {eliminarUsuarioFavoritoPro => {
                      return (
                        <TouchableOpacity
                          onPress={() =>
                            favouritesDelete(
                              eliminarUsuarioFavoritoPro,
                              item.id,
                            )
                          }>
                          <Icon
                            name="heart"
                            type="AntDesign"
                            size={16}
                            style={{
                              alignSelf: 'center',
                              marginTop: 0,
                              marginRight: 0,
                              color: colors.ERROR,
                            }}
                          />
                        </TouchableOpacity>
                      );
                    }}
                  </Mutation>
                ) : (
                  <Mutation mutation={CREAR_USUARIO_FAVORITE}>
                    {crearUsuarioFavoritoPro => {
                      return (
                        <TouchableOpacity
                          onPress={() =>
                            anadirFavorito(crearUsuarioFavoritoPro, item.id, id)
                          }>
                          <Icon
                            name="hearto"
                            type="AntDesign"
                            size={16}
                            style={{
                              alignSelf: 'center',
                              marginTop: 0,
                              marginRight: 0,
                              color: colors.rgb_153,
                            }}
                          />
                        </TouchableOpacity>
                      );
                    }}
                  </Mutation>
                )}
              </View>
            </View>
            <View style={{flexDirection: 'row', width: dimensions.Width(65)}}>
              <CustomText
                light={colors.main}
                dark={colors.main}
                style={[
                  stylesText.secondaryText,
                  {marginTop: dimensions.Height(1)},
                ]}>
                {item.profesion}
              </CustomText>
              <Start
                star={averageRating}
                styles={{marginLeft: 'auto', marginTop: 10}}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={[stylesText.secondaryText, {marginTop: 10}]}>
                ({averageRating})
              </CustomText>
            </View>
            <CustomText
              numberOfLines={1}
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1), width: 200},
              ]}>
              {item.experiencia} de experiencia
            </CustomText>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[
                stylesText.secondaryText,
                {marginTop: dimensions.Height(1)},
              ]}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.main}
              />{' '}
              {item.ciudad}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingTop: dimensions.Width(3),
          }}>
          <TouchableOpacity>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={[stylesText.mainText, {marginTop: dimensions.Height(1)}]}>
              {item.Precio}€ /Consulta
            </CustomText>
          </TouchableOpacity>
          <View style={{marginLeft: 'auto'}}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={isNavigation}
                title="Hacer consulta"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={props.data}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <NoData menssge="Aún no tenemos ningún profesional" />
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(96),
    height: 'auto',
    alignSelf: 'center',
    backgroundColor: new DynamicValue(colors.white, colors.back_suave_dark),
    padding: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
});
