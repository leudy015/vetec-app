import React from 'react';
import {Image, View} from 'react-native';
import {image} from '../constants/image';
import {CustomText} from './CustomText';
import {dimensions, colors} from '../themes';

const NoData = props => {
  return (
    <View style={{alignSelf: 'center', padding: 20}}>
      <Image
        source={image.Vacia}
        style={{
          width: dimensions.Width(69),
          alignSelf: 'center',
          height: dimensions.Height(22),
        }}
      />
      <CustomText
        light={colors.blue_main}
        dark={colors.light_white}
        style={{
          textAlign: 'center',
          fontSize: dimensions.FontSize(20),
          fontWeight: '200',
        }}>
        {props.menssge}
      </CustomText>
    </View>
  );
};

export default NoData;
