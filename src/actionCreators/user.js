import {
    SET_USER,
    RESET_USER,
  
  } from '../redux/type'
  
  export function setUser() {
    return { type: RESET_USER,payload:{} }
  }
  
  export function resetUser(info) {
    return { type: SET_USER, payload: info }
  }
  
