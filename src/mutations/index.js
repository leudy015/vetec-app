import gql from 'graphql-tag';

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarUsuario($email: String!, $password: String!) {
    autenticarUsuario(email: $email, password: $password) {
      success
      message
      data {
        token
        id
        verifyPhone
      }
    }
  }
`;

export const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        id
        nombre
        apellidos
        email
      }
    }
  }
`;

export const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      id
      email
      UserID
      nombre
      apellidos
      ciudad
      telefono
      avatar
      notificacion
      MyVet
      isPlus
      connected
      lastTime
      setVideoConsultas
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      message
    }
  }
`;

export const NUEVA_MASCOTA = gql`
  mutation crearMascota($input: Mascotainput) {
    crearMascota(input: $input) {
      success
      message
      data {
        id
        name
        age
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    }
  }
`;

export const CREAR_USUARIO_FAVORITE = gql`
  mutation crearUsuarioFavoritoPro($profesionalId: ID!, $usuarioId: ID!) {
    crearUsuarioFavoritoPro(
      profesionalId: $profesionalId
      usuarioId: $usuarioId
    ) {
      success
      message
    }
  }
`;

export const ELIMINAR_USUARIO_FAVORITE = gql`
  mutation eliminarUsuarioFavoritoPro($id: ID!) {
    eliminarUsuarioFavoritoPro(id: $id) {
      success
      message
    }
  }
`;

export const ELIMINAR_MASCOTA = gql`
  mutation eliminarMascota($id: ID!) {
    eliminarMascota(id: $id) {
      success
      message
    }
  }
`;

export const CREAR_MODIFICAR_CONSULTA = gql`
  mutation crearModificarConsulta($input: ConsultaCreationInput) {
    crearModificarConsulta(input: $input) {
      success
      message
      data {
        id
        cupon
        nota
        aceptaTerminos
        endDate
        time
        cantidad
        descuento {
          clave
          descuento
          tipo
        }
        usuario {
          id
        }
        profesional {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          profesion
          experiencia
          descricion
          Precio
          notificacion
          isProfesional
          isVerified
          isAvailable
        }
        estado
        status
        progreso
        created_at
        mascota {
          id
          name
          age
          usuario
          avatar
        }
      }
    }
  }
`;

export const READ_MESSAGE = gql`
  mutation readMessage($conversationID: ID!) {
    readMessage(conversationID: $conversationID) {
      success
      message
    }
  }
`;

export const RECIBE_MESSAGE = gql`
  mutation recibeMessage($conversationID: ID!) {
    recibeMessage(conversationID: $conversationID) {
      success
      message
    }
  }
`;

export const DELETE_CONVERSATION = gql`
  mutation deleteConversation($id: ID!) {
    deleteConversation(id: $id) {
      success
      message
    }
  }
`;

export const PROFESSIONAL_CONSULTA_PROCEED = gql`
  mutation consultaProceed(
    $consultaID: ID!
    $estado: String!
    $progreso: String!
    $status: String!
    $nota: String
    $coment: String
    $rate: Int
    $descripcionproblem: String
  ) {
    consultaProceed(
      consultaID: $consultaID
      estado: $estado
      progreso: $progreso
      status: $status
      nota: $nota
      coment: $coment
      rate: $rate
      descripcionproblem: $descripcionproblem
    ) {
      success
      message
    }
  }
`;

export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID!) {
    readNotification(notificationId: $notificationId) {
      success
      message
    }
  }
`;

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;
