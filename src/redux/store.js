import { createStore,applyMiddleware,compose } from 'redux';
import thunk from "redux-thunk";
import { persistStore, persistReducer } from 'redux-persist'
import {appReducer} from './combileReducer'
import AsyncStorage from '@react-native-community/async-storage';

const persistConfig = {
  key: 'root',
  storage:AsyncStorage,
  whitelist:['user']
}

const persistedReducer = persistReducer(persistConfig, appReducer)


const enhancer = compose(
    applyMiddleware(thunk)
);

let store = createStore(persistedReducer, {}, enhancer)
let persistor = persistStore(store)
   
export  { store, persistor }