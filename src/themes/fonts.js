export const fonts={
    sansbold:'Clear Sans Bold',
    sanslight:'ClearSans-Light',
    sansregular:'Clear Sans',
    sansmedium:'Clear Sans Medium',
    impact:'Impact'
}