import React, {Component} from 'react';
import {View, NativeModules} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from './../themes';
import {CustomText} from './../components/CustomText';

const {RNTwitterSignIn} = NativeModules;

const Constants = {
  TWITTER_COMSUMER_KEY: 'owgI95CXvidObdnCzoV9hySvo',
  TWITTER_CONSUMER_SECRET: 'lcY38cZ9aDaSjhxlJbCED7MYveLLofnlIS8y3GDWhBoU2rOhaG',
};

export default class TwitterButton extends Component {
  state = {
    isLoggedIn: false,
  };

  componentDidMount() {
    RNTwitterSignIn.init(
      Constants.TWITTER_COMSUMER_KEY,
      Constants.TWITTER_CONSUMER_SECRET,
    );
  }

  _twitterSignIn = () => {
    RNTwitterSignIn.logIn()
      .then(loginData => {
        let email = loginData.email;
        let firstName = loginData.userName;
        let lastName = '';
        let token = loginData.userID;
        this.props.onSuccess({firstName, lastName, email, token});
        const {authToken, authTokenSecret} = loginData;
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true,
          });
        }
      })
      .catch(error => {
        console.log('error', error);
      });
  };

  handleLogout = () => {
    console.log('logout');
    RNTwitterSignIn.logOut();
    this.setState({
      isLoggedIn: false,
    });
  };

  render() {
    const {isLoggedIn} = this.state;
    return (
      <View>
        {isLoggedIn ? (
          <Icon.Button
            onPress={this.handleLogout}
            name="twitter"
            backgroundColor={colors.main}>
            <CustomText
              style={{
                fontFamily: 'Arial',
                fontSize: 22,
                paddingVertical: 10,
                color: colors.white,
              }}>
              Cerrar sesión
            </CustomText>
          </Icon.Button>
        ) : (
          <Icon.Button
            onPress={() => this._twitterSignIn()}
            name="twitter"
            backgroundColor={colors.twitter_color}>
            <CustomText
              style={{
                fontFamily: 'Arial',
                fontSize: 22,
                paddingVertical: 10,
                color: colors.white,
              }}>
              Continúa con Twitter
            </CustomText>
          </Icon.Button>
        )}
      </View>
    );
  }
}
