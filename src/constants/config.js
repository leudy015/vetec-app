export const NETWORK_INTERFACE_LINK = 'https://server.vetec.es';
export const NETWORK_INTERFACE = `${NETWORK_INTERFACE_LINK}/graphql`;
export const NETWORK_INTERFACE_LINK_AVATAR = `${NETWORK_INTERFACE_LINK}/assets/images/`;
