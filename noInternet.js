import  React  from 'react';
import { View, Image } from "react-native";
import { CustomText } from './src/components/CustomText';
import { image } from './src/constants/image';
import { colors, dimensions } from './src/themes';
import { Button } from './src/components/Button';
import NavigationService from "./src/services/NavigationService";
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet, useDarkModeContext } from 'react-native-dark-mode'



export default function Nointernet(){

    const styles = useDynamicStyleSheet(dynamicStyles)

    return(
        <View style={styles.container} >
          <Image 
          source={image.Vacia} 
          style={{width: dimensions.Width(75), alignSelf: 'center', height: dimensions.Height(23)}} 
          />
          <CustomText light={colors.back_dark} dark={colors.white} style={{textAlign: 'center', fontSize: dimensions.FontSize(18), fontWeight: '300', paddingHorizontal: dimensions.Width(4), marginTop: dimensions.Height(2)}}>Hay un problema con tu conexión a internet.</CustomText>
          <View style={styles.signupButtonContainer}>
          <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
              onPress={() => NavigationService.navigate('Home')} title="Volver a intentarlo"
              titleStyle={styles.buttonTitle} />
          </View>
      </View>
    )
}

const dynamicStyles = new DynamicStyleSheet({

    container: {
        flex: 1,
        justifyContent: 'center', 
        height: dimensions.Height(100),
        backgroundColor: new DynamicValue(colors.white, colors.back_dark)
      },
    
    buttonView: {
      backgroundColor: colors.light_blue,
      width: dimensions.Width(80),
      borderRadius: dimensions.Width(8),
      alignSelf: 'center',
      marginTop: dimensions.Height(4)
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(2),
      color: colors.white,
      fontWeight: '300',
      fontSize: dimensions.FontSize(14),
    },
    
    })
    
